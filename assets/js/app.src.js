/* ************************ */
var PARSE_APP = "dDuzq2rRDyVvIJqYY3kVPHxklPKgMvpk5qQmYAZH";
var PARSE_KEY = "QqVbUXskSz6PacYbD5gIc0mjBT15be1Sj6ZFhFUk";
var DEMO_APP  = "8VE1TaqKtx";
Parse.initialize(PARSE_APP, PARSE_KEY);
/* ************************ */
/*
!!! Not using default views to avoid to much dependency with Parse !!!
*/

/* ************************ */
/* ACTIONS */
/* ************************ */
function doLogin(form_name, email_field, password_field)
{
  var email = $('#'+email_field).val();
  var password = $('#'+password_field).val();

  if ($('#'+email_field).formance('validate_email') && $('#'+password_field).val() != "")
  {
    Parse.User.logIn(email, password, {
      success: function(user) {
        //Store user's token in session
        localStorage.setItem("user_name", user.get("name"));
        localStorage.setItem("session_token", user.getSessionToken());

        viewApps();

      },
      error: function(user, error) {
        alert("Invalid username or password. Please try again.");
      }
    });
  }
}

function doForgot(email)
{
  Parse.User.requestPasswordReset(email, {
    success:function() {
      alert("Reset instructions emailed to you.");
    },
    error:function(error) {
      alert(error.message);
    }
  });
}

function doLogout()
{
  localStorage.removeItem("user_name");
  localStorage.removeItem("session_token");
  Parse.User.logOut();
  viewHome();
}

function doSignup(email_field, password_field, full_name_field)
{

  var email = $('#'+email_field).val();
  var password = $('#'+password_field).val();
  var full_name = $('#'+full_name_field).val();

  if ($('#'+email_field).formance('validate_email') && $('#'+password_field).val() != "")
  {

    Parse.User.signUp(email, password, { name : full_name, email : email }, {
      success: function(user) {

        var acl = new Parse.ACL();
        acl.setWriteAccess(Parse.User.current().id, true);
        acl.setPublicReadAccess(true);
        user.setACL(acl);
        user.save();

        //Store user's token in session
        localStorage.setItem("user_name", user.get("name"));
        localStorage.setItem("session_token", user.getSessionToken());

        //Add the DEMO app
        // Declare the types.
      var Application = Parse.Object.extend("Application");
      
      // Create the post
      var myApp = new Application();

      myApp.id = DEMO_APP;

      var relation = myApp.relation("team");

      relation.add(user);
       
      // This will save both myPost and myComment
      myApp.save().then(function(feedback) {
        viewApps();
      }, function(error) {
      });
        
      },
      error: function(user, error) {
        alert(error.message);
      }
    });
  }

}

function doRemoveApp()
{

  bootbox.confirm("Are you sure you want to delete the app from your account?", function(result) {
    if (result == true) {
      var URL = getUrlVars();
      var app_id = URL['app'];
      
      var app = new Parse.Object("Application");  
      app.id = app_id;

      var relation = app.relation("team");
      relation.remove(Parse.User.current());
       
      // This will save both myPost and myComment
      app.save().then(function(feedback) {
        viewApps();
      }, function(error) {
      });
    };
  }); 

  
  
}

function doEarlyAccess(email_field, full_name_field)
{

  var email = $('#'+email_field).val();
  var full_name = $('#'+full_name_field).val();

  if ($('#'+email_field).formance('validate_email'))
  {

    // Declare the types.
    var Queue = Parse.Object.extend("Queue");
    
    // Create the post
    var myInvitation = new Queue();
    myInvitation.set("name", full_name);
    myInvitation.set("email", email);
     
    // This will save both myPost and myComment
    myInvitation.save().then(function(object) {
      viewThankYou();
    }, function(error) {
    });

  }

}

function doValidateInvitation()
{
  var URL = getUrlVars();
  var queue_id = URL['invitation'];

  var Queue = Parse.Object.extend("Queue");
  var query = new Parse.Query(Queue);

  query.get(queue_id, {
    success: function(object) {          
      $("#name").val(object.get("name"));
      $("#email").val(object.get("email"));     
    },
    error: function(error) {
      viewHome();
    }
  });
}

function doUpdate(email_field, name_field)
{

  var email = $('#'+email_field).val();
  var name = $('#'+name_field).val();

  var user = Parse.User.current();

  user.setEmail(email);
  user.setUsername(email);
  user.save({
    name: name
  }).then(function(user) {
    viewProfile();
  }, function(error) {
  });;
}

function doAddRoute(route_field, rating_field)
{
  var log = new Parse.Object("Log");

  var route_id = $('#'+route_field).val();
  
  var rating_id = $('#'+rating_field).val();
  if (rating_id == "0") {
      rating_id = null;
  }

  doSaveRoute(route_id, rating_id);

}

function doDeleteBucket(object_id)
{
  bootbox.confirm("Are you sure you want to delete the connector?", function(result) {
    if (result == true)
    {
      var bucket = new Parse.Object("Bucket");
      bucket.id = object_id;
      bucket.destroy({}).then(function(obj) {
        viewApp();
      }, function(error) {
        
      });
    }
  }); 
  
}

function doArchiveFeedback()
{

  bootbox.confirm("Are you sure you want to archive the feedback?", function(result) {
    if (result == true)
    {
      
      var URL = getUrlVars();

      var log_id = null;

      if (URL['feedback'] != undefined) {
        log_id = URL['feedback'];
      };

      var feedback = new Parse.Object("Feedback");
      feedback.id = log_id;

      feedback.save({
        isActive: false
      }).then(function(feedback) {
        window.history.back();
      }, function(error) {
      });
    }
  }); 

}

function doCreateApp(name_field)
{
  name = $("#"+name_field).val();

  //Let's find out if it's a Bundle ID or just an app name  
  $.ajax({
    url: "http://itunes.apple.com/lookup?bundleId="+name,
    dataType: "JSONP"
  }).done(function(data) {
    // Declare the types.
    var Application = Parse.Object.extend("Application");
    
    // Create the post
    var myApp = new Application();

    if (data.resultCount > 0)
    {
      app = data.results[0];
      
      myApp.set("name", app.trackName);  
      myApp.set("icon", app.artworkUrl100);
      myApp.set("storeId", app.bundleId);  

    }
    else
    {      
      myApp.set("name", name);      
    }

    var relation = myApp.relation("team");
    relation.add(Parse.User.current());
     
    // This will save both myPost and myComment
    myApp.save().then(function(feedback) {
      viewApps();
    }, function(error) {
    });
  });

  

}

function doCreateBucket(service_field, description_field, token_field)
{
  service_id = $("#"+service_field).val();
  description = $("#"+description_field).val();
  token_str = $("#"+token_field).val();

  var URL = getUrlVars();
  var app_id = URL['app'];
  
  var app = new Parse.Object("Application");  
  app.id = app_id;

  var service = new Parse.Object("Service");  
  service.id = service_id;

  var token = new Parse.Object("Token");  
  token.set("service", service);
  token.set("token", token_str);
  token.set("user",Parse.User.current());

  token.save().then(function(token_saved) {
    
    // Declare the types.
    var Bucket = Parse.Object.extend("Bucket");
    
    // Create the post
    var bucket = new Bucket();
    bucket.set("application", app);
    bucket.set("token", token_saved);
    bucket.set("description", description);

    // This will save both myPost and myComment
    bucket.save().then(function(bucket_saved) {
      viewApp();
    }, function(error) {
    });


  }, function(error) {
  });

  

}

/* ************************ */
/* REDIRECTIONS */
/* ************************ */
function doDownloadSDK()
{
  window.location.href = "builds/Klimbz.v0.2.zip";
}

function viewProfile()
{
  window.location.href = "profile.html";
}

function viewAppSettings(app_id)
{
  if (app_id == null) {
    var URL = getUrlVars();
    app_id = URL['app'];
  } 
  window.location.href = "settings.html?app="+app_id;
}


function viewDownloadSDK(app_id)
{
  if (app_id == null) {
    var URL = getUrlVars();
    app_id = URL['app'];
  } 
  window.location.href = "download.html?app="+app_id;
}

function viewHome()
{
  if (hasSessionActive()) { 
    viewApps(); 
  }
  else
  {
    window.location.href = "index.html";  
  }
  
}


function viewThankYou()
{
  window.location.href = "thank_you.html";
}

function viewApp(app_id)
{
  if (app_id == null) {
    var URL = getUrlVars();
    app_id = URL['app'];
  } 
  window.location.href = "app.html?app="+app_id;
}

function viewCreateConnector(app_id)
{
  if (app_id == null) {
    var URL = getUrlVars();
    app_id = URL['app'];
  } 
  window.location.href = "connector_create.html?app="+app_id;
}

function viewApps()
{
  window.location.href = "apps.html";
}

function viewFeedback(feedback_id)
{
  window.location.href = "feedback.html?feedback="+feedback_id;
}

function viewCreateApp()
{
 window.location.href = "app_create.html"; 
}

/* ************************ */
/* FILLING VIEWS */
/* ************************ */
function fillProfileForUser(user, routes_template,routes_div)
{
  routes_template = $("#"+routes_template).html();

  var loggedUser = Parse.User.current();


  
  $("#profile_name").html(user.get("name"));
  $("#profile_username").html(user.get("username"));

  if (user.get("image") != undefined) { $("#profile_picture").attr("src",user.get("image").url()); }

  var Log = Parse.Object.extend("Log");
  var query = new Parse.Query(Log);
  query.equalTo('createdBy', user);
  query.include('route');
  query.include('route.color');
  query.include('route.grade');
  query.include('route.wall');
  query.include('route.wall.gym');
  query.include('createdBy');
  query.find({
    success: function(results) {
      for (var i = 0; i < results.length; i++) {
        var object = results[i];
        if (object.get('route') == undefined) { continue; };
        
        var color = object.get('route').get('color');        
        var rating_name = "Not rated yet";
        var grade = object.get('route').get('grade');
        var wall = object.get('route').get('wall');
        var gym = wall.get('gym');
        var route = object.get('route');
        var createdBy = object.get('createdBy');

        var routes_action = "javascript:viewRoutes('"+wall.id+"')";

        var delete_action = "";

        if (loggedUser.id == user.id) {
          delete_action = "javascript:doDeleteLog('"+object.id+"')";
        }

        var wall_link = "javascript:viewRoutes('"+wall.id+"')";
        var gym_link = "javascript:viewWalls('"+gym.id+"')";
        var route_link = "javascript:viewRoute('"+route.id+"')";

        if (i == 0) {
          $("#gym_name").html(" @"+ gym.get('name'));
          $("#wall_name").html(" @"+ wall.get("name"));
          $("#wall_description").html(wall.get("description"));
          if (wall.get("image")!=undefined) { $("#wall_image").attr('src', wall.get("image").url());    };

        };
        
        var routes_action = "javascript:viewRoute('"+object.id+"')";

        route_html = routes_template;
        route_html = route_html.replace("{{route_created}}",object.createdAt);
        route_html = route_html.replace("{{wall_name}}",wall.get('name'));
        route_html = route_html.replace("{{gym_name}}",gym.get('name'));
        route_html = route_html.replace("{{route_setter}}", createdBy.get('name'));
        route_html = route_html.replace(/{{route_action}}/g, routes_action);
        route_html = route_html.replace(/{{route_delete_action}}/g, delete_action);
        route_html = route_html.replace(/{{wall_link}}/g, wall_link);
        route_html = route_html.replace(/{{gym_link}}/g, gym_link);
        route_html = route_html.replace(/{{route_link}}/g, route_link);

        if (grade && color) {
          route_html = route_html.replace("{{route_grade}}", grade.get('description'));
          route_html = route_html.replace("{{route_color}}", color.get('description'));
          $('#'+routes_div).append(route_html);
        }
      }
    },
    error: function(error) {
      alert("Error: " + error.code + " " + error.message);
    }
  });

}
function createStats(feedback_array)
{
  var classes = new Array();
  var platform = new Array();
  var oses = new Array();
  var dates = new Array();

  for (var i = 0; i < feedback_array.length; i++) {
    feedback = feedback_array[i];

    if (classes[feedback.get('classCaller')] == undefined) { classes[feedback.get('classCaller')] = 0; }
    if (platform[feedback.get('platformType')] == undefined) { platform[feedback.get('platformType')] = 0; }
    if (oses[feedback.get('osVersion')] == undefined) { oses[feedback.get('osVersion')] = 0; }
    if (dates[showDateWithFormat(feedback.createdAt)] == undefined) { dates[showDateWithFormat(feedback.createdAt)] = 0; }

    classes[feedback.get('classCaller')]++;
    platform[feedback.get('platformType')]++;
    oses[feedback.get('osVersion')]++;
    dates[showDateWithFormat(feedback.createdAt)]++;

  }

  console.log(dates);
  console.log(classes);
  console.log(platform);
  console.log(oses);

  platform_chart = new Array();
  for (var key in platform) {
    value = new Array();
    value[0] = key;
    value[1] = platform[key];
    platform_chart[platform_chart.length] = value;
  };


  oses_chart = new Array();
  for (var key in oses) {
    value = new Array();
    value[0] = key;
    value[1] = oses[key];
    oses_chart[oses_chart.length] = value;
  };


  classes_chart = new Array();
  for (var key in classes) {
    value = new Array();
    value[0] = key;
    value[1] = classes[key];
    classes_chart[classes_chart.length] = value;
  };

  if (classes_chart.length == 0)
  {
    value = new Array();
    value[0] = "No data";
    value[1] = 100;

    classes_chart[classes_chart.length] = value;
    oses_chart[oses_chart.length] = value;
    platform_chart[platform_chart.length] = value;

  }
  
  
  $('#chart_platforms').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            credits: {
            enabled : false
          }, 
            title: {
                text: 'Devices Used'
            },
            tooltip: {
              pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                type: 'pie',
                name: 'Devices',
                data: platform_chart
            }]
        });

  $('#chart_classes').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            credits: {
            enabled : false
          },
            title: {
                text: 'Screens With Feedback'
            },
            tooltip: {
              pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                type: 'pie',
                name: 'Screens',
                data: classes_chart
            }]
        });

  $('#chart_oses').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            credits: {
            enabled : false
          },
            title: {
                text: 'iOS Version'
            },
            tooltip: {
              pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                type: 'pie',
                name: 'iOS',
                data: oses_chart
            }]
        });

  

}

function fillFeedback(feedback_template, feedback_div)
{
  feedback_template = $("#"+feedback_template).html();

  var URL = getUrlVars();
  var gym_id = null;
  var Feedback = Parse.Object.extend("Feedback");
  var query = new Parse.Query(Feedback);

  query.equalTo("isActive", true);
  query.include('application');  
  query.descending("createdAt");

  if (URL['app'] != undefined) {
    app_id = URL['app'];
    var app = new Parse.Object("Application");
    app.id = app_id;
    query.equalTo("application", app);
  };

  query.find( {
    success: function(results) {
      var set = false;
      for (var i = 0; i < results.length; i++) {

        var object = results[i];
        var feedback = object;

        var app = object.get('application');
        
        if (app_id != null && app_id == app.id && !set){
          $("#app_name").html(app.get('name'));        
          set = true;
        }
    
        feedback_html = feedback_template;

        var feedback_action = "javascript:viewFeedback('"+object.id+"')";

        if ( feedback.get("screenshot")) {
          feedback_html = feedback_html.replace("{{feedback_image}}", feedback.get("screenshot"));  
        }
        else
        {
          feedback_html = feedback_html.replace("{{feedback_image}}", "img/empty.png");  
        }
                
        feedback_html = feedback_html.replace("{{feedback_class_caller}}",feedback.get('classCaller'));
        feedback_html = feedback_html.replace("{{feedback_description}}", feedback.get('description'));
        feedback_html = feedback_html.replace("{{feedback_date}}", showDateWithFormat(feedback.createdAt));
        feedback_html = feedback_html.replace(/{{feedback_action}}/g, feedback_action);

        $('#'+feedback_div).append(feedback_html);
      
      }

      createStats(results);

      $("#app_feedback_count").html(results.length);
      
    },
    error: function(error) {
      alert("Error: " + error.code + " " + error.message);
    }
  });

  

}

function fillBuckets(bucket_template, buckets_div)
{
  bucket_template = $("#"+bucket_template).html();

  var URL = getUrlVars();
  var Bucket = Parse.Object.extend("Bucket");
  var query = new Parse.Query(Bucket);

  query.include('application');  
  query.include('token');  
  query.include('token.service');  

  query.descending("createdAt");

  if (URL['app'] != undefined) {
    app_id = URL['app'];
    var app = new Parse.Object("Application");
    app.id = app_id;
    query.equalTo("application", app);
  };

  query.find( {
    success: function(results) {
      var set = false;
      for (var i = 0; i < results.length; i++) {

        var object = results[i];
        var bucket = object;
        var token = object.get('token');
        var service = token.get('service');
        var app = object.get('application');
       
        bucket_html = bucket_template;

        var bucket_action = "javascript:doDeleteBucket('"+object.id+"')";

        if (service.get('icon') != undefined) {
          bucket_html = bucket_html.replace("{{service_icon}}",service.get('icon'));
        }
        else
        {
          bucket_html = bucket_html.replace("{{service_icon}}","images/circle-icons/full-color/smartphone.png");
        }
        
        bucket_html = bucket_html.replace("{{service_name}}",service.get('name'));
        bucket_html = bucket_html.replace("{{service_link}}",service.get('link'));
        bucket_html = bucket_html.replace("{{token_token}}", token.get('token'));
        bucket_html = bucket_html.replace("{{bucket_description}}", bucket.get('description'));
        bucket_html = bucket_html.replace(/{{bucket_action}}/g, bucket_action);

        $('#'+buckets_div).append(bucket_html);
      
      }

    },
    error: function(error) {
      alert("Error: " + error.code + " " + error.message);
    }
  });

  

}


function fillApp()
{
  var URL = getUrlVars();
  var Application = Parse.Object.extend("Application");
  var query = new Parse.Query(Application);
  var app_id;

  query.include('application');

  if (URL['app'] != undefined) {
    app_id = URL['app'];    
  }

  query.get(app_id, {
    success: function(object) {
      if (object.get("icon") != undefined) { $("#app_icon").attr("src",object.get("icon")); } else  { $("#app_icon").css("display","none"); }
      $("#app_name").html(object.get("name"));     
      $("#application_key ").html(object.id);
    },
    error: function(error) {
      alert("Error: " + error.code + " " + error.message);
    }
  });
}

function fillApps(apps_template, app_div)
{

  apps_template = $("#"+apps_template).html();

  var URL = getUrlVars();

  var gym_id = null;

  var Application = Parse.Object.extend("Application");
  var User = Parse.Object.extend("User");

  var query = new Parse.Query(Application);
  query.descending("createdAt");
  query.equalTo("team", Parse.User.current());

  query.find( {
    success: function(results) {
      for (var i = 0; i < results.length; i++) {

        var object = results[i];

        var app_action = "javascript:viewApp('"+object.id+"')";

        app_html = apps_template;
        if (object.get('icon') != undefined) { 
          app_html = app_html.replace("{{app_icon}}",object.get('icon')); 
        }
        else
        {
          app_html = app_html.replace("{{app_icon}}", "images/circle-icons/full-color/smartphone.png"); 
        }
        
        app_html = app_html.replace("{{app_name}}",object.get('name'));      
        app_html = app_html.replace("{{app_key}}",object.id);                
        app_html = app_html.replace(/{{app_action}}/g,app_action);
        
        $('#'+app_div).append(app_html);

        
      }
    },
    error: function(error) {
      alert("Error: " + error.code + " " + error.message);
    }
  });
}

function fillFeedbackDetails()
{

  var URL = getUrlVars();
  var route_id = null;
  var Feedback = Parse.Object.extend("Feedback");
  var query = new Parse.Query(Feedback);

  query.include('application');

  if (URL['feedback'] != undefined) {
    feedback_id = URL['feedback'];    
  }

  query.get(feedback_id, {
    success: function(object) {
      var application = object.get('application');
     
      if (object.get("screenshot")!=undefined) { $("#feedback_image").attr('src', object.get("screenshot"));    };

      $("#app_link").attr("href","javascript:viewApp('"+application.id+"')");
      $("#app_name").html(application.get("name"));
      $("#feedback_description").html(object.get("description"));
      $("#feedback_date").html(showDateWithFormat(object.createdAt));
      $("#feedback_class_caller").html(object.get("classCaller"));
      $("#feedback_os").html(object.get("osVersion"));
      $("#feedback_platform").html(object.get("platformType"));
      $("#feedback_screen_width").html(object.get("screenWidth"));
      $("#feedback_screen_height").html(object.get("screenHeight"));
      $("#feedback_screen_brightness").html(object.get("screenBrightness"));
      $("#feedback_cpu_usage").html(object.get("cpuUsage"));
      $("#feedback_free_memory").html(object.get("freeMemory"));
      $("#feedback_battery_charge").html(object.get("batteryCharge"));      
      $("#feedback_stack_trace").text(object.get("stackTrace"));  
      var trace = object.get("varTrace");
      trace = (trace.substring(1,trace.length - 1)); 
      $('#feedback_var_trace').jsontree(trace);
    },
    error: function(error) {
      alert("Error: " + error.code + " " + error.message);
    }
  });
}

function fillProfile(route_template,routes_div)
{

  var URL = getUrlVars();

  var user_id = null;
  var user = Parse.User.current();

  if (URL['user'] != undefined) {
    var User = Parse.Object.extend("User");
    var query = new Parse.Query(User);
    query.get(URL['user'], {
      success: function(object) {
        fillProfileForUser(object, route_template,routes_div);
      },
      error: function(error) {
        alert("Error: " + error.code + " " + error.message);
      }
    });
  }
  else
  {
    fillProfileForUser(user, route_template,routes_div);
  }

}

function fillEditableProfile()
{
  var user = Parse.User.current();
  $("#profile_name").val(user.get("name"));
  $("#profile_username").val(user.get("username"));
}

/* ************************ */
/* FILLING INPUTS */
/* ************************ */
function fillServiceDetails(service_id, service_info_div, description_div, details_div)
{
  var URL = getUrlVars();

  var Service = Parse.Object.extend("Service");
  var query = new Parse.Query(Service);

  query.get(service_id, {
    success: function(object) {

      var name = object.get('name');
      var link = object.get('link');
      var linkHelp = object.get('helpLink');
      $("#"+details_div).css('display','');
      $("#"+description_div).html(link);
      $("#"+service_info_div).html("<strong>" + name + "</strong> requires an authentication token and the " + link + ". Get more info on how to get a token at <a href='"+linkHelp+"' target='_blank'>" + linkHelp + "</a>");

    },
    error: function(error) {
      alert("Error: " + error.code + " " + error.message);
    }
  });
}

function fillInputServices(services_field)
{
  var Service = Parse.Object.extend("Service");
  var query = new Parse.Query(Service);

  $('#'+services_field).html("");


  query.find({
    success: function(results) {
      $('#'+services_field).append('<option value="0">--Select Service--</option>');
      for (var i = 0; i < results.length; i++) {
        var object = results[i];
        $('#'+services_field).append('<option value='+object.id+'>'+object.get('name')+'</option>');
      }
    },
    error: function(error) {
      alert("Error: " + error.code + " " + error.message);
    }
  });
}

function fillInputWall(gym_id, wall_field)
{
  var gym = new Parse.Object("Gym");
  gym.id = gym_id;

  var Wall = Parse.Object.extend("Wall");

  var query = new Parse.Query(Wall);
  query.equalTo('gym', gym);

  $('#'+wall_field).html("");


  query.find({
    success: function(results) {
      $('#'+wall_field).append('<option value="0">--Select Wall--</option>');
      for (var i = 0; i < results.length; i++) {
        var object = results[i];
        $('#'+wall_field).append('<option value='+object.id+'>'+object.get('name')+'</option>');
      }
    },
    error: function(error) {
      alert("Error: " + error.code + " " + error.message);
    }
  });
}

function fillInputRoute(wall_id, route_field)
{
  var wall = new Parse.Object("Wall");
  wall.id = wall_id;

  var Route = Parse.Object.extend("Route");

  var query = new Parse.Query(Route);
  query.equalTo('wall', wall);
  query.include('grade');
  query.include('color');
  query.include('createdBy');

  $('#'+route_field).html("");

  query.find({
    success: function(results) {
      $('#'+route_field).append('<option value="0">--Select Route--</option>');
      for (var i = 0; i < results.length; i++) {
        var object = results[i];
        var grade = object.get('grade');
        var color = object.get('color');
        var createdBy = object.get('createdBy');
        $('#'+route_field).append('<option value='+object.id+'>'+grade.get('description')+ ' (' +color.get('description')+')</option>');
      }
    },
    error: function(error) {
      alert("Error: " + error.code + " " + error.message);
    }
  });
}

/* ************************ */
/* SESSION  */
/* ************************ */
function hasSessionActive()
{
  return (Parse.User.current() != null);
}

function setSessionMenu()
{
  if (hasSessionActive())
  {
    $('.session_name').html(localStorage.getItem("user_name"));
    $('#option_session').css('display','');
    $('#option_signin').css('display','none');
  }
  else
  {
    $('#option_session').css('display','none');
    $('#option_signin').css('display','');
  }
}

function goToHome()
{
  window.location.replace("index.html");
}

/* ************************ */
/* UTILS  */
/* ************************ */
function createMenu()
{
  var menu = "";
  
  
  
  if (hasSessionActive())
  {
    menu += '<li><a href="apps.html">Applications</a></li>';  
    menu += '<li class="dropdown" id="option_session">';
    menu += '  <a href="#" class="dropdown-toggle" data-toggle="dropdown">' + Parse.User.current().get('name') + ' <b class="caret"></b></a>';
    menu += '  <ul class="dropdown-menu">';    
    menu += '    <li><a href="profile_edit.html">Edit Profile</a></li>';    
    menu += '    <li class="divider"></li>';
    menu += '    <li><a href="#" onclick="doLogout()">Logout</a></li>';
    menu += '  </ul>';
    menu += '</li>';
  }
  else
  {
    menu += '<li id="option_signin"><a href="signin.html">Sign In</a></li>';  
  }
  
  $("#menu").html(menu);

}

function getUrlVars()
{
  var vars = [], hash;
  var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
  for(var i = 0; i < hashes.length; i++)
  {
    hash = hashes[i].split('=');
    vars.push(hash[0]);
    vars[hash[0]] = hash[1];
  }
  return vars;
}

function showDateWithFormat(date)
{
  d = new Date(date);
  return (d.getMonth() + 1) + "/" + d.getDate() + "/" + d.getFullYear();
}

//Klimbz Web

function addFeedback(app_id)
{

  bootbox.prompt("Let us know what you think so far.", function(result) {                
    if (result === null) {                                             
                               
    } else {
      var Feedback = Parse.Object.extend("Feedback");

      var feedback = new Feedback();

      var app = new Parse.Object("Application");
      app.id = app_id;

      feedback.set("application", app);
      feedback.set("varTrace", "\""+JSONify(navigator)+"\"");
      feedback.set("classCaller", window.location.href);
      feedback.set("platformType", navigator.platform);
      feedback.set("description", result);
      feedback.set("isActive", true);
       
      // This will save both myPost and myComment
      feedback.save().then(function(feedback) {

      }, function(error) {
      });
    }
  });

}

function startKlimbz(app_id)
{

  html = "<div class=\"row early-access\"><center>You are using an unlimited account as part of the early access program. <a href=\"javascript:addFeedback('"+app_id+"')\">Give us your feedback here.</a></center></div>";
  
  var body   = document.body || document.getElementsByTagName('body')[0];
  if (body.id == "home" || body.id == "signin" || body.id == "signup") { return; }
  div = document.createElement('div');
  div.innerHTML = html;
  body.insertBefore(div,body.childNodes[0]);

}

function JSON_object(key, value) {    
    return "\""+key+"\":\""+value+"\"";
}

function JSONify()
{
  json = "{";
  first = true;
  for(var key in navigator) {
    if (!first) { json = json + ","; } else {first = false; }
    json = json + JSON_object(key,navigator[key]);

  }
  json = json + "}";
  return json;
}


/* GLOBAL */
$( document ).ready(function() {
  
  $.getScript("includes/clean/js/bootstrap.min.js", function(){});
  $.getScript("includes/react/js/theme.js", function(){});
  $.getScript("includes/formance/js/jquery.formance.min.js", function(){});
  $.getScript("includes/misc/bootbox.min.js", function(){});

  createMenu();

  if (!hasSessionActive()) { 
    $(".logged-in").css("display","none"); 
  } 
  else {     
    startKlimbz("PANjzM9Fpw"); 
    $(".logged-in").css("display","");  
    $(".logged-in").removeClass("hidden");  
  }

});

