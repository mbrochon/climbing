<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Comment extends CI_Controller {

	public function __construct()
  {
    parent::__construct();
    
    $this->load->driver('cache');

    //if ($this->utils->get_logged_user() == null) redirect('site/login');
  }

  function ResizeToDimension($dimension, $source, $extension, $destination)
  {

    //get the image size
    $size = getimagesize($source);

    //determine dimensions
    $width = $size[0];
    $height = $size[1];

    //determine what the file extension of the source
    //image is
    switch($extension)
    {

      //its a gif
      case 'gif': case 'GIF':
        //create a gif from the source
        $sourceImage = imagecreatefromgif($source);
        break;
      case 'jpg': case 'JPG': case 'jpeg':
        //create a jpg from the source
        $sourceImage = imagecreatefromjpeg($source);
        break;
      case 'png': case 'PNG':
        //create a png from the source
        $sourceImage = imagecreatefrompng($source);
        break;

    }

    // find the largest dimension of the image
    // then calculate the resize perc based upon that dimension
    $percentage = ( $width >= $height ) ? 100 / $width * $dimension : 100 / $height * $dimension;

    // define new width / height
    $newWidth = $width / 100 * $percentage;
    $newHeight = $height / 100 * $percentage;

    // create a new image
    $destinationImage = imagecreatetruecolor($newWidth, $newHeight);

    // copy resampled
    imagecopyresampled($destinationImage, $sourceImage, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);

      //exif only supports jpg in our supported file types
      if ($extension == "jpg" || $extension == "jpeg")
    {

      //fix photos taken on cameras that have incorrect
      //dimensions
      $exif = exif_read_data($source);

      //get the orientation
      if (isset($exif['Orientation'])) {
        
        $ort = $exif['Orientation'];

      //determine what oreientation the image was taken at
      switch($ort)
        {

            case 2: // horizontal flip

                $this->ImageFlip($dimg);

              break;

            case 3: // 180 rotate left

                $destinationImage = imagerotate($destinationImage, 180, -1);

              break;

            case 4: // vertical flip

                $this->ImageFlip($dimg);

              break;

            case 5: // vertical flip + 90 rotate right

                $this->ImageFlip($destinationImage);

                $destinationImage = imagerotate($destinationImage, -90, -1);

              break;

            case 6: // 90 rotate right

                $destinationImage = imagerotate($destinationImage, -90, -1);

              break;

            case 7: // horizontal flip + 90 rotate right

                $this->ImageFlip($destinationImage);

                $destinationImage = imagerotate($destinationImage, -90, -1);

              break;

            case 8: // 90 rotate left

                $destinationImage = imagerotate($destinationImage, 90, -1);

              break;

        }
        
      }
      

    }

    // create the jpeg
    return imagejpeg($destinationImage, $destination, 100);

  }

  function upload_image()
  {

    if (!isset($_FILES['image'])) {
        return "";
    }

    $source_file = $_FILES['image']['tmp_name']; 

    $temp_file = tempnam(sys_get_temp_dir(), 'klimbz');

    $this->ResizeToDimension(500,  $source_file, "jpg", $temp_file);

    $logged_in_user = $this->utils->get_logged_user();
    $this->load->library('s3');

    $filename = '';

    for ($i=0; $i<20; $i++) { 
        $d=rand(1,30)%2; 
        $filename .= $d ? chr(rand(65,90)) : chr(rand(48,57)); 
    }
        
    $filename = $filename . ".jpg";       
    
    $bucket_name = 'klimbz-images';     
    $bucket_response = $this->s3->putBucket($bucket_name, 'public-read');    
    $image_url = 'http://'. $bucket_name . '.s3.amazonaws.com/' . $filename;
    $input = $this->s3->inputFile($temp_file);
    
    $response = $this->s3->putObject($input, $bucket_name, $filename, 'public-read');
    
    return $image_url;

  }

  public function create()
  {
    
    $this->load->model('User_model');  
    $this->load->model('Comment_model');  
    $this->load->model('Color_model');  
    $this->load->model('Wall_model');  
    $this->load->model('Gym_model'); 

    $logged_in_user = $this->utils->get_logged_user();    

    $text = $this->input->post('text');
    $conversation_id = $this->input->post('conversation_id');
    $caller_id = $this->input->post('caller_id');
    $conversation_type_id = $this->input->post('conversation_type_id');
    $gym_id = $this->input->post('gym_id');
    
    $user = $this->User_model->get($logged_in_user->id);
    //$is_setter = $user->is_setter_at_gym($gym_id);
   
    $comment = new Comment_model();
    $comment->text = $text;
    
    if ($_FILES['image']['size'] > 0) {
      $image_url = $this->upload_image();
      $comment->media_url = $image_url;
    }
    else
    {
      $comment->media_url = $this->input->post('media_link'); 
    }

    $comment->created_by = $logged_in_user->id;      
    $comment->conversation_id = $conversation_id;
    $comment->create();

    $url='gym/view/'.$caller_id;
    
    if ($conversation_type_id == CONVERSATION_TYPE_WALL) {
      $url='wall/view/'.$caller_id;  
    }
    else if ($conversation_type_id == CONVERSATION_TYPE_ROUTE) {
      $url='route/view/'.$caller_id;  
    }
    else if ($conversation_type_id == CONVERSATION_TYPE_USER) {
      $url='user/profile/'.$caller_id;  
    }
    
    $this->send_email_to_setters_users($user,$comment,$gym_id,$url);
    
    redirect($url);  
    

  }

  public function send_email_to_setters_users($user,$comment,$gym_id,$url){
    
    
    $users =  $this->User_model->get_all_setters($gym_id);
    $email =null;
    $bcc = null;
        foreach ($users as $userRow):
            if ($userRow->email != $user->email){
               if ($email==null){
                    $email= $userRow->email;
               }
               else { 
                  $bcc = ($bcc == null )?$userRow->email:$bcc.', '. $userRow->email;
               }
            }   
        endforeach;       

        $link_text = "View full comment on the website";
        if ($comment->media_url){
            $link_text.=' (Image attached)';
        }
        //send email to beta user
        $message_data = array(
          "page_title" => "Klimbz",
          "title" =>$user->name.' ('.$user->email .') commented on the Klimbz website.',
          "description" => '"'.$comment->text.'"',
          "link" => site_url() .$url,
          "link_text" => $link_text 
        );

        $message = $this->load->view('/mails/app_communications',$message_data,true);  
        $subject = $user->name.' commented on the Klimbz website.';
        //$this->utils->send_mail('maxibrochon@hotmail.com', $subject, $message, $bcc);
        if ($email !=null) {
            $this->utils->send_mail($email, $subject, $message, $bcc);
        }
      }
      
  
}

/* End of file user.php */
/* Location: ./application/controllers/user.php */