<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site extends CI_Controller {

	var $captchas;

	function __construct(){
        parent::__construct(); // needed when adding a constructor to a controller        
    }  

	/*
	* Loads landing page
	*/
	public function index()
	{

		$logged_in_user = $this->utils->get_logged_user();

    	if(isset($logged_in_user))
    	{
    		redirect('user/dashboard');
    	}
		else
		{	
			$data = array();

			$this->custom_loader->load_app_view('Klimbz','site/home',$data);
		}
		
	}
	
	public function signup($gym_id = null, $gym_sha = null, $error =null)
	{
		
		$data = array();
                if ($gym_sha != null && $gym_sha != 0){
		if (isset($gym_id)) {
			 $this->load->model('Gym_model');

			 if (md5($gym_id . URL_SALT) != $gym_sha) {
			 	redirect('site/signup');
			 }

			 $application = $this->Gym_model->get($gym_id);
			 $data['gym'] = $application;
                         $data['gym_sha'] = $gym_sha;
                         
		}
                }
                if ($error!=null)
                    if ($error =='nullpass')
                        $data['error'] = 'Please enter your password.';  
                    else if ($error =='nullname')
                        $data['error'] = 'Please enter your name.';  
                        else if ($error =='existingaccount')
                            $data['error'] = 'The email account already exists. Please enter another email account.';  
		$this->custom_loader->load_app_view('Klimbz','site/signup', $data);	

	}

	public function request($code = null)
	{
		$this->custom_loader->load_app_view('Klimbz','site/early_access');	
	}

	public function login($error=null)
	{
            $data = null;	
            $logged_in_user = $this->utils->get_logged_user();
            if(isset($logged_in_user))
                {
                        redirect("user/dashboard");
                }
            else
		{
                   if ($error != null){ 
                      $data = array('error'=>"The E-Mail address or password are not valid");
                   }
                   $this->custom_loader->load_app_view('Klimbz | Login','site/login',$data);
		}
	}

	public function forgot($error=null)
	{
		//Add params to show in the view
		$this->custom_loader->load_app_view('Klimbz | Reset Password','site/forgot');
	}

	public function thanks($error=null)
	{
		//Add params to show in the view
		$this->custom_loader->load_app_view('Klimbz | Reset Password','site/thanks');
	}

	public function password_reset($code=null)
	{		
		$data = array("code"=>$code);		
		$this->custom_loader->load_app_view('Klimbz | Reset Password','site/password_reset', $data);
	}

	public function terms($error=null)
	{
		//Add params to show in the view
		$this->custom_loader->load_site_view('Klimbz | Terms of Service','site/terms');
	}

	public function about($error=null)
	{
		//Add params to show in the view
		$this->custom_loader->load_site_view('Klimbz | About','site/about');
	}

	public function privacy($error=null)
	{
		//Add params to show in the view
		$this->custom_loader->load_site_view('Klimbz | Privacy Policy','site/privacy');
	}

	public function logout()
	{
		$this->session->unset_userdata('user');
        redirect('/site/login');
	}

	

	public function home()
	{		
	    //Show Loading Page
	    $this->custom_loader->load_app_view('Klimbz','site/home',array());
	}

	//Method to test email templates in browser
	public function mail($mail_template, $title, $description)
	{		
	    
	    $message_data = array(
        	"page_title" => "Kloc",
        	"title" => urldecode($title),
        	"description" => urldecode($description),
        	"link" => "http://kloc.me",
        	"link_text" => "Login Now"
      	);

	    $this->custom_loader->load_app_view('Kloc | Template','mails/'.$mail_template,$message_data);
	}
}

/* End of file site.php */
