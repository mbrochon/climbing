<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->database();
		$this->load->helper('url');
		
		$this->load->library('grocery_CRUD');	

		if ($this->utils->get_logged_user() == null) 
		{
			redirect('site');
		}
		else
		{
			$user = $this->utils->get_logged_user();
			
			if (!preg_match(REGEX_ADMIN_EXCEPTION, $user->email)) {
				redirect('site');
			}
		}

	}
	
	function _admin_output($output = null)
	{
		$this->load->view('admin.php',$output);	
	}	
	
	function index()
	{
		$this->_admin_output((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
	}	

	function users()
	{
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('users');
			$crud->columns('email','name','created_on','password');

			$crud->set_subject('User');
			
			$output = $crud->render();
			
			$this->_admin_output($output);
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	function grades()
	{
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('grades');

			$crud->set_subject('Grade');
			
			$output = $crud->render();
			
			$this->_admin_output($output);
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	function colors()
	{
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('colors');
			
			$crud->set_subject('Color');
			
			$output = $crud->render();
			
			$this->_admin_output($output);
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	function gyms()
	{
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('gyms');
			
			$crud->set_subject('Gym');
			
			$output = $crud->render();
			
			$this->_admin_output($output);
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	function gym_setters()
	{
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('gym_setters');
			
			$crud->set_relation('gym_id','gyms','name');
			$crud->display_as('gym_id','Gym');
			
			$crud->set_relation('user_id','users','name');
			$crud->display_as('user_id','Setter');

			$crud->set_subject('Gym Setters');
			
			$output = $crud->render();
			
			$this->_admin_output($output);
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	function gym_links()
	{
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('gym_links');
			
			$crud->set_relation('gym_id','gyms','name');
			$crud->display_as('gym_id','Gym');
			
			$crud->set_subject('Gym Links');
			
			$output = $crud->render();
			
			$this->_admin_output($output);
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	function walls()
	{
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('walls');
			$crud->set_subject('Wall');
			
			$crud->set_relation('gym_id','gyms','name');
			$crud->display_as('gym_id','Gym');

			$output = $crud->render();
			
			$this->_admin_output($output);
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	function routes()
	{
		try{
			$crud = new grocery_CRUD();

			$crud->set_theme('datatables');
			$crud->set_table('routes');
			$crud->set_subject('Route');
			
			$crud->set_relation('wall_id','walls','name');
			$crud->display_as('wall_id','Wall');

			$crud->set_relation('color_id','colors','name');
			$crud->display_as('color_id','Color');

			$crud->set_relation('grade_id','grades','name');
			$crud->display_as('grade_id','Grade');

			$crud->set_relation('created_by','users','name');
			$crud->display_as('created_by','Setter');

			$crud->change_field_type('is_archived', 'true_false');

			$output = $crud->render();
			
			$this->_admin_output($output);
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
}