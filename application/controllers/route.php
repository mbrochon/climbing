<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Route extends CI_Controller {

	public function __construct()
  {
    parent::__construct();
    $this->load->model('Route_model');    
    $this->load->driver('cache');

    if ($this->utils->get_logged_user() == null) redirect('site/login');
  }

  public function view($id)
  {
    $this->load->model('Gym_model');  
    $this->load->model('Wall_model');  
    $this->load->model('Conversation_model');
    $this->load->model('Comment_model');
    $this->load->model('User_model');  
    
    $logged_in_user = $this->utils->get_logged_user();

    $route = $this->Route_model->get($id, $logged_in_user->id);
    $wall = $this->Wall_model->get($route->wall_id);
    $gym = $this->Gym_model->get($wall->gym_id);    
    $attempts = $this->Route_model->get_attempts($id, $logged_in_user->id);
    $climb_types = $this->Route_model->get_climb_types();
    if ($route) {      
      $data = array();  
      $data['wall'] = $wall;
      $data['gym'] = $gym;
      $data['route'] = $route;
      $data['user'] = $logged_in_user;
      $data['attempts'] = $attempts;
      $user = $this->User_model->get($logged_in_user->id);
      $data['is_setter'] = $user->is_setter_at_gym($gym->id);
      $data['climb_types'] = $climb_types;
      $conversation = new Conversation_model();
      $conversation->conversation_type_id = CONVERSATION_TYPE_ROUTE;
      $conversation->parent_id = $route->id;

      $conversation = $conversation->get_by_type_and_parent();
      $comments = $this->Comment_model->get_by_conversation($conversation->id);

      $data['conversation'] = $conversation;
      $data['comments'] = $comments;

      $this->custom_loader->load_app_view('Klimbz','app/route/view',$data);  
    }

  }

  public function tick($id)
  {
    $this->load->model('User_model');  
    $logged_in_user = $this->utils->get_logged_user();
    $route = $this->Route_model->get($id);
    $climb_types = $this->Route_model->get_climb_types();

    $data['route'] = $route;
    $data['climb_types'] = $climb_types;

    $this->custom_loader->load_app_view('Klimbz','app/_common/tick_route',$data);  

  }

  public function add_to_profile($id)
  {
        
    
    $this->load->model('User_model');  
    $logged_in_user = $this->utils->get_logged_user();
    $route = $this->Route_model->get($id);

    $user = $this->User_model->get($logged_in_user->id);

    $data = array();

    $newDate = DateTime::createFromFormat("m-d-Y", $this->input->post('climbed_on'));
    if ($newDate)
     $newDate = $newDate->format('Y-m-d H:i:s');
    else 
     $newDate = date("Y-m-d H:i:s");    
    $data['climbed_on'] = $newDate;
    $data['climb_type'] = $this->input->post('climb_type');
    $data['repeat'] = ($this->input->post('repeat') == "on") ? 1 : 0;
    $data['route_id'] = $route->id;

    $user->add_route($data);

    $value = $this->input->post("rating_value");
    if ($value !=null && $value>0 ){
        $route->rate($user->id, $value);
    }
    redirect($this->input->post('from'));

  }

  public function add_attempt($id)
  {
    
    $this->load->model('User_model');  
    $logged_in_user = $this->utils->get_logged_user();
    $route = $this->Route_model->get($id);

    $user = $this->User_model->get($logged_in_user->id);
    $user->add_route_attempt($route->id, $this->input->post('notes'));

    redirect($this->input->post('from'));

  }

  public function rate()
  {

    $this->load->model('User_model');  
    $logged_in_user = $this->utils->get_logged_user();
    $route = $this->Route_model->get($this->input->post("route_id"));

    $user = $this->User_model->get($logged_in_user->id);
    $route->rate($user->id, $this->input->post("rating_value"));

    echo json_encode(array('success' => true));
  }

  public function archive($id)
  {
    $this->load->model('User_model');  
    $route = $this->Route_model->get($id);
    
    $logged_in_user = $this->utils->get_logged_user();

    if ($route) {      

      $user = $this->User_model->get($logged_in_user->id);    
       
      $this->Route_model->archive_route($id);

      redirect('wall/view/' . $route->wall_id);

    }

  }

  public function delete($id)
  {
    
    $route = $this->Route_model->get($id);
    
    $logged_in_user = $this->utils->get_logged_user();

    if ($route) {      
       
      $route->delete();

      redirect('wall/view/' . $route->wall_id);

    }

  }

  public function create($wall_id)
  {
    

    $this->load->model('User_model');  
    $this->load->model('Grade_model');  
    $this->load->model('Color_model');  
    $this->load->model('Wall_model');  

    $logged_in_user = $this->utils->get_logged_user();
    $wall = $this->Wall_model->get($wall_id);

    $grade = $this->input->post('grade');

    if ($grade != "") {
      
      $route = new Route_model();
      
      $color = new Color_model();
      $color->name = trim($this->input->post('color'));
      $color = $color->get();

      $setter = new User_model();
      $setter->name = trim($this->input->post('setter'));
      $setter->gym_id = $wall->gym_id;
      $setter = $setter->get_setter();

      $route->grade_id = $this->input->post('grade');
      $route->color_id = $color->id;
      $route->created_by = $setter->id;
      $route->wall_id = $wall->id;
      $route->create();

      redirect('wall/view/'.$wall->id);
    }
    else
    {
      $data = array();

      $data['user'] = $this->User_model->get($logged_in_user->id);
      
      $data['wall'] = $wall;            
      $data['grades'] = $this->Grade_model->get_all();            
      $data['colors'] = $this->Color_model->get_all();
      $data['setters'] = $this->User_model->get_all_setters($wall->gym_id);

      $this->custom_loader->load_app_view('Klimbz','app/route/create',$data);
    }

  }

  public function hidecomment($id)
  {
     $this->load->model('Comment_model');
    $this->load->model('User_model');  
    
    $logged_in_user = $this->utils->get_logged_user();
    $gym_id = ($this->input->post('gym_id'))?$this->input->post('gym_id'):0;
    $route_id = ($this->input->post('route_id'))?$this->input->post('route_id'):0;
  
    $user = $this->User_model->get($logged_in_user->id);
    
    $is_setter = $user->is_setter_at_gym($gym_id);
      
    if ($is_setter){
        $comment = $this->Comment_model->get_by_id_and_gym_id($id,$gym_id);
        if ($comment == null)
            $error = 'This action is forbidden, please check that the comments belong to the Gym which you are setter';
        else {
            $is_hidden = ($comment->is_hidden==1)?null:1;
            $comment->is_hidden = $is_hidden;
            $this->Comment_model->update_comment($comment);
        }
    }

    redirect('route/view/'.$route_id);
    
    }

     public function deletecomment($id)
  {
     $this->load->model('Comment_model');
    $this->load->model('User_model');  
    
    $logged_in_user = $this->utils->get_logged_user();
    $gym_id = ($this->input->post('gym_id'))?$this->input->post('gym_id'):0;
    $route_id = ($this->input->post('route_id'))?$this->input->post('route_id'):0;
  
    $user = $this->User_model->get($logged_in_user->id);
    
    $is_setter = $user->is_setter_at_gym($gym_id);
      
    if ($is_setter){
        $comment = $this->Comment_model->get_by_id_and_gym_id($id,$gym_id);
        if ($comment == null)
            $error = 'This action is forbidden, please check that the comments belong to the Gym which you are setter';
        else {
           $this->Comment_model->delete_comment($comment);
        }
    }

    redirect('route/view/'.$route_id);
    
    }
}

/* End of file user.php */
/* Location: ./application/controllers/user.php */