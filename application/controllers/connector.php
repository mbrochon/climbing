<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Connector extends CI_Controller {

	public function __construct()
  {
    parent::__construct();
    $this->load->model('Application_model');    
    $this->load->model('Connector_model');    
    $this->load->driver('cache');

    if ($this->utils->get_logged_user() == null) redirect('site/login');
  }

  public function create($id)
  {

    $connector = $this->input->post('connector');

    $logged_in_user = $this->utils->get_logged_user();
    $application = $this->Application_model->get($id, $logged_in_user);


    if ($connector != "") {
      $token = $this->input->post('token');
      $destination = $this->input->post('destination');
      $connector = $this->Connector_model->get($connector);

      $application->add_connector($connector, $token, $destination, $logged_in_user);

      redirect("app/view/" . $application->id);

    }
    else
    {
      $data = array();    
      $data['connectors'] = $this->Connector_model->get_all();
      $data['application'] = $application;
      $this->custom_loader->load_app_view('Klimbz','app/connector/create',$data);  
    }
    
  }

  public function delete($application_id, $connector_id)
  {
    $this->load->model('Input_model');
    $this->load->model('Metadata_model');

    $logged_in_user = $this->utils->get_logged_user();
    $application = $this->Application_model->get($application_id, $logged_in_user);
    $connector = $this->Connector_model->get($connector_id);

    if ($application) {      
      $application->delete_connector($connector);
      redirect("app/view/" . $application->id);
    }

  }

}

/* End of file user.php */
/* Location: ./application/controllers/user.php */