<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
  {
    parent::__construct();
    $this->load->model('User_model');    
    $this->load->driver('cache');


  }

  function upload_profile_picture()
  {
    
    //Resizing the image
    $image = new SimpleImage(); 
    $image->load($_FILES['image']['tmp_name']); 
    $image->resizeToWidth(500); 

    $temp_file = tempnam(sys_get_temp_dir(), 'klimbz');
    $image->save($temp_file);
    
    $logged_in_user = $this->utils->get_logged_user();
    $this->load->library('s3');

    $filename = '';

    for ($i=0; $i<20; $i++) { 
        $d=rand(1,30)%2; 
        $filename .= $d ? chr(rand(65,90)) : chr(rand(48,57)); 
    }

    $filename = $filename . ".jpg";       
    
    $bucket_name = 'klimbz-images';     
    $bucket_response = $this->s3->putBucket($bucket_name, 'public-read');    
    $image_url = 'http://'. $bucket_name . '.s3.amazonaws.com/' . $filename;

    $input = $this->s3->inputFile($temp_file);
    
    $response = $this->s3->putObject($input, $bucket_name, $filename, 'public-read');
    
    $logged_in_user = $this->utils->get_logged_user();
    
    $user = $this->User_model->get($logged_in_user->id);

    $user->profile_picture_url =  $image_url;

    unset($user->password);    
    $user->update();    
    $this->session->set_userdata('user', json_encode($user));

    redirect('user/edit');

  }

  public function profile($user_id)
  {
   
    $this->load->model('Gym_model');
    $this->load->model('Statistic_model');

    $profile_user = $this->User_model->get_by_id($user_id);

    if(isset($profile_user))
    {
      $data = array();

      $user = $this->User_model->get($profile_user->id);
      $last_climbs = $this->Statistic_model->last_climbs($profile_user->id, null);
      $all_climbs = $this->Statistic_model->user_climbs($profile_user->id, null);

      //Calculating recent achievement
      $max = 0;
      foreach ($last_climbs as $climb) {
        if ($climb->grade_value > $max) {
          $max = $climb->grade_value;
        }
      }

      $gyms = $this->Gym_model->get_all();

      $data['user'] = $user;
      $data['gyms'] = $gyms;      
      $data['gyms_managed'] = $this->Gym_model->managed_by_user($profile_user->id);
      $data['stats_grades'] = $this->Statistic_model->grades_stats($profile_user->id, null);
      $data['last_climbs'] = $last_climbs;
      $data['recent_achievement'] = $max;
      $data['all_climbs'] = $all_climbs;

     // print_r($data);

      $this->custom_loader->load_app_view('Klimbz','app/user/profile',$data);
    }
    else
    { 
      redirect("site/login");
    }
  }

  public function api_climbs()
  {
   
    $this->load->model('Gym_model');
    $this->load->model('Statistic_model');

    $timestamp = $this->input->get("timestamp");
    $user_id = $this->input->get("user_id");
    
    header("application-type:json");
    $all_climbs = $this->Statistic_model->user_climbs_by_date($user_id, date("Y/m/d", $timestamp));
    echo json_encode($all_climbs);
    
  }

  public function dashboard($gym_id = null)
  {
    
    if ($this->utils->get_logged_user() == null) redirect('site/login');

    $this->load->model('Gym_model');
    $this->load->model('Statistic_model');

    $logged_in_user = $this->utils->get_logged_user();

    if(isset($logged_in_user))
    {
      $data = array();

      $user = $this->User_model->get($logged_in_user->id);
      $last_climbs = array();
      $all_climbs = array();

      if ($gym_id == "all") {
        $last_climbs = $this->Statistic_model->last_climbs($logged_in_user->id, null);
        $all_climbs = $this->Statistic_model->user_climbs($logged_in_user->id, null);
      }
      else if ($gym_id == null)
      {
         $last_climbs = $this->Statistic_model->last_climbs($logged_in_user->id, null);
         if (count($last_climbs) > 0) {
            $gym_id = $last_climbs[0]->gym_id;

            redirect('user/dashboard/' . $gym_id);

         }
         
      }
      else        
      {
        $last_climbs = $this->Statistic_model->last_climbs($logged_in_user->id, $gym_id);
        $all_climbs = $this->Statistic_model->user_climbs($logged_in_user->id, $gym_id);        
      }

      

      //$last_climbs = $this->Statistic_model->last_climbs($logged_in_user->id, $gym_id);
      

      //Calculating recent achievement
      $max = 0;
      foreach ($last_climbs as $climb) {
        if ($climb->grade_value > $max) {
          $max = $climb->grade_value;
        }
      }


      $gyms = $this->Gym_model->get_all();

      $data['gym_selected'] = $gym_id;
      $data['user'] = $user;
      $data['gyms'] = $gyms;      
      $data['gyms_managed'] = $this->Gym_model->managed_by_user($logged_in_user->id);
      $data['stats_grades'] = $this->Statistic_model->grades_stats($logged_in_user->id, ($gym_id == "all")?null:$gym_id);
      $data['last_climbs'] = $last_climbs;
      $data['recent_achievement'] = $max;
      $data['all_climbs'] = $all_climbs;
      
      if (isset($_GET['welcome'])) {
        $data['welcome'] = "";
      }
      

      $this->custom_loader->load_app_view('Klimbz','app/user/dashboard',$data);
    }
    else
    { 
      redirect("site/login");
    }
  }

  public function upgrade($application_id = null)
  { 
    $plan = $this->input->post("plan");
    $logged_in_user = $this->utils->get_logged_user();

    if ($plan != "") {

      $message_data = array(
        "page_title" => "Klimbz",
        "title" => "Thanks " . $logged_in_user->name . "!",
        "description" => "You will be contacted by one of our account managers to finish the Upgrade process to the ". strtoupper($plan) ." plan."      
      );
      
      $message = $this->load->view('/mails/app_communications',$message_data,true);  
      $subject = "[Klimbz] Upgrading your account";

      $this->utils->send_mail($logged_in_user->email, $subject, $message);
      $this->utils->send_mail(ADMIN_EMAIL, $subject, $message);

      if ($application_id != null) {
        $this->load->model('Application_model');
        $application = $this->Application_model->get($application_id, null);
        redirect('app/view/'.$application->id.'/1');
      }      
      else
      {
        redirect('user/dashboard');  
      }
      
    }
    else
    {
      $data = array();
      $this->load->model('Application_model');
      $application = $this->Application_model->get($application_id, null);        
      $data['application'] = $application;

      $this->custom_loader->load_app_view('Klimbz','app/user/upgrade',$data);  
    }
    
  }

  public function create()
  {
    $error = null;
    $user = new User_model();
    $gym_id = ($this->input->post('gym'))?$this->input->post('gym'):0;
    $gym_sha = ($this->input->post('gym_sha'))?$this->input->post('gym_sha'):0;
    $name = $this->input->post('name');
    $password = $this->input->post('password');
    if ($password =='')
        $error = 'nullpass';
    if ($error == null && trim($name)=='')
        $error = 'nullname';
    
    if ($error == null && $this->User_model->get_by_email($this->input->post('email'))==null)
    {
      $user->name = $this->input->post('name');    
      $user->email = $this->input->post('email');
      $user->password = $this->input->post('password');
      $user->profile_picture_url = "";
      $user = $user->create();

      unset($user->password);
      $this->session->set_userdata('user', json_encode($user));

      $gym_id = $this->input->post('gym');

      if ($gym_id != "") {
        $this->load->model('Gym_model');
        $gym = $this->Gym_model->get($gym_id);
        $gym->add_setter($user->id);
      }

      redirect('user/dashboard?welcome');

    }
    else
    {
     $error = ($error)?$error:'existingaccount';
    }
    redirect("site/signup/$gym_id/$gym_sha/$error");
  
  }


  public function invite($gym_id = null)
  {
    $this->load->model('Gym_model');
    $logged_in_user = $this->utils->get_logged_user();

    $gym = $this->Gym_model->get($gym_id);

    $email = $this->input->post('email');

    if ($email != "") {
      
      $user = $this->User_model->get_by_email($email);

      $name = $this->input->post('name');
 
      if ($user == null) {
 
        $message_data = array(
          "page_title" => "Klimbz",
          "title" => "Hi " . $name . "!",
          "description" => $logged_in_user->name . " invited you to join the gym: " . $gym->name,
          "link" => site_url() ."site/signup/".$gym->id,
          "link_text" => "Join Now"    
        );
        
        $message = $this->load->view('mails/app_communications',$message_data,true);  
        $subject = "[Klimbz] Invitation from " . $logged_in_user->name;

        $this->utils->send_mail($email, $subject, $message);

      }
      else
      {
      
        $message_data = array(
          "page_title" => "Klimbz",
          "title" => "Hi " . $name . "!",
          "description" => $logged_in_user->name . " added you to the gym: " . $gym->name,
          "link" => site_url() ."site/login",
          "link_text" => "Login Now"    
        );
        
        $message = $this->load->view('mails/app_communications',$message_data,true);  
        $subject = "[Klimbz] You have been added to " . $gym->name;

        $this->utils->send_mail($email, $subject, $message);

        $gym->add_setter($user->id);
      }

      redirect('gym/view/'.$gym->id);
      
    }
    else
    {
      $data = array();

      $data['user'] = $this->User_model->get($logged_in_user->id);      
      $data['gym'] = $gym;

      $this->custom_loader->load_app_view('Klimbz','app/user/invite',$data);
    }

  }

  public function edit()
  {
    
    if ($this->utils->get_logged_user() == null) redirect('site/login');

    $logged_in_user = $this->utils->get_logged_user();

    $name = $this->input->post('name');    

    if ($name != "") {
      $user = $this->User_model->get($logged_in_user->id);
      $user->name = $name;
      $user->update();
      unset($user->password);
      $this->session->set_userdata('user', json_encode($user));
      redirect('user/dashboard');
    }
    else
    {
      $data = array();
      $data['user'] = $this->User_model->get($logged_in_user->id);      
      $this->custom_loader->load_app_view('Klimbz','app/user/edit',$data);
    }   

  }

  public function queue()
  {
    $user = new User_model();

    if ($this->User_model->get_by_email($this->input->post('email'))==null)
    {
      $user->name = $this->input->post('name');    
      $user->email = $this->input->post('email');      
      $user->user_status_id = 1;

      $user = $user->create();

      redirect('site/thanks');

    }
    else
    {
      redirect('site/signup');
    }

  }

  public function validate()
  {    
		$user = $this->User_model->validate($this->input->post('email'), $this->input->post('password'));
    
		if (isset($user))
		{			
                unset($user->password);
                $this->session->set_userdata('user', json_encode($user));

                redirect('user/dashboard');
		}
		else
		{
			redirect('site/login/invalidpassword');
		}
  }

  public function reset()
  {
    $email = $this->input->post('email');
    $password = $this->input->post('password');

    if ($this->User_model->get_by_email($email))
    {        
      
      $encoded_mail = md5($email.URL_SALT);

      if ($this->input->post('code') == $encoded_mail) {

          $user = $this->User_model->get_by_email($email);
          $password = $password;
          $salt = '';
          //generate salt----------------------------------->
          for ($i=0; $i<=32; $i++) {
            $d=rand(1,30)%2;
            $salt .= $d ? chr(rand(65,90)) : chr(rand(48,57));
          }
          //end generate salt--------------------------------|
          
          //hash password with salt-->
          $hashed = md5($password . $salt);     
          //here is your new encrypted password, ready to store in the database table,  `jos_users`
          $encrypted = $hashed . ':' . $salt;
          $user->password = $encrypted;

          $user->update_password();

          redirect('site/login/');
      }
    }
    else
    {
      //invalid
      redirect('/site/forgot/error');
    }
  }

  public function forgot()
  {
    $email = $this->input->post('email');    

    if ($this->User_model->get_by_email($email))
    {        
      $encoded_mail = md5($email.URL_SALT);
      //send email to beta user
      $message_data = array(
        "page_title" => "Klimbz",
        "title" => "Forgotten Password",
        "description" => "Proceed by clicking on the button below.",
        "link" => site_url() ."site/password_reset/".$encoded_mail,
        "link_text" => "Reset Password"    
      );
      
      $message = $this->load->view('/mails/app_communications',$message_data,true);  
      $subject = "Forgotten Password.";

      $this->utils->send_mail($this->input->post('email'), $subject, $message);

      redirect('site/login/');
    }
    else
    {
      //invalid
      redirect('site/forgot/error');
    }
  }


}

/* End of file user.php */
/* Location: ./application/controllers/user.php */