<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Wall extends CI_Controller {

	public function __construct()
  {
    parent::__construct();
    $this->load->model('Wall_model');    
    $this->load->driver('cache');

    $config['upload_path'] = './uploads/';
    $config['allowed_types'] = 'gif|jpg|png';
    $config['max_size'] = '0';
    $config['max_width']  = '0';
    $config['max_height']  = '0';

    $this->load->library('upload', $config);

    if ($this->utils->get_logged_user() == null) redirect('site/login');
  }

  function ResizeToDimension($dimension, $source, $extension, $destination)
  {

    //get the image size
    $size = getimagesize($source);

    //determine dimensions
    $width = $size[0];
    $height = $size[1];

    //determine what the file extension of the source
    //image is
    switch($extension)
    {

      //its a gif
      case 'gif': case 'GIF':
        //create a gif from the source
        $sourceImage = imagecreatefromgif($source);
        break;
      case 'jpg': case 'JPG': case 'jpeg':
        //create a jpg from the source
        $sourceImage = imagecreatefromjpeg($source);
        break;
      case 'png': case 'PNG':
        //create a png from the source
        $sourceImage = imagecreatefrompng($source);
        break;

    }

    // find the largest dimension of the image
    // then calculate the resize perc based upon that dimension
    $percentage = ( $width >= $height ) ? 100 / $width * $dimension : 100 / $height * $dimension;

    // define new width / height
    $newWidth = $width / 100 * $percentage;
    $newHeight = $height / 100 * $percentage;

    // create a new image
    $destinationImage = imagecreatetruecolor($newWidth, $newHeight);

    // copy resampled
    imagecopyresampled($destinationImage, $sourceImage, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);

      //exif only supports jpg in our supported file types
      if ($extension == "jpg" || $extension == "jpeg")
    {

      //fix photos taken on cameras that have incorrect
      //dimensions
      $exif = exif_read_data($source);

      //get the orientation
      if (isset($exif['Orientation'])) {
        
        $ort = $exif['Orientation'];

      //determine what oreientation the image was taken at
      switch($ort)
        {

            case 2: // horizontal flip

                $this->ImageFlip($dimg);

              break;

            case 3: // 180 rotate left

                $destinationImage = imagerotate($destinationImage, 180, -1);

              break;

            case 4: // vertical flip

                $this->ImageFlip($dimg);

              break;

            case 5: // vertical flip + 90 rotate right

                $this->ImageFlip($destinationImage);

                $destinationImage = imagerotate($destinationImage, -90, -1);

              break;

            case 6: // 90 rotate right

                $destinationImage = imagerotate($destinationImage, -90, -1);

              break;

            case 7: // horizontal flip + 90 rotate right

                $this->ImageFlip($destinationImage);

                $destinationImage = imagerotate($destinationImage, -90, -1);

              break;

            case 8: // 90 rotate left

                $destinationImage = imagerotate($destinationImage, 90, -1);

              break;

        }
        
      }
      

    }

    // create the jpeg
    return imagejpeg($destinationImage, $destination, 100);

  }

  function convert_data_url($data_url, $filename) {
    // Assumes the data URL represents a JPEG image
    $image = base64_decode( str_replace('data:image/jpeg;base64,', '', $data_url));
    $fp = fopen($filename, 'w');
    fwrite($fp, $image);
    fclose($fp);
  }


  function upload_image($wall_id)
  {

    $source_file = $_FILES['image']['tmp_name']; 

    $temp_file = tempnam(sys_get_temp_dir(), 'klimbz');

    $this->ResizeToDimension(500,  $source_file, "jpg", $temp_file);

    $logged_in_user = $this->utils->get_logged_user();
    $this->load->library('s3');

    $filename = '';

    for ($i=0; $i<20; $i++) { 
        $d=rand(1,30)%2; 
        $filename .= $d ? chr(rand(65,90)) : chr(rand(48,57)); 
    }
        
    $filename = $filename . ".jpg";       
    
    $bucket_name = 'klimbz-images';     
    $bucket_response = $this->s3->putBucket($bucket_name, 'public-read');    
    $image_url = 'http://'. $bucket_name . '.s3.amazonaws.com/' . $filename;
    $input = $this->s3->inputFile($temp_file);
    
    $response = $this->s3->putObject($input, $bucket_name, $filename, 'public-read');
    
    $wall = $this->Wall_model->get($wall_id);

    $this->Wall_model->add_image($wall_id, $logged_in_user->id, $image_url);
    
    redirect('wall/view/' . $wall->id);

  }

  public function view($id)
  {
    
    $this->load->model('Gym_model');  
    $this->load->model('Route_model');  
    $this->load->model('User_model');  
    $this->load->model('Conversation_model');
    $this->load->model('Statistic_model');
    $this->load->model('Comment_model');

    $wall = $this->Wall_model->get($id);
    
    $logged_in_user = $this->utils->get_logged_user();
    $gym = $this->Gym_model->get($wall->gym_id);
    $routes = $this->Route_model->get_by_wall($wall->id, 0, $logged_in_user->id);

    if ($wall) {      
      $data = array();  
      $data['wall'] = $wall;
      $data['images'] = $this->Wall_model->get_images($wall->id);

      $data['climb_types'] = $this->Route_model->get_climb_types();
      $data['gym'] = $gym;
      $data['routes'] = $routes;
      $data['archived_routes'] = $this->Route_model->get_by_wall($wall->id, 1);

      $user = $this->User_model->get($logged_in_user->id);
      $data['user'] = $user;
      $data['is_setter'] = $user->is_setter_at_gym($gym->id);

      $data['routes_per_grade'] =  $this->Statistic_model->grades_stats_wall($wall->id);
      
      $conversation = new Conversation_model();
      $conversation->conversation_type_id = CONVERSATION_TYPE_WALL;
      $conversation->parent_id = $wall->id;

      $conversation = $conversation->get_by_type_and_parent();
      $comments = $this->Comment_model->get_by_conversation($conversation->id);

      $data['conversation'] = $conversation;
      $data['comments'] = $comments;

      $this->custom_loader->load_app_view('Klimbz','app/wall/view',$data);  
    }

  }

  public function archive($id)
  {
    
    $wall = $this->Wall_model->get($id);
    
    $logged_in_user = $this->utils->get_logged_user();

    if ($wall) {      
       
      $this->Wall_model->archive_routes($wall->id);

      redirect('wall/view/' . $wall->id);

    }

  }

  
  public function update($id)
  {
    $this->load->model('Metadata_model'); 
    $this->load->model('Log_model'); 

    $logged_in_user = $this->utils->get_logged_user();
    $input = $this->Input_model->get($id);

    if ($input) {    

      $application_id = $input->application_id;      
      
      $input->input_status_id = $this->input->post('input_status');

      $input->assigned_to = $this->input->post('assigned_to');

      $input->update();

      redirect('app/view/' . $application_id);

    }

  }

  public function create($gym_id)
  {
    

    $this->load->model('User_model');  
    $this->load->model('Grade_model');  
    $this->load->model('Color_model');  
    $this->load->model('Wall_model');  
    $this->load->model('Gym_model'); 

    $logged_in_user = $this->utils->get_logged_user();
    $gym = $this->Gym_model->get($gym_id);

    $name = $this->input->post('name');

    if ($name != "") {
      
      $wall = new Wall_model();
      
      $wall->name = $this->input->post('name');
      $wall->wall_type_id = $this->input->post('type');      
      $wall->gym_id = $gym->id;
      $wall->create();

      redirect('gym/view/'.$gym->id);
    }
    else
    {
      $data = array();

      $data['user'] = $this->User_model->get($logged_in_user->id);
      
      $data['gym'] = $gym;            
      $data['types'] = $this->Wall_model->get_types();            
      
      $this->custom_loader->load_app_view('Klimbz','app/wall/create',$data);
    }

  }



}

/* End of file user.php */
/* Location: ./application/controllers/user.php */