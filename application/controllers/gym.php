<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gym extends CI_Controller {

	public function __construct()
  {
    parent::__construct();
    $this->load->model('Gym_model');    
    $this->load->driver('cache');

    if ($this->utils->get_logged_user() == null) redirect('site/login');
  }

  public function index()
  {
    
    $logged_in_user = $this->utils->get_logged_user();

    if(isset($logged_in_user))
    {
      $data = array();

      $gyms = $this->Gym_model->get_all();

      $data['gyms'] = $gyms;
      
      $this->custom_loader->load_app_view('Klimbz','app/gym/view_all',$data);
    }
    else
    { 
      redirect("site/login");
    }
  }


  public function view($id)
  {
    
    $this->load->model('Wall_model');
    $this->load->model('User_model');
    $this->load->model('Statistic_model');
    $this->load->model('Conversation_model');
    $this->load->model('Comment_model');
    $this->load->model('Route_model');

    $logged_in_user = $this->utils->get_logged_user();
    $gym = $this->Gym_model->get($id);

    if ($gym) {      
      $data = array();  
      
      $user = $this->User_model->get($logged_in_user->id);

      $data['gym'] = $gym;
      $data['links'] = $gym->get_links();
      $data['walls'] = $this->Wall_model->get_by_gym($gym->id);      
      $data['user'] = $user;
      $data['is_setter'] = $user->is_setter_at_gym($gym->id);
      $data['top_climbed_routes'] =  $this->Statistic_model->top_climbed_routes($gym->id, 5);
      $data['least_climbed_routes'] =  $this->Statistic_model->least_climbed_routes($gym->id, 5);
      $data['routes_per_grade'] =  $this->Statistic_model->grades_stats_gym($gym->id);
      $data['routes_recent_updates'] =  $this->Route_model->get_updated_recent($gym->id);
       
      foreach ($data['walls'] as $wall) {
        if ($wall->image != "")
        {
          $gym_image = $wall->image;
          break;
        }
      }

      $routes = 0;
      
      foreach ($data['routes_per_grade'] as $route_count) {
        $routes = $routes + $route_count->quantity;
      }

      $data['total_routes'] = $routes;
      $data['gym_image'] = $gym_image;

      $conversation = new Conversation_model();
      $conversation->conversation_type_id = CONVERSATION_TYPE_GYM;
      $conversation->parent_id = $gym->id;

      $conversation = $conversation->get_by_type_and_parent();
      $comments = $this->Comment_model->get_by_conversation($conversation->id);

      $data['conversation'] = $conversation;
      $data['comments'] = $comments;

      $this->custom_loader->load_app_view('Klimbz','app/gym/view',$data);  
    }

  }

  public function moderatecomments($id)
  {
       $this->load->model('User_model');
    
       $logged_in_user = $this->utils->get_logged_user();
       $gym = $this->Gym_model->get($id);
       
       $user = $this->User_model->get($logged_in_user->id);
       $gymId =$gym->id;
       $is_setter= $user->is_setter_at_gym($gymId);
       
       if ($is_setter){
           $gym->moderate_comments = ($gym->moderate_comments==1)?null:1;
           $gym->Gym_model->moderate_comments($gym);
           redirect("gym/view/$gymId");
       }
       
           
  }
}

/* End of file user.php */
/* Location: ./application/controllers/user.php */