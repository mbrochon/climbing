  <? $this->load->view('app/_common/header',null); ?> 

<body id="services">  
  
  <? $this->load->view('app/_common/navigation',null); ?> 

    <div id="grid-second">
      <div class="container">    
        <div class="page-header">
          <div class="row">
            <div class="col-md-1">
              <img src="<?=$this->utils->get_gravatar_user($user)?>"
              alt="" class="img-responsive" width="100%" />
            </div>
            <div class="col-md-11">
              <h4><?=$user->name?></h4>
              <? if (count($last_climbs) > 0): ?>
              <small>
                  Last Session <span class="badge"><?=$this->utils->formatted_date($last_climbs[0]->created_on)?></span>                
                
                  Recent achievement <span class="badge">V<?=$recent_achievement?></span>
                </small>
              <? endif; ?>     
            </div>
          </div>
        </div> 
        <div class="row">
          <? if (count($last_climbs) > 0): ?>
          <div class="col-md-8">
            <? $this->load->view('app/_common/date_chart', array('user'=>$user, 'height' => '170px', 'title' => 'Climbs', 'width' => '100%', 'stats' => $all_climbs)); ?>            
          </div>
          <div class="col-md-4">
            <? if (count($last_climbs) > 0): ?>              
              <? $this->load->view('app/_common/pie_chart', array('height' => '190px', 'title' => 'Grades', 'width' => '100%', 'stats' => $stats_grades)); ?>
            <? endif; ?>   
          </div>
        <? else: ?>
           <div class="col-md-12">
            <h3>No activity registered yet.</h3>            
           </div>
        <? endif; ?>
        </div>  
        <hr/> 
        <div class="row" style="display:none">                            
          
          <div class="col-md-4">
            <h3>Last Session</h3>
            <? if (count($last_climbs) > 0): ?>
              <ul class="list-group">              
              <? foreach ($last_climbs as $climbs) :?>    
                <li class="list-group-item">
                  <span class="badge"><?=$this->utils->formatted_date($climbs->created_on)?></span>
                  <a href="<?=site_url()?>route/view/<?=$climbs->route_id?>"><?=$climbs->color_name?> <?=$climbs->grade_name?></a><br/>
                  <small><?=$climbs->gym_name?></small>
                </li>        
              <? endforeach; ?>
              </ul>
            <? else: ?>
              <h5>No logged activity.</h5>
            <? endif; ?>
          </div>          
        </div>
      </div>
    </div>
        
    

  <? $this->load->view('app/_common/footer',null); ?> 

</body>

</html>