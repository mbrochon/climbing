<? $this->load->view('app/_common/header',null); ?> 

  <body id="features">
    <? $this->load->view('app/_common/navigation',null); ?> 


<div id="contact">
          <div class="container">
              <div class="section_header">
                  <h3>Invitation to join <strong><?=$application->name?></strong></h3>
              </div>
              <div class="row ">
                <div class="col-md-7">
                  <form method="post" action="<?=site_url()?>user/invite/<?=$application->id?>">
                  <div class="form-group">
                      <label for="email" class="control-label">Email</label>
                      
                          <input type="text" class="form-control email" data-formance_algorithm='complex' name="email" id="profile_username" placeholder="Email Address">
                      
                  </div>
                      
                  <div class="form-group">
                      <label for="fullname" class="control-label">Full Name</label>
                      
                          <input type="text" class="form-control" name="name" id="profile_name"  placeholder="Full Name">
                      
                  </div>
                  
                  <div class="form-group">        
                    <button id="btn-signup" type="submit" class="button button-small"><i class="icon-hand-right"></i> Send Invitation</button>
                  </div>
                </form>
                </div>                        
              </div>                    
          </div>
      </div>

    <? $this->load->view('app/_common/footer',null); ?>

  </body>

  </html>
