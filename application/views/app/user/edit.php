<? $this->load->view('app/_common/header',null); ?> 

  <body id="features">
    <? $this->load->view('app/_common/navigation',null); ?> 


<div id="contact">
          <div class="container">
            <div class="row">
            <div class="col-md-12">
              <h2>Profile Edit</h2>              
            </div>
          </div>
            <hr/>              
              <div class="row ">
                <div class="col-md-12">
                  <form class="form-horizontal" method="post" action="<?=site_url()?>user/edit">
                     <fieldset>
              <legend>Personal Information</legend>

                  <div class="form-group">
                      <label for="email" class="col-lg-2 control-label">Email</label>
                       <div class="col-md-4">
                          <input type="text" class="form-control email" disabled data-formance_algorithm='complex' name="email" value="<?=$this->utils->get_logged_user()->email?>" id="profile_username" placeholder="Email Address">
                      </div>
                  </div>
                      
                  <div class="form-group">
                      <label for="fullname" class="col-lg-2 control-label">Full Name</label>
                       <div class="col-md-4">
                          <input type="text" class="form-control" name="name" id="profile_name" value="<?=$this->utils->get_logged_user()->name?>" placeholder="Full Name">
                        </div>
                  </div>

                  

                  <div class="form-group">        
                    <div class="col-lg-10 col-lg-offset-2">
                  <button class="btn btn-default" onclick="window.history.go(-1); return false;">Cancel</button>
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                  </div>
                </fieldset>
                </form>
              </div>  
            </div>
            <hr/>
              <div class="section_header">
                  <h4>Profile Picture</h4>
              </div>
              <div class="row ">
                <div class="col-md-2">
                  <img src="<?=$this->utils->get_gravatar_logged_user()?>" width="180" />
                </div>
                <div class="col-md-10">
                <div class="form-group">
                    <form method="post" enctype="multipart/form-data" action="<?=site_url()?>user/upload_profile_picture">
                      <input type="file" name="image" />
                      <br/>
                      <input type="submit" class="btn btn-primary btn-sm" value="Upload Image" />
                    </form>
                   </div>
                 </div>
                </div>                        
                                
          </div>
      </div>

    <? $this->load->view('app/_common/footer',null); ?>

  </body>

  </html>
