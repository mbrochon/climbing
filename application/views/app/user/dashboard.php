  <? $this->load->view('app/_common/header',null); ?> 

<body id="services">  
  
  <? $this->load->view('app/_common/navigation',null); ?> 

    <div id="grid-second">
      <div class="container">    
        <div class="page-header">
          <div class="row">
            <div class="col-md-1">
              <img src="<?=$this->utils->get_gravatar_logged_user()?>"
              alt="" class="img-responsive" width="100%" />
            </div>
            
            <div class="col-md-11">
              <? if (!isset($welcome)): ?>
           
              <h4><?=$user->name?></h4>
              <? if (count($last_climbs) > 0): ?>
              <small>
                  Last Session <span class="badge"><?=$this->utils->formatted_date($last_climbs[0]->created_on)?></span>                
                
                  Recent achievement <span class="badge">V<?=$recent_achievement?></span>
                </small>
              <? else: ?>
                <small>You haven't registered any activity yet.</small>
              <? endif; ?>     
            </div>
   
          </div>
        </div> 
				   <select name="gym" onchange="window.location='<?=site_url()?>user/dashboard/' + this.value">
		                  <option value="all">--All gyms--</option>
		                <? foreach ($gyms as $gym) :?>
		                  <option value="<?=$gym->id?>" <?=($gym_selected == $gym->id)?"selected":""?>><?=$gym->name?></option>
		                <? endforeach;?>                    
		                </select>
		              <? endif; ?>
         <div class="row">
	
        <? if (count($last_climbs) > 0): ?>
       
          <div class="col-md-8">
            <? $this->load->view('app/_common/date_chart', array('user'=>$this->utils->get_logged_user(), 'height' => '230px', 'title' => 'Climbs', 'width' => '100%', 'stats' => $all_climbs)); ?>            
          </div>
          <div class="col-md-4">
            <? if (count($last_climbs) > 0): ?>              
              <? $this->load->view('app/_common/pie_chart', array('height' => '230px', 'title' => 'Grades', 'width' => '100%', 'stats' => $stats_grades)); ?>
            <? endif; ?>   
          </div>
        
        <? else: ?>
           <div class="col-md-12">
            <h3>Get started logging your indoor climbing.</h3>
			<h4>If you don't see your gym listed,  <a href="mailto:info@klimbz.com?Subject=Hello" target="_top"> <b>contact us</b></a> and we can get your gym going.
            <hr/>
            <h4><span class="label label-info">1</span> Begin by selecting the gym you regularly climb in.</h4><br/>
            <h4><span class="label label-info">2</span> Then select a wall. </h4><br/>
            <h4><span class="label label-info">3</span> Tick routes as you go or after your session. <i>It's that easy.</i>  </h4>
            <br/>
			
             <a class="btn btn-lg btn-default" href="<?=site_url()?>gym/index">Continue</a>
           </div>
        <? endif; ?>
        </div>  
        <hr/> 
        <div class="row" style="display:none">                            
          
          <div class="col-md-4">
            <h3>Last Session</h3>
            <? if (count($last_climbs) > 0): ?>
              <ul class="list-group">              
              <? foreach ($last_climbs as $climbs) :?>    
                <li class="list-group-item">
                  <span class="badge"><?=$this->utils->formatted_date($climbs->created_on)?></span>
                  <a href="<?=site_url()?>route/view/<?=$climbs->route_id?>"><?=$climbs->color_name?> <?=$climbs->grade_name?></a><br/>
                  <small><?=$climbs->gym_name?></small>
                </li>        
              <? endforeach; ?>
              </ul>
            <? else: ?>
              <h5>No logged activity.</h5>
            <? endif; ?>
          </div>          
        </div>

        <? if (count($gyms_managed) > 0): ?>
        <div class="row">            
          <div class="col-sm-12">  
           <h4>You are setter at</h4>    
         </div>
              <? foreach ($gyms_managed as $gym) :?>

                 <div class="col-sm-4">
                  <div class="thumbnail">
                    <iframe width="100%" height="120" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/?ie=UTF8&amp;ll=<?=$gym->latitude?>,<?=$gym->longitude?>&amp;spn=0.045157,0.15398&amp;t=m&amp;z=13&amp;output=embed"></iframe>
                    <div class="caption">
                      <h5><a href="<?=site_url()?>gym/view/<?=$gym->id?>"><?=$gym->name?></a></h5>
                      <p><?=$gym->address?></p>                      
                    </div>
                  </div>
                      
                  </div>


              <? endforeach; ?>

            </div>
          <? endif;?>

      </div>
    </div>
        
    

  <? $this->load->view('app/_common/footer',null); ?> 

</body>

</html>