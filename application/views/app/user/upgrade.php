<? $this->load->view('app/_common/header',null); ?> 

  <body id="pricing">
    <? $this->load->view('app/_common/navigation',null); ?> 


                  <div id="first-option">
    <div class="container">
      <div class="row header">
        <div class="col-md-12">
          <h3>Upgrade your account</h3>
          <p>Choose the pricing that fits your business needs and an account manager will contact you as soon as possible.</p>
        </div>
      </div>

      <div class="row charts">
        <div class="col-md-4">
          <div class="chart first">
            <div class="plan-name">Startup</div>
            <div class="quantity">
              <span class="dollar">$</span>
              <span class="price">39</span>
              <span class="period">/month</span>
            </div>
            <div class="specs">
              <div class="spec">
                <span class="variable">5</span>
                team members
              </div>
              <div class="spec">
                <span class="variable">2</span>
                connectors
              </div>
              <div class="spec">
                <span class="variable">Basic</span>
                reporting tools
              </div>     
              <div class="spec">
                <span class="variable">1 month</span>
                data retention
              </div>        
              <div class="spec">
                <span class="variable">No</span>
                support
              </div>
            </div>
            <form method="post" action="<?=site_url()?>user/upgrade/<?=$application->id?>">
              <input type="hidden" name="plan" value="startup" />
              <input type="submit" value="Select Plan" class="button button-small" />
            </form>
          </div>
        </div>
        <div class="col-md-4">
          <div class="chart featured">
            <img src="<?=assets_url()?>images/ribbon.png" class="popular" alt="ribbon">
            <div class="plan-name">Profesional</div>
            <div class="quantity">
              <span class="dollar">$</span>
              <span class="price">89</span>
              <span class="period">/month</span>
            </div>            
            <div class="specs">
              <div class="spec">
                <span class="variable">10</span>
                team members
              </div>
              <div class="spec">
                <span class="variable">4</span>
                connectors
              </div>
              <div class="spec">
                <span class="variable">Advanced</span>
                reporting tools
              </div>
              <div class="spec">
                <span class="variable">2 months</span>
                data retention
              </div>
              <div class="spec">
                <span class="variable">24x7</span>
                support
              </div>
              <br/><br/>
            </div>
            <form method="post" action="<?=site_url()?>user/upgrade/<?=$application->id?>">
              <input type="hidden" name="plan" value="professional" />
              <input type="submit" value="Select Plan" class="button button-small" />
            </form>
          </div>
        </div>
        <div class="col-md-4">
          <div class="chart last">
            <div class="plan-name">Premium</div>
            <div class="quantity">
              <span class="dollar">$</span>
              <span class="price">129</span>
              <span class="period">/month</span>
            </div>
            <div class="specs">
              <div class="spec">
                <span class="variable">Unlimited</span>
                team members
              </div>
             <div class="spec">
                <span class="variable">Unlimited</span>
                connectors
              </div>
              <div class="spec">
                <span class="variable">Advanced</span>
                reporting tools
              </div>
              <div class="spec">
                <span class="variable">Unlimited</span>
                data retention
              </div>
              <div class="spec">
                <span class="variable">24x7</span>
                support
              </div>              
            </div>
            <form method="post" action="<?=site_url()?>user/upgrade/<?=$application->id?>">
              <input type="hidden" name="plan" value="premium" />
              <input type="submit" value="Select Plan" class="button button-small" />
            </form>
          </div>
        </div>        
      </div>      
      <div class="row faq">
        <div class="col-md-12">
          <div class="row header">
            <div class="col-md-12">
              <h3>Frequently Asked Questions</h3>
            </div>
          </div>
          <div class="row questions">
            <div class="col-md-6">
              <div class="question">
                <strong>Do I need a card to signup?</strong>
                <p>
                  If you are signing up for the free plan there's no need to enter a credit card. However, if you are upgrading your account our account manager will contact you and will offer the payment method most suitable for your company.
                </p>
              </div>              
            </div>
            <div class="col-md-6">
              <div class="question">
                <strong>Can I cancel at anytime?</strong>
                <p>
                  Yes, you can cancel at any time without any fee or penalty at all.
                </p>
              </div>              
            </div>
          </div>
        </div>
      </div>      
    </div>
  </div>

                
    <? $this->load->view('app/_common/footer',null); ?>

  </body>

  </html>
