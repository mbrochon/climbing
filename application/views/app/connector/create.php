<? $this->load->view('app/_common/header',null); ?> 

  <body id="features">
    <? $this->load->view('app/_common/navigation',null); ?> 


       <div id="contact">
        <div class="container">
            <div class="section_header">
                <h3>New Connector</h3>
            </div>
            <div class="row ">

              


                <div class="col-md-7">
                  
                  <div class="panel-group" id="accordion">
                    
                <? foreach ($connectors as $connector) :?>

                  <div class="panel panel-default">
                    
                      <div class="panel-heading">
                        <h4 class="panel-title">
                          <a data-toggle="collapse" data-parent="#accordion" href="#collapse_<?=$connector->id?>">
                            <?=$connector->name?>
                          </a>
                        </h4>
                      </div>
                      <div id="collapse_<?=$connector->id?>" class="panel-collapse collapse">                        
                        <div class="panel-body">
                          <form method="post" action="<?=site_url()?>connector/create/<?=$application->id?>/">
                            <input type="hidden" name="connector" value="<?=$connector->id?>" />    
                            <div class="form-group">
                              <label for="project"><?=$connector->destination_name?></label>
                              <input class=" form-control" name="destination" type="text" placeholder="" />    
                            </div>
                            <div class="form-group">
                              <label for="token">Token</label>
                              <input class=" form-control" name="token" type="text" placeholder="" />         
                            </div>
                            <div class="form-group">        
                              <button id="btn-signup" type="submit" class="button button-small"><i class="icon-hand-right"></i> Add</button>
                            </div>
                          </form>
                        </div>
                      </div>
                    
                  </div>
                <? endforeach; ?>
                   
              </div>


                </div>  

                <div class="col-md-5">                  
                  <h5>About connectors</h5>
                  <p><small>Once we receive a new feedback we communicate with the third party connector you create. For example, if you add a Github repository, every feedback received on Klimbz will create a new Issue in the selected Github repository.</small></p>
                  <br/>
                  <h5>Do you want more connectors ?</h5>
                  <p><small>Let us know what service you want to integrate to. <a href="javascript:addFeedback()">Click here</a></small></p>
                </div>                      
              </div>    
            

            </div>
        </div>
    <? $this->load->view('app/_common/footer',null); ?>

  </body>

  </html>