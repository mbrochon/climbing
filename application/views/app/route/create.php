  <? $this->load->view('app/_common/header',null); ?> 

<body id="services">  
  
  <? $this->load->view('app/_common/navigation',null); ?> 

    <div id="grid-second">
        <div class="container">
           
           <form class="form-horizontal" action="<?=site_url()?>route/create/<?=$wall->id?>" method="Post">
            <fieldset>
              <legend>New Route</legend>

              <div class="form-group">
                  <label for="route" class="col-lg-2 control-label">Grade</label>
                   <div class="col-md-9">
                    <select name="grade" class="form-control">
                    <? foreach ($grades as $grade) :?>
                      <option value="<?=$grade->id?>"><?=$grade->name?></option>
                    <? endforeach;?>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="rating" class="col-lg-2 control-label">Color</label> 
                   <div class="col-md-9">                 
                  <input type="text" class="form-control" name="color" id="color" autocomplete="off"/>
                </div>
                </div>

                 <div class="form-group">
                  <label for="rating" class="col-lg-2 control-label">Setter</label> 
                   <div class="col-md-9">                 
                  <input type="text" class="form-control" name="setter" id="setter" autocomplete="off"/>
                </div>
                </div>

              
              <div class="form-group">
                <div class="col-lg-10 col-lg-offset-2">
                  <button class="btn btn-default" onclick="window.history.go(-1); return false;">Cancel</button>
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </div>
            </fieldset>
          </form>

            
            

            </div>
            </div>
        
    

  <? $this->load->view('app/_common/footer',null); ?> 

  <script>
      
      <? $i=0; ?>
      $("#color").typeahead({ source: [
        <? foreach ($colors as $color) :?>
          "<?=$color->name?>"
          <? if ($i+1 < count($colors)): ?>
            ,
          <? endif; ?>
        <? $i++; ?>
        <? endforeach; ?>        
        ] 
      });

      <? $i=0; ?>
      $("#setter").typeahead({ source: [
        <? foreach ($setters as $setter) :?>
          "<?=$setter->name?>"
          <? if ($i+1 < count($setters)): ?>
            ,
          <? endif; ?>
        <? $i++; ?>
        <? endforeach; ?>        
        ] 
      });
  
  </script>
</body>

</html>