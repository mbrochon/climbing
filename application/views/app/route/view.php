  <? $this->load->view('app/_common/header',null); ?> 

<body id="services">  
  
  <? $this->load->view('app/_common/navigation',null); ?> 

    <div id="grid-second">
        <div class="container">
            
            <div class="row">
              <div class="col-md-3 col-xs-6">
                <h3><small><a href="<?=site_url()?>wall/view/<?=$wall->id?>">< <?=$wall->name?></a></small></h3>
		<h2><?=$route->grade?> <?=$route->color?></h2>
              </div>
                <div class="col-md-9 col-xs-6" style="margin-top: 67px;">  
                    <input id="route_rating_<?=$route->id?>" route="<?=$route->id?>" type="number" class="rating" data-size="sm" min="1" max="5" value="<?=$route->rating?>" data-show-clear="false" data-show-caption="false" disabled="disabled">
              </div>  
            </div>      

            <hr/>

            <div class="row">
                <div class="col-sm-8">
                    
                    <div class="panel-body" style="margin-left: -20px;"> 
                  <? $this->load->view('app/_common/tick_route_2', array('wall' => $wall, 'route' => $route, 'climb_types' => $climb_types)); ?>      
                  </div> 
                    
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h3 class="panel-title">Comments</h3>
                    </div>
                    <div class="panel-body" style="padding:30px;">
                      <? $this->load->view('app/_common/comments_widget', array('user' => $user, 'conversation_type_id' => CONVERSATION_TYPE_ROUTE, 'obj' => $route, 'comments' => $comments,'is_setter' => $is_setter,'gym_id' =>$gym->id,'moderate_comments' =>$gym->moderate_comments)); ?>
                    </div>
                  </div>

                  

                </div>
                
                <!-- SideBar -->                
                <div class="col-sm-4">
                  <div class="sidebar">

                    <? if (isset($wall->image)): ?>
                    <img class="post_pic img-responsive" src="<?=$wall->image?>" />
                  <? else: ?>
                    <img class="post_pic img-responsive" src="<?=assets_url()?>images/empty.png" />
                  <? endif;?>

                  <br/>
                  
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h3 class="panel-title">Route attempts</h3>
                    </div>
                    <div class="panel-body">
                      <? $this->load->view('app/_common/attempts_widget', array('user' => $user, 'route' => $route, 'attempts' => $attempts)); ?>
                    </div>
                  
                  </div>

                  <div class="panel panel-default">
                    <div class="panel-heading">
                      <h3 class="panel-title">Route info</h3>
                    </div>
                    <div class="panel-body">
                      <h5>Setter <span class="label"><a href="<?=site_url()?>user/profile/<?=$route->user_id?>"><?=$route->setter?></a></span></h5>
                      <h5>Date <span class="label"><?=$route->created_on?></span></h5>
                      <h5>Times climbed <span class="label"><?=round($route->climbs)?></span></h5>                     
                    </div>
                  </div>

                  
                    

                  </div>

                </div>
            </div>
            
            

            </div>
            </div>
        
    

  <? $this->load->view('app/_common/footer',null); ?> 

  <script>
    $('.rating').on("rating.change", function(event, value, caption) {
      
      var data = new FormData();
      data.append("rating_value", value);
      data.append("route_id", $(this).attr('route'));
      $('#rating_value').val(value);
    });
  </script>

</body>

</html>