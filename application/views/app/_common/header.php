<head>
    <meta charset="utf-8">
    <title>Klimbz</title>
    <meta name="description" content="Klimbz">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

    <!-- stylesheets -->
  <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
  
  <link href='<?=assets_url()?>css/custom.bootstrap.min.css' type="text/css" rel="stylesheet"/>

  <link href='<?=assets_url()?>vendors/star-rating/css/star-rating.min.css' type="text/css" rel="stylesheet"/>

  <link href='<?=assets_url()?>css/custom.css' type="text/css" rel="stylesheet"/>
  
  <link href='http://fonts.googleapis.com/css?family=Pontano+Sans' rel='stylesheet' type='text/css'>


    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">


  </head>
