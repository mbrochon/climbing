<? foreach ($links as $link): ?>

  <? if (stripos($link->link, "facebook.com") !== false): ?>
    <div class="fb-like-box" data-href="<?=$link->link?>" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="true" data-show-border="true"></div>
  <? elseif (stripos($link->link, "twitter.com") !== false): ?>
    <br/>
    <a href="<?=$link->link?>" class="twitter-follow-button" data-show-count="false">Follow @<?=str_replace("https://twitter.com/", "", $link->link)?></a>
    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
  <? else: ?>
    <div id="link_<?=$link->id?>"></div>

    <script>
      $( document ).ready(function() { 
        $('#link_<?=$link->id?>').FeedEk({
          FeedUrl:'<?=$link->link?>',
          MaxCount : 5,
          ShowDesc : false,
          ShowPubDate:true,        
          TitleLinkTarget:'_blank',
        });
      });
    </script>
  <? endif; ?>
  
<? endforeach; ?>