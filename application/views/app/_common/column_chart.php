<? $random_id = "date_chart_" . rand(); ?>
<div id="<?=$random_id?>" style="width:<?=$width?>; height: <?=$height?>; margin: 0 auto"></div>
<script>
  $( document ).ready(function() {    
    $('#<?=$random_id?>').highcharts({
      credits: {
        enabled : false
      },            
      chart: {
          type: 'column'
      },     
      title : { text: "", floating: true },
      subtitle: {
          text: '',
          x: -20,
          y: -100,
          floating: true
      },            
      plotOptions: {
          column: {
              pointPadding: 0.2,
              borderWidth: 0
          }
      },
      xAxis: {
          type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
      },
      yAxis: {
          title: {
              text: 'Routes',
              style: {
                color: '#00333F',
                fontSize: '14px'                
              }
          },
          labels: {
              enabled: true
          },
          lineWidth : 1,
          gridLineWidth : 1,
          gridLineDashStyle: 'shortdash',
          gridLineColor: '#e2e2e2'
      },
      tooltip: {
        useHTML: true,
        shared: true,
        valueSuffix: ''
      },
      legend: {
          enabled : false,
          layout: 'vertical',
          align: 'right',
          verticalAlign: 'top',
          x: 10,
          y: 100,
          borderWidth: 1
      },     
      series: [{
          type: 'column',
          name: '<?=$title?>',
          data: [
            <? $i = 0 ;?>
            <? foreach ($stats as $stat) :?>              
              ['<?=$stat->name?>',<?=$stat->quantity?>]
              <? if ($i+1<count($stats)) :?>
                ,
              <? endif; ?>

              <? $i++ ;?>
            <? endforeach; ?>
          ],
          dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                x: 4,
                y: 10,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif',
                    textShadow: '0 0 3px black'
                }
            }
      }]
    });
  });
</script> 