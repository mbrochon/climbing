 
<style>
* {
    margin: 0;
}
html, body {
    height: 100%;
}
.wrapper {
    min-height: 100%;
    height: auto !important;
    height: 100%;
    margin: 0 auto -155px; /* the bottom margin is the negative value of the footer's height */
}
.footer, .push {
    height: 155px; /* .push must be the same height as .footer */
}
</style>
<div class="push"></div>
 </div>

 <footer id="footer">
        <div class="container">            
            <div class="row credits">
                <div class="col-md-12">                    
                    <div class="row copyright">
                        <div class="col-md-12">
                            <h5><small>&copy; 2014 Klimbz, LLC. All rights reserved.</small></h5>
<small>This site is a work in progress. Please feel free to contact me with comments, feedback or whatever,<a href="mailto:info@klimbz.com?Subject=Hello, ken" target="_top"> ken</a>. </small>

                        </div>
                    </div>
                </div>            
            </div>
        </div>
    </footer>

  <!-- javascript -->

  
  <script type="text/javascript" src="<?=assets_url()?>vendors/highcharts/highcharts.js"></script>
  
  <!-- Latest compiled and minified JavaScript -->
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

  <script type="text/javascript" src="<?=assets_url()?>vendors/misc/bootbox.min.js"></script>
  <script type="text/javascript" src="<?=assets_url()?>vendors/typeahead/bootstrap3-typeahead.js"></script>
  
  <script src='<?=assets_url()?>vendors/star-rating/js/star-rating.min.js' type="text/javascript" language="javascript"></script>


  <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1956963-13', 'auto');
  ga('send', 'pageview');

</script>
