<? 
  $user = $this->utils->get_logged_user();   
?>



<header class="navbar navbar-default" role="banner">
  
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="<?=site_url()?>" class="navbar-brand"><img src="<?=assets_url()?>images/logo-white.png" width="80"/></a>
            </div>

            <div class="navbar-collapse collapse navbar-right" id="navbar-collapse">
                <ul class="nav navbar-nav col-md-12 " id="menu">                  
                   <li class="dropdown">
                   <a href="<?=site_url()?>gym/index">              
                      Gyms
                  </a>
                 </li>

                  <li class="dropdown">
                   <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?=$user->name?> <img src="<?=$this->utils->get_gravatar_logged_user()?>" width="20" /> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                      <li><a href="<?=site_url()?>user/dashboard">Dashboard</a></li>
                      <li><a href="<?=site_url()?>user/edit">Edit Profile</a></li>
                      <li class="divider"></li>
                      <li><a href="<?=site_url()?>site/logout">Logout</a></li>
                    </ul>
                  </li>
                </ul>
            </div>
        </div>
    
    </header>

<div class="wrapper">