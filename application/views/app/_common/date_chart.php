<? $random_id = "date_chart_" . rand(); ?>
<div id="<?=$random_id?>" style="width:<?=$width?>; height: <?=$height?>; margin: 0 auto"></div>
<script>
  $( document ).ready(function() {  

    Highcharts.wrap(Highcharts.Point.prototype, 'select', function (proceed) {

        proceed.apply(this, Array.prototype.slice.call(arguments, 1));

        $.ajax({
          url: "<?=site_url()?>user/api_climbs?timestamp="+(this.x/1000)+"&user_id=" + <?=$user->id?>      
        }).done(function(response) {          
          var obj = $.parseJSON(response);
          var html = "";
          for (var i = obj.length - 1; i >= 0; i--) {
            var climb = obj[i];
            html = html + "<p><strong>" + climb.grade_name + " " + climb.color_name + "</strong> at " + climb.wall_name + " in " + climb.gym_name + "</p>";
            $("#modal_title").html(climb.date_climb);
          };
          $("#html_climbs").html(html);
          $("#modal_climbs").modal();
        });

    });

    var climbChart = $('#<?=$random_id?>').highcharts({
      credits: {
        enabled : false
      },  
      chart: {
            zoomType: 'x'
        },                
      title : { text: "", floating: true },
      subtitle: {
            text: document.ontouchstart === undefined ?
                    'Click and drag in the plot area to zoom in' :
                    'Pinch the chart to zoom in'
        },      
      plotOptions: {
          column: {
              pointPadding: 0.2,
              borderWidth: 0
          }
      },
      xAxis: {
          type: "datetime",
          lineWidth : 1,
          gridLineDashStyle: 'shortdash',
          gridLineColor: '#e2e2e2',
          gridLineWidth: 1,          
          tickLength: 0, 
          tickInterval: 7 *24 * 3600 * 1000
      },
      yAxis: {
          title: {
              text: 'Routes',
              style: {
                color: '#00333F',
                fontSize: '14px'                
              }
          },
          labels: {
              enabled: true
          },
          lineWidth : 1,
          gridLineWidth : 1,
          gridLineDashStyle: 'shortdash',
          gridLineColor: '#e2e2e2'
      },
      tooltip: {
          backgroundColor: 'none',
          borderWidth: 0,          
          useHTML: false,
          padding: 0          
      },
      legend: {
          enabled : false,
          layout: 'vertical',
          align: 'right',
          verticalAlign: 'top',
          x: 10,
          y: 100,
          borderWidth: 1
      },     
      series: [{
          type: 'column',
          name: '<?=$title?>',
          allowPointSelect: true,
          data: [
            <? $i = 0 ;?>
            <? foreach ($stats as $stat) :?>
              <? $datetime = DateTime::createFromFormat('Y-m-d H:i:s', $stat->name . " 12:00:00"); ?>
              [<?=($datetime->format("U")*1000)?>,<?=$stat->quantity?>]
              <? if ($i+1<count($stats)) :?>
                ,
              <? endif; ?>

              <? $i++ ;?>
            <? endforeach; ?>
          ]
      }]
    });
  });
</script> 

<div class="modal fade" id="modal_climbs" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Climbed on <span id="modal_title"/></h4>
      </div>
      <div class="modal-body">
        <div id="html_climbs"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>        
      </div>
    </div>
  </div>
</div>