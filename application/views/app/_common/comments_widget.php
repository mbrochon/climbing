  <script>
    function askForConfirmation(id,confirm_message)
    {
      if (confirm_message==''){  
         confirm_message = "Are you sure ?";
      }
      bootbox.dialog({
        message: confirm_message,
        title: "",
        buttons: {
          success: {
            label: "Yes, I'm sure.",
            className: "btn-danger",
            callback: function() {
              $("#"+id).submit();
            }
          }, 
          danger: {
            label: "Changed my mind.",
            className: "btn-primary",
            callback: function() {

            }
          }                           
        }
      });
    }
  </script>


  <div class="row">
  <form action="<?=site_url()?>comment/create" method="post" enctype="multipart/form-data">
    <input type="hidden" value="<?=$conversation->id?>" name="conversation_id" />
    <input type="hidden" value="<?=$obj->id?>" name="caller_id" />
    <input type="hidden" value="<?=$conversation_type_id?>" name="conversation_type_id" />
    <input type="hidden" value="<?=$user->id?>" name="user_id" />
    <div class="send-wrap ">
        <textarea class="form-control send-message" name="text" rows="3" placeholder="Write a comment..."></textarea>
    </div>
    <div class="btn-panel">           
      <br/>           
      <label>Attach an image</label> 
      <input type="file" name="image" accept="image/*" />
      
      <button class="btn btn-primary pull-right" role="button" type="submit">Add Comment</button>

    </div>
  </form>
</div>
 
<div class="row">
  <div class="conversation-wrap col-sm-12">
    <? foreach ($comments as $comment) :?>   
     <? if (!$comment->is_hidden || $is_setter): ?>
    <hr/> 
      <div class="media msg">
        <a class="pull-left" href="<?=site_url()?>user/profile/<?=$comment->user_id?>">
          <img class="media-object" data-src="holder.js/64x64" alt="64x64" style="width: 32px; height: 32px;" src="<?=$this->utils->get_gravatar_user($comment)?>">
        </a>
        <div class="media-body">
          <div class="col-md-12">
            <small class="pull-right time"><i class="fa fa-clock-o"></i> <?=$comment->created_on?></small>
            <h5 class="media-heading"><?=$comment->username?></h5>
            <? if ($comment->is_hidden): ?>
                <? if ($is_setter): ?>
                    <small class="hidden-label col-lg-12"><?=$comment->text?> (Hidden)</small>
                <? endif; ?>    
            <? else: ?>
                <small class="col-lg-12"><?=$comment->text?></small>
            <? endif; ?>
            
                
             <? if ($is_setter): ?>

             <div class="tick_route row">
                  
                   <? if ($moderate_comments): ?> 
                      <div class="col-md-5 col-xs-12  pull-right">
                        <? if ($comment->is_hidden): ?>
                        <form method="post" action="<?=site_url()?>route/hidecomment/<?=$comment->id?>" id="form_archive_<?=$comment->id?>" class="navbar-form navbar-left">                          
                                 <input type="hidden" value="<?=$gym_id?>" name="gym_id" />
                                 <input type="hidden" value="<?=$obj->id?>" name="route_id" />
                                 <input type="button" id="blue-button" class="btn btn-danger btn-primary btn-xs" value="Show Comment" onclick="askForConfirmation('form_archive_<?=$comment->id?>','Do you want to show this comment?')" />
                             </form>
                        <? else: ?>
                             <form method="post" action="<?=site_url()?>route/hidecomment/<?=$comment->id?>" id="form_archive_<?=$comment->id?>" class="navbar-form navbar-left">                          
                                 <input type="hidden" value="<?=$gym_id?>" name="gym_id" />
                                 <input type="hidden" value="<?=$obj->id?>" name="route_id" />
                                 <input type="button" class="btn btn-danger btn-xs" value="Hide Comment" onclick="askForConfirmation('form_archive_<?=$comment->id?>','Do you want to hide this comment?')" />
                             </form>
                        <? endif;?>
                  <? else:?>
                        <div class="col-md-2  pull-right">    
                  <? endif;?>    
                  <form method="post" action="<?=site_url()?>route/deletecomment/<?=$comment->id?>"  id="form_delete_<?=$comment->id?>" class="navbar-form navbar-left">                          
                      <input type="hidden" value="<?=$gym_id?>" name="gym_id" />
                      <input type="hidden" value="<?=$obj->id?>" name="route_id" />
                      <input type="button" class="btn btn-danger btn-xs" value="Delete" onclick="askForConfirmation('form_delete_<?=$comment->id?>','Do you want to delete this comment?')" />
                   </form>
                  </div>    
              </div>
            <? endif; ?>                               
              
          </div>
          <? if ($comment->media_url != null && $comment->media_url != '0' ): ?> 
          <div class="col-md-12">
             <br/>
            <img src="<?=$comment->media_url?>" width="100%" />        
          </div>
          <? endif; ?>
        </div>
      </div>
     <? endif; ?>
    <? endforeach; ?>
  </div>
</div>