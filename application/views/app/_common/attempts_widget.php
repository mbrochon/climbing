 <div class="row">
  <div class="col-sm-12">
  <form action="<?=site_url()?>route/add_attempt/<?=$route->id?>" method="post">
    <input type="hidden" name="from" value="route/view/<?=$route->id?>" />
    
    <div class="send-wrap ">
        <textarea class="form-control send-message" name="notes" rows="3" placeholder="Write a note..."></textarea>
    </div>
    <div class="btn-panel">           
      <br/>           
      <input type="submit" class="btn btn-default btn-sm" value="Add attempt (<?=(isset($route->climbed_attempts))?$route->climbed_attempts:"0"?>)" />
    </div>

  </form>
  </div>
</div>
 <hr/>
<div class="row">
  <div class="conversation-wrap col-sm-12">
    <? foreach ($attempts as $attempt) :?>   
      
        
          <div class="col-md-12">
            <small><i class="fa fa-clock-o"></i> <?=$this->utils->formatted_date($attempt->created_on)?></small>   
            <br/>         
            <? if (isset($attempt->notes)):?>
              <small><i class="fa fa-comment-o"></i> <?=$attempt->notes?></small>          
            <? endif; ?>
            <hr/>
          </div>          
      
    <? endforeach; ?>
  </div>
</div>