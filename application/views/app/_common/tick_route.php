
<form action="<?=site_url()?>route/add_to_profile/<?=$route->id?>" class="navbar-form navbar-left" method="post">
    <input type="hidden" name="from" value="wall/view/<?=$wall->id?>#<?=$route->id?>" />
  
    <div class="climb_type_date">      
        <h5>Date</h5>
        <input type="text" name="climbed_on" id="datepicker" class="datepicker" value="<?=date('m-d-Y')?>" />        
    </div>    
    <br/>
    <div class="climb_types">
    <? foreach ($climb_types as $climb_type):?>
      <input type="radio" name="climb_type" value="<?=$climb_type->id?>"/>  <?=$climb_type->name?>  &nbsp;   
    <? endforeach; ?>
      <input type="checkbox" name="repeat" /> Repeat
    </div>
    <br/>
    <div class="climb_type_row">    
    <input type="submit" class="btn btn-primary btn-sm" value="Submit" /> <input type="button" class="btn btn-default btn-sm" value="Cancel" onclick="launchTickRoute('tick_route_<?=$route->id?>_placeholder');" />
    </div>
</form>
