<? $random_id = "chart_" . rand(); ?>
<div id="<?=$random_id?>" style="width:<?=$width?>; height: <?=$height?>; margin: 0 auto"></div>
<script>
  $( document ).ready(function() {    
    $('#<?=$random_id?>').highcharts({
      chart: {
          plotBackgroundColor: null,
          plotBorderWidth: null,
          plotShadow: false,
          spacing: [0,0,0,0]
      },
      credits: {
        enabled : false
      }, 
      title: {
        text: '',
        style: { "color": "#333333", "fontSize": "0px" },
        align: "right",
        verticalAlign : "top"
      },
      tooltip: {
        pointFormat: '{point.name}: <b>{point.y:.0f}</b>'
      },
      plotOptions: {
            pie: {
                dataLabels: {
                    enabled: true,
                    distance: -50,
                    style: {
                        fontWeight: 'bold',
                        color: 'white',
                        textShadow: '0px 1px 2px black'
                    }
                },                
            }
        },
      series: [{
          type: 'pie',
          name: '<?=$title?>',
          data: [
            <? foreach ($stats as $stat) :?>
              ['<?=$stat->name?>',<?=$stat->quantity?>],
            <? endforeach; ?>
          ]
      }]
    });
  });
</script> 