
<form action="<?=site_url()?>route/add_to_profile/<?=$route->id?>" class="navbar-form navbar-left" method="post">
    <input type="hidden" name="from" value="wall/view/<?=$wall->id?>#<?=$route->id?>" />
    <div class="row">
        <div class="date-field col-md-3 col-xs-4">      
            <h5>Date</h5>
            <input type="text" name="climbed_on" id="datepicker" class="datepicker" value="<?=date('m-d-Y')?>" />        
        </div>    
        <div class="col-md-6 col-xs-12 climb_types">
        <? foreach ($climb_types as $climb_type):?>
          <input type="radio" name="climb_type" value="<?=$climb_type->id?>"/>  <?=$climb_type->name?>  &nbsp;   
        <? endforeach; ?>
        <label class="repeat-label"><input type="checkbox" name="repeat" /> Repeat</label> 
        </div>
        <div class="col-md-3 col-xs-4 date-field">
            <h5>Rate it</h5>
            <input type="hidden" name="rating_value" id="rating_value" value=""/>
            <input id="route_rating_<?=$route->id?>" route="<?=$route->id?>" type="number" class="rating" data-size="sm" min="1" max="5" value="<?=$route->rating?>" data-show-clear="false" data-show-caption="false">
        </div>
      </div>
    <div class="row" style="margin-top: 30px;">
        <div class="col-md-3 col-xs-3 climb_type_row">    
            <input type="hidden" name="from" value="route/view/<?=$route->id?>" />
            <input type="submit" class="btn btn-primary btn-md" value="Tick route (<?=(isset($route->climbed_times))?$route->climbed_times:"0"?>)" />
        </div>
      </div>
    
</form>
