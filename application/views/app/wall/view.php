  <? $this->load->view('app/_common/header',null); ?> 

  <style>
    .rating-xs { margin-top: 5px; }
  </style>

   <script>

    function launchTickRoute(id)
    {
      $("#"+id).slideToggle();
    }
    
     function showRoutesByGrade(grade)
    {
      if (grade == "-") {
        $(".route_container").css('display','');      
      }
      else
      {
        $(".route_container").css('display','none');
        $(".grade_"+grade).css('display','');  
      }
      
    }
  </script>
<body id="services">  
  
  <? $this->load->view('app/_common/navigation',null); ?> 

    <div id="grid-second">
       
                 
            
             
        <div class="container">
          
          <div class="row">
	
	
             <? if (count($images)>0) :?>
             <div class="col-md-3">
		<h3><small><a href="<?=site_url()?>gym/view/<?=$gym->id?>">&#60; <?=$gym->name?></a></small></h3>
                  <img src="<?=$images[0]->path?>" width="100%" />       
                  
             </div> 
                  <? endif;?>

            <div class="col-md-2 col-xs-4">

                <h2><?=$wall->name?></h2>
                <h5> Active Routes <span class="badge"><?=count($routes)?></span></h5>


             </div>
                <div class="col-md-2  pull-right">
                   <? if ($is_setter): ?>                    
                         <form action="<?=site_url()?>route/create/<?=$wall->id?>" class="navbar-form navbar-left">
                           <input type="submit" class="btn btn-primary btn-sm" value="New Route" />
                         </form>

                         <form action="<?=site_url()?>wall/archive/<?=$wall->id?>" id="form_archiveall_<?=$wall->id?>" class="navbar-form navbar-left">
                           <input type="button" class="btn btn-danger btn-sm" value="Archive All Routes" onclick="askForConfirmation('form_archiveall_<?=$wall->id?>')" />
                         </form>
                   <? endif; ?>
                </div>            
            </div>
          
            <div class="row container">
              <div class="col-md-12">
                <br/><br/>
                <? $this->load->view('app/_common/column_chart', array('height' => '230px', 'title' => 'Routes', 'width' => '100%', 'stats' => $routes_per_grade)); ?>            
                <br/><br/><br/>
              </div>
            </div>
                

            <hr/>
            <div class="row features">    
                          
            
              <div class="col-md-9">

                <ul class="nav nav-tabs" style="margin-bottom: 15px;">
                  <li class="active"><a href="#current_routes" data-toggle="tab">Current Routes <span class="badge"><?=count($routes)?></span></a></li>
                  <li class=""><a href="#archived_routes" data-toggle="tab">Archived Routes <span class="badge"><?=count($archived_routes)?></span></a></li>                  
                </ul>
                <div id="myTabContent" class="tab-content">
                  <div class="tab-pane fade active in" id="current_routes">
                    <div class="col-md-12">
                    <h5>Filter by Grade</h5>
                    <? 
                      $grades = array();
                      foreach ($routes as $route) 
                      {
                        $grades[$route->grade] = 1;
                      } 

                    ?>

                    <select onchange="showRoutesByGrade(this.value)">
                        <option value="-">Select</option>
                        <? foreach ($grades as $key => $value): ?>
                          <option value="<?=strtolower($key)?>"><?=$key?></option>
                        <? endforeach; ?>                        
                    </select>
                    <hr/>
                    </div>

                    <? foreach ($routes as $route) :?>
                      
                      <div class="col-md-6 route_container grade_<?=strtolower($route->grade)?> color_<?=strtolower($route->color)?>">          
                        <a name="<?=$route->id?>"></a>
                        <div class="panel panel-default">
                          <div class="panel-heading">
                            <a href="<?=site_url()?>route/view/<?=$route->id?>"><?=$route->name?> <?=$route->grade?> <?=$route->color?></a>                            
                            <? if (time() - strtotime($route->created_on) < 5*60*3600): ?>
                              <span class="badge badge-important">New</span>
                            <? endif; ?>
                          <input id="route_rating_<?=$route->id?>" route="<?=$route->id?>" type="number" class="rating" data-size="xs" min="1" max="5" value="<?=$route->rating?>" data-show-clear="false" data-show-caption="false">

							</div>
                          <div class="panel-body">
                              <small>Setter: <strong><a href="<?=site_url()?>user/profile/<?=$route->setter_id?>"><?=$route->setter?></a></strong></small>&nbsp &nbsp
                              <small>Created on: <span class="date"><strong><?=$this->utils->formatted_date($route->created_on)?></strong></span></small>
                             
                              <div class="tick_route row">
                              
                                <form action="<?=site_url()?>route/add_to_profile/<?=$route->id?>" class="navbar-form navbar-left">
                                  <input type="hidden" name="from" value="wall/view/<?=$wall->id?>#<?=$route->id?>" />
                                  <input type="button" class="btn btn-primary btn-xs" onclick="launchTickRoute('tick_route_<?=$route->id?>_placeholder');" value="Tick Route (<?=($route->climbed_times!=""?$route->climbed_times:"0")?>)" />
                                </form>

                              <? if ($is_setter): ?>
                                <form action="<?=site_url()?>route/archive/<?=$route->id?>" id="form_archive_<?=$route->id?>" class="navbar-form navbar-left">                          
                                  <input type="button" class="btn btn-danger btn-xs" value="Archive" onclick="askForConfirmation('form_archive_<?=$route->id?>')" />
                                </form>
                                <form action="<?=site_url()?>route/delete/<?=$route->id?>"  id="form_delete_<?=$route->id?>" class="navbar-form navbar-left">                          
                                  <input type="button" class="btn btn-danger btn-xs" value="Delete" onclick="askForConfirmation('form_delete_<?=$route->id?>')" />
                                </form>
                              <? endif; ?>                               
                              </div>

              <div id="tick_route_<?=$route->id?>_placeholder" style="display:none">
                                <? $this->load->view('app/_common/tick_route', array('route' => $route, 'climb_types' => $climb_types)); ?>            
                              </div>
							
                          </div>
                        </div>

                      </div>   

                     
                    <? endforeach; ?>

                     <script>
                        function askForConfirmation(id)
                        {
                          bootbox.dialog({
                            message: "Are you sure ?",
                            title: "",
                            buttons: {
                              success: {
                                label: "Yes, I'm sure.",
                                className: "btn-danger",
                                callback: function() {
                                  $("#"+id).submit();
                                }
                              }, 
                              danger: {
                                label: "Changed my mind.",
                                className: "btn-primary",
                                callback: function() {
                                  
                                }
                              }                           
                            }
                          });
                        }
                      </script>
                  </div>
                  <div class="tab-pane fade" id="archived_routes">
                     <? foreach ($archived_routes as $route) :?>
                        <div class="col-md-4">          
                          <div class="panel panel-default">
                            <div class="panel-heading"><a href="<?=site_url()?>route/view/<?=$route->id?>"><?=$route->name?> <?=$route->grade?> <?=$route->color?> </a></div>
                            <div class="panel-body">
                              
                              <p><small>Created by <strong><?=$route->setter?></strong> on <span class="date"><?=$route->created_on?></span></small></p>
                              <br/>
                              <input id="route_rating_<?=$route->id?>" route="<?=$route->id?>" type="number" class="rating" data-size="xs" min="1" max="5" value="<?=$route->rating?>" data-show-clear="false" data-show-caption="false">
                                                    
                            </div>
                          </div>

                        </div>   
                      <? endforeach; ?>
                  </div>
                  
                </div>
              
              </div>

              <div class="col-md-3">
                <? if ($is_setter): ?>   
                <ul class="nav nav-tabs" style="margin-bottom: 15px;">
                  <li class="active"><a href="">Upload a Photo</a></li>                  
                </ul>
                    
                
                      <form method="post" enctype="multipart/form-data" id="upload" action="<?=site_url()?>wall/upload_image/<?=$wall->id?>">                    
                    <input type="file" name="image" accept="image/*" />
                    <br/>

                    <input type="submit" class="btn btn-success btn-sm" value="Upload" />


                </form>
                  
                
              <? endif; ?>
                <? $i = 0; ?>
                <!--<? foreach ($images as $image) :?>
                  <? if ($i==0):?>
                  <hr/>                
                  <? else: ?>
                    <img src="<?=$image->path?>" width="100" />                    
                  <? endif;?>
                  <? $i++; ?>
                <? endforeach; ?>
                -->
                
              </div>
              

            </div>

            

            </div>
            </div>
        
    

  <? $this->load->view('app/_common/footer',null); ?> 


  <div class="modal fade" id="modal_tick_route" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          <h4 class="modal-title" id="modal_tick_title"></h4>
        </div>
        <div class="modal-body">
          <iframe src="" id="modal_tick_route_url" width="100%" height="300" border="0" style="margin:0;padding:0;border:0;overflow:hidden"/>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal" onclick="location.reload(true)">Close</button>        
        </div>
      </div>
    </div>
  </div>

  <script>

    $('.rating').on("rating.change", function(event, value, caption) {
      
      var data = new FormData();
      data.append("rating_value", value);
      data.append("route_id", $(this).attr('route'));
      
      $.ajax({
          url: '<?=site_url()?>route/rate',
          type: 'POST',
          data: data,
          cache: false,
          dataType: 'json',
          processData: false, // Don't process the files
          contentType: false, // Set content type to false as jQuery will tell the server its a query string request
          success: function(data, textStatus, jqXHR)
          {
            if(typeof data.error === 'undefined')
            {
              location.reload(true);
            }
          },
          error: function(jqXHR, textStatus, errorThrown)
          {
            // Handle errors here
            console.log('ERRORS: ' + textStatus);
            console.log(errorThrown);
            // STOP LOADING SPINNER
          }
      });

    });

    $(function() {
      $('.datepicker').datepicker();
    });

    var url_upload = "<?=site_url()?>wall/upload_image/<?=$wall->id?>";

  </script>

</body>

</html>