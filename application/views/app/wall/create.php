  <? $this->load->view('app/_common/header',null); ?> 

<body id="services">  
  
  <? $this->load->view('app/_common/navigation',null); ?> 

    <div id="grid-second">
        <div class="container">
           
           <form class="form-horizontal" action="<?=site_url()?>wall/create/<?=$gym->id?>" method="Post">
            <fieldset>
              <legend>New Wall</legend>
              <div class="form-group">
                <label for="wall_name" class="col-lg-2 control-label">Name</label>
                      <div class="col-md-9">
                          <input type="text" class="form-control"  name="name" placeholder="Wall Name">
                      </div>
              </div>
              <div class="form-group">
                <label for="wall_name" class="col-lg-2 control-label">Type</label>
                <div class="col-md-9">
                    <select name="type"  class="form-control">
                         <? foreach ($types as $type) :?>
                          <option value="<?=$type->id?>"><?=$type->name?></option>
                        <? endforeach;?>
                    </select>
                </div>
              </div>
              
              <div class="form-group">
                <div class="col-lg-10 col-lg-offset-2">
                  <button class="btn btn-default" onclick="window.history.go(-1); return false;">Cancel</button>
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </div>
            </fieldset>
          </form>

            

        </div>
    </div>
        
    

  <? $this->load->view('app/_common/footer',null); ?> 

</body>

</html>