  <? $this->load->view('app/_common/header',null); ?> 

<body id="services">  
  
  <? $this->load->view('app/_common/navigation',null); ?> 

    <div id="grid-second">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <h2>Gyms</h2>              
            </div>            
           
          </div>      
 <hr/>
            <div class="row features">              
              <? foreach ($gyms as $gym) :?>

                 <div class="col-sm-4">
                  <div class="thumbnail">
                    <div class="caption">
                      <h3><a href="<?=site_url()?>gym/view/<?=$gym->id?>"><?=$gym->name?></a></h3>
                      <p><?=$gym->address?></p>                      
                    </div>
                    <iframe width="100%" height="150" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/?ie=UTF8&amp;ll=<?=$gym->latitude?>,<?=$gym->longitude?>&amp;spn=0.045157,0.15398&amp;t=m&amp;z=13&amp;output=embed"></iframe>
                    
                  </div>
                      
                  </div>


              <? endforeach; ?>

            </div>

            

            </div>
            </div>
        
    

  <? $this->load->view('app/_common/footer',null); ?> 

</body>

</html>