  <? $this->load->view('app/_common/header',null); ?> 

<body id="services">  
  
  <div id="fb-root"></div>
  <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=710634812359952&version=v2.0";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));</script>

  <style>
    #gym_header {
      background-image:url(<?=$gym_image?>);
      margin-top: -21px;
      padding: 20px 0px 20px 0px; 
      -webkit-background-size: cover;
      -moz-background-size: cover;
      -o-background-size: cover;
      background-size: cover;
    }

    .white
    {
      color: #fff;
    }
  </style>
  <? $this->load->view('app/_common/navigation',null); ?> 

    <div id="grid-second">
        <div class="container-fluid" id="gym_header">
          <div class="container">
            <div class="col-md-10 col-xs-8">
              <h2 class="white"><?=$gym->name?></h2><p> 
              <h3 class="white"><small><?=$gym->address?></small></h3>
            </div>
              <div class="col-md-2 col-xs-12 pull-right">
              <? if($is_setter): ?>
              <form class="form-gym-view" action="<?=site_url()?>wall/create/<?=$gym->id?>" method="get">
                <button type="submit" class="btn btn-primary btn-sm">New Wall</button>     
              </form>
              <form class="form-gym-view">
                <button class="btn btn-default btn-sm" data-toggle="modal" data-target="#setter_link_modal" onclick="return false;">
                  Invite a Setter
                </button>
              </form>
              <form class="form-gym-view" action="<?=site_url()?>gym/moderatecomments/<?=$gym->id?>" method="post">
                <? if ($gym->moderate_comments): ?>  
                    <button type="submit" class="btn btn-primary btn-sm">
                      Moderate Comments is ON
                    </button>
                <? else: ?>
                    <button type="submit" class="btn btn-primary btn-sm">
                      Moderate Comments is OFF
                    </button>
                <? endif; ?>
              </form>
              <? endif; ?>
            </div>            

          </div>      
        </div>
        <div class="container">
            <br/>
            <div class="row">     
              <div class="col-md-8">

                <h3>There are <?=$total_routes?> active routes at <?=$gym->name?></h3>
                <? $this->load->view('app/_common/column_chart', array('height' => '230px', 'title' => 'Routes', 'width' => '100%', 'stats' => $routes_per_grade)); ?>            

                <h4>Walls</h4>    
                  
                <div class="row"> 
                  <? foreach ($walls as $wall) :?>                  
                    <div class="col-sm-4">
                      <a href="<?=site_url()?>wall/view/<?=$wall->id?>">
                        <div class="thumbnail">
                          
                          <div style="height:134px;overflow:hidden;">
                            <img src="<?=($wall->image!="")?$wall->image:assets_url().'images/empty.png'?>" alt="" class="img-responsive" />
                          </div>
                                  
                        <div class="caption">
                          <h4><a href="<?=site_url()?>wall/view/<?=$wall->id?>"><?=$wall->name?></a> 
                          <? if (time() - strtotime($wall->updated) < 5*60*3600): ?><span class="badge-success">Updated</span><? endif; ?></h4>
                          <p> Routes<span> <span pan class="badge"><?=$wall->routes?></span><br/>
                                 Grade Range <span class="badge">V<?=$wall->min_grade?></span> - <span class="badge">V<?=$wall->max_grade?></span></p>
                          
                        </div>
                      </div>
                          </a>
                    </div>
                  <? endforeach; ?>
                </div>

                
              </div>
              <div class="col-md-4">

                <? if (1==2): ?>
                <h4>Latest Updates on Walls</h4>
                <div class="list-group"> 
                  <? foreach ($routes_recent_updates as $route_update) :?>   
                    <div class="media msg">
                      <a class="pull-left" href="<?=site_url()?>user/profile/<?=$route_update->user_id?>">
                        <img class="media-object" data-src="holder.js/64x64" alt="64x64" style="width: 32px; height: 32px;" src="<?=$this->utils->get_gravatar_user($route_update)?>">
                      </a>
                      <div class="media-body">
                        <div class="col-md-12">
                          <small class="pull-right time"><i class="fa fa-clock-o"></i> <?=$route_update->created_on?></small>
                          <h5 class="media-heading"><?=$route_update->setter?></h5>
                          <small class="col-lg-12"><a href="<?=site_url()?>route/view/<?=$route_update->route_id?>"><?=$route_update->grade?> <?=$route_update->color?></a> at <a href="<?=site_url()?>wall/view/<?=$route_update->wall_id?>"><?=$route_update->wall?></a></small>
                        
                        </div>                        
                      </div>
                    </div>
                  <? endforeach; ?>
                </div>
                <? endif; ?>

                <? $this->load->view('app/_common/links_widget', array('links' => $links)); ?>

                <!--
                <h4>Comments</h4>  

                <? $this->load->view('app/_common/comments_widget', array('user' => $user, 'conversation_type_id' => CONVERSATION_TYPE_GYM, 'obj' => $gym, 'comments' => $comments)); ?>
  -->
                <h4>Most Climbed Routes</h4>
                <div class="list-group">       
                <? $i = 1; ?>      
                <? foreach ($top_climbed_routes as $route) :?>   
                   <a href="<?=site_url()?>route/view/<?=$route->id?>" class="list-group-item">
                     <span class="badge badge-info">#<?=$i?></span>
                    <h5 class="list-group-item-heading"><?=$route->grade_name?> <?=$route->color_name?></h5>
                    <small><?=$route->wall_name?></small>
                  </a>              
                  <?$i++?>
                <? endforeach; ?>
                </div>   

                <h4>Least Climbed Routes</h4>
                <div class="list-group">     
                 <? $i = 1; ?>        
                <? foreach ($least_climbed_routes as $route) :?>   
                   <a href="<?=site_url()?>route/view/<?=$route->id?>" class="list-group-item">
                    <span class="badge">#<?=$i?></span>
                    <h5 class="list-group-item-heading"><?=$route->grade_name?> <?=$route->color_name?></h5>
                    <small><?=$route->wall_name?></small>
                  </a>       
                  <?$i++?>       
                <? endforeach; ?>
                </div>                
              </div>
            </div>

            

            </div>
            </div>
        
    


<!-- Modal -->
<div class="modal fade" id="setter_link_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Inviation Link</h4>
      </div>
      <div class="modal-body">
        <p>Here's the link to invite a setter to this gym.</p>
        <h5><?=site_url()?>site/signup/<?=$gym->id?>/<?=md5($gym->id.URL_SALT)?></h5>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>        
      </div>
    </div>
  </div>
</div>


  <? $this->load->view('app/_common/footer',null); ?> 

  <link href='<?=assets_url()?>vendors/FeedEk/css/FeedEk.css' type="text/css" rel="stylesheet"/>
  <script type="text/javascript" src="<?=assets_url()?>vendors/FeedEk/js/FeedEk.js"></script>

  
  
</body>

</html>