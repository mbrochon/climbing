<? $this->load->view('app/_common/header',null); ?> 

<body>
  <? if ($from_upgrade == 1): ?>

        <div class=""><div class="alert alert-success"><strong>Thanks!</strong> An account manager will contact you soon.</div></div>
      <? else: ?>
        <? if ($role->name == "Owner") :?>
          <div class="">
            <? if ($application->upto_members <= count($members)) :?>
              <div class="alert alert-warning"><strong>Watch out!</strong> Maximum number of collaborators on free account reached.<strong><a href="<?=site_url()?>user/upgrade/<?=$application->id?>">Upgrade Account</a></strong> </div>
            <? else: ?>
              <div class="alert alert-info"><strong><?=$application->upto_members-count($members)?> collaborator slot left!</strong> You can remove this limit by <strong><a href="<?=site_url()?>user/upgrade/<?=$application->id?>">upgrading your account</a></strong></div>
            <? endif; ?>
          </div>
        <? endif; ?>
      <? endif; ?>

 <? $this->load->view('app/_common/navigation',null); ?> 

    <div class="container">
      
      
      <div class="row padding_10">
        
          <div class="col-md-12 ">        
            <center>
              <img src="<?=($application->icon!="")?$application->icon:assets_url()."images/icons/app.png"?>" width="50" />
              <h1><?=$application->name?></h1>
            </center>
          </div>
          <div class="col-md-12 ">                 
           
              <div class="row padding_10">                 
                <div class="col-md-3">
                  <? $this->load->view('app/_common/metadata_view',array('title' => 'Status', 'stats' => $stats_statuses, 'application' => $application)); ?>   
                </div>
               
                <div class="col-md-3">
                  <? $this->load->view('app/_common/metadata_view',array('title' => 'Screens', 'stats' => $stats_view_controllers, 'application' => $application)); ?>   
                </div>

                <div class="col-md-3">
                  <? $this->load->view('app/_common/metadata_view',array('title' => 'iOS Version', 'stats' => $stats_oses, 'application' => $application)); ?>   
                </div>

                <div class="col-md-3">
                  <? $this->load->view('app/_common/metadata_view',array('title' => 'Application Version', 'stats' => $stats_application_version, 'application' => $application)); ?>   
                </div>

              </div>
            

           </div>

           <div class="col-md-12 ">
          <ul class="list-group ">
  
            <div class="section_header">
                  <h3>Latest feedback</h3>
              </div>
            <? foreach ($inputs as $input) :?>
            <li class="list-group-item">
              <span class="label label-<?=($input->status=="New")?"info":"success"?>"><?=$input->status?></span>    
    <a href="<?=site_url()?>input/view/<?=$input->id?>"><?=$input->comment?></a>
    <hr class="less_margin"/>
  </li>
                    
                <? endforeach; ?>
                </ul>
        </div>
      </div>
    </div>
  

<? $this->load->view('app/_common/footer',null); ?>

</body>

</html>