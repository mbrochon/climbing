<? $this->load->view('app/_common/header',null); ?> 

<body>

        <? if ($role->name == "Owner") :?>
          <div class="">
            <? if ($application->upto_members <= count($members)) :?>
              <div class="alert alert-warning"><strong>Watch out!</strong> Maximum number of collaborators on free account reached.<strong><a href="<?=site_url()?>user/upgrade/<?=$application->id?>">Upgrade Account</a></strong> </div>
            <? else: ?>
              <div class="alert alert-info"><strong><?=$application->upto_members-count($members)?> collaborator slot left!</strong> You can remove this limit by <strong><a href="<?=site_url()?>user/upgrade/<?=$application->id?>">upgrading your account</a></strong></div>
            <? endif; ?>
          </div>
        <? endif; ?>
   

 <? $this->load->view('app/_common/navigation',null); ?> 

    <div class="container">
      
      
      <div class="row">
        <div class="col-md-12">
          <div class="section_header">
                  <h3>Settings</h3>
              </div>
                    <!-- Nav tabs -->
          <ul class="nav nav-tabs">
            <li class="active"><a href="#sdk" data-toggle="tab">Integration and SDK</a></li>
            <li><a href="#connectors" data-toggle="tab">External Connectors</a></li>
          </ul>

          <!-- Tab panes -->
          <div class="tab-content">
            <div class="tab-pane active" id="sdk">
              <h4>Integration</h4>
              <p><strong>1.</strong> Download the SDK <a href="http://Klimbz-releases.s3.amazonaws.com/Klimbz.v0.3.zip">here</a> and extract it to get the Klimbz.a and Klimbz.h files.</p>
              <p><strong>2.</strong> Drag and Drop both files to your project.</p>
              <p><strong>3.</strong> Import the framework into your application's delegate. I.e: AppDelegate.m.</p>
              <pre class=" language-c"><code class=" language-c">#import "Klimbz.h"</code></pre>
              <p><strong>4.</strong> Initialize Klimbz by adding this line somewhere in "application:didFinishLaunchingWithOptions:"</p>
              <pre class="language-c"><code class=" language-c">[Klimbz initWithApplicationKey:@"<?=$application->key?>"];</code></pre>
              <p><strong>5.</strong> Add "-ObjC" in your Build Settings->Other Linker Flags option</p>
              <h4>...and you are done. Start gathering feedback by shaking the iPhone or iPad.</h4>
            </div>
            <div class="tab-pane" id="connectors">
               <h4>Connectors</h4>
              
              <? foreach ($connectors as $connector) :?>
                <div class="media">
                  <a class="pull-left" href="#">
                    <img src="<?=$connector->icon?>" alt="service" style="width:50px;" align="left" />
                  </a>
                  <div class="media-body">
                    <div class="info">
                      <strong><?=$connector->name?></strong>
                      <p><b><?=$connector->destination_name?></b>: <?=$connector->destination?></p>
                      <? if ($role->name == "Owner") :?>
                        <a href="<?=site_url()?>connector/delete/<?=$application->id?>/<?=$connector->id?>">Delete</a>
                      <? endif; ?>
                      <br/><br/>
                    </div>
                  </div>
                </div>                              
              <? endforeach; ?>
              <? if ($role->name == "Owner") :?>          
                    <a href="<?=site_url()?>connector/create/<?=$application->id?>" class="join-team button button-small">Add Connector</a>  
                  <? endif; ?>
            </div>

          </div>


        </div>
      </div>
    </div>


<? $this->load->view('app/_common/footer',null); ?>

</body>

</html>