<? $this->load->view('app/_common/header',null); ?> 

<body>
  
        
        <? if ($role->name == "Owner") :?>
          <div class="">
            <? if ($application->upto_members <= count($members)) :?>
              <div class="alert alert-warning"><strong>Watch out!</strong> Maximum number of collaborators on free account reached.<strong><a href="<?=site_url()?>user/upgrade/<?=$application->id?>">Upgrade Account</a></strong> </div>
            <? else: ?>
              <div class="alert alert-info"><strong><?=$application->upto_members-count($members)?> collaborator slot left!</strong> You can remove this limit by <strong><a href="<?=site_url()?>user/upgrade/<?=$application->id?>">upgrading your account</a></strong></div>
            <? endif; ?>
          </div>
        <? endif; ?>
     
 <? $this->load->view('app/_common/navigation',null); ?> 

    <div class="container-fluid">
      
      
      <div class="row padding_10">
        <div class="col-sm-2">
          <form action="" method="get">
            <h5>Screens</h5>
            <? foreach ($metadata->view_controller as $view_controller): ?>
              <input type="checkbox" name="<?=$view_controller->name?>[]" value="<?=$view_controller->value?>" <?=(isset($filter[$view_controller->name]) && in_array($view_controller->value,$filter[$view_controller->name]))?"checked":""?>> <?=$view_controller->value?> <span class="label label-primary"><?=$view_controller->sumatory?></span></input>
              <br/>
            <? endforeach; ?>

            <h5>Application Version</h5>
            <? foreach ($metadata->application_version as $application_version): ?>
              <input type="checkbox" name="<?=$application_version->name?>[]" value="<?=$application_version->value?>" <?=(isset($filter[$application_version->name]) && in_array($application_version->value,$filter[$application_version->name]))?"checked":""?>> <?=$application_version->value?> <span class="label label-primary"><?=$application_version->sumatory?></span></input>
              <br/>
            <? endforeach; ?>

            <h5>System Version</h5>
            <? foreach ($metadata->system_version as $system_version): ?>
              <input type="checkbox" name="<?=$system_version->name?>[]" value="<?=$system_version->value?>" <?=(isset($filter[$system_version->name]) && in_array($system_version->value,$filter[$system_version->name]))?"checked":""?>> <?=$system_version->value?> <span class="label label-primary"><?=$system_version->sumatory?></span></input>
              <br/>
            <? endforeach; ?>

            <h5>Device</h5>
            <? foreach ($metadata->device as $device): ?>
              <input type="checkbox" name="<?=$device->name?>[]" value="<?=$device->value?>" <?=(isset($filter[$device->name]) && in_array($device->value,$filter[$device->name]))?"checked":""?>> <?=$device->value?> <span class="label label-primary"><?=$device->sumatory?></span></input>
              <br/>
            <? endforeach; ?>
            <br/><br/>
             <button id="btn-signup" type="submit" class="button button-small"><i class="icon-hand-right"></i> Refresh</button>
          </form>          

        </div>
          <div class="col-md-10">                 
            
              <div class="row">                 
                
                <div id="feedbacks"></div>
               
                <? foreach ($inputs as $input) :?>
                    <div class="col-md-2 feature">
                      <div style="width:100%;overflow:hidden; height:180px;">
                        <img src="<?=$input->image?>" alt="blog1" style="width:100%;margin-top:0%;margin-left:0%;" />
                      </div>     
                      <div style="width:100%;overflow:hidden; height:30px;">                                               
                        <p><?=$input->comment?></p>
                      </div>
                      <p><span class="label label-info"><?=$input->status?></span></p> <a href="<?=site_url()?>input/view/<?=$input->id?>">Details</a>
                      <br/><br/>
                    </div>    

                <? endforeach; ?>

              </div>
            

           </div>
      </div>
    </div>
  


<? $this->load->view('app/_common/footer',null); ?>

</body>

</html>