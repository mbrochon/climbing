<? $this->load->view('app/_common/header',null); ?> 

  <body id="features">
    <? $this->load->view('app/_common/navigation',null); ?> 


      <div id="contact">
          <div class="container">
              <div class="section_header">
                  <h3>New App</h3>
              </div>
              <div class="row ">

                <div class="col-md-7">                
                  <p>For now, we are only accepting iOS applications. </p>
                  <form role="form" id="create" method="POST" action="<?=site_url()?>app/create">
                    <div class="form-group">
                      <label for="name">Name or Bundle ID if your app is already in the App Store</label>
                      <input type="text" class="form-control" id="name" name="name" placeholder="i.e. Facebook or com.facebook.facebook" />
                    </div>
                    <div class="form-group">        
                      <button id="btn-signup" type="submit" class="button button-small"><i class="icon-hand-right"></i> Create</button>
                    </div>
                  </form>
                </div>                        
              </div>                    
          </div>
      </div>

    <? $this->load->view('app/_common/footer',null); ?>

  </body>

  </html>