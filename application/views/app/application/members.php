<? $this->load->view('app/_common/header',null); ?> 

<body>

        <? if ($role->name == "Owner") :?>
          <div class="">
            <? if ($application->upto_members <= count($members)) :?>
              <div class="alert alert-warning"><strong>Watch out!</strong> Maximum number of collaborators on free account reached.<strong><a href="<?=site_url()?>user/upgrade/<?=$application->id?>">Upgrade Account</a></strong> </div>
            <? else: ?>
              <div class="alert alert-info"><strong><?=$application->upto_members-count($members)?> collaborator slot left!</strong> You can remove this limit by <strong><a href="<?=site_url()?>user/upgrade/<?=$application->id?>">upgrading your account</a></strong></div>
            <? endif; ?>
          </div>
        <? endif; ?>
   

 <? $this->load->view('app/_common/navigation',null); ?> 

    <div class="container">
      
      
      <div class="row">
        <div class="col-md-12">
          <div class="section_header">
                  <h3>Members</h3>
              </div>
        <? if ($role->name == "Owner") :?>
          <p><?=$application->upto_members-count($members)?> collaborator slot left.</p>
        <? endif; ?>
        <? foreach ($members as $member) :?>
            <div class="media">
              <a class="pull-left" href="#">
                <img src="<?=$this->utils->get_gravatar_user($member)?>" align="left" />
              </a>
              <div class="media-body">
                <h4 class="media-heading"><?=$member->name?></h4>
                
              </div>
            </div>                              
        <? endforeach; ?>
<br/>
        <? if ($role->name == "Owner") :?>
          <? if ($application->upto_members <= count($members)) :?>
            <a href="<?=site_url()?>user/upgrade/<?=$application->id?>" class="button button-small">Upgrade Acccount</a>  
          <? else: ?>
            <a href="<?=site_url()?>user/invite/<?=$application->id?>" class="button button-small">Invite Member</a>  
          <? endif; ?>
        <? endif; ?>
      </div>
      </div>
    </div>


<? $this->load->view('app/_common/footer',null); ?>

</body>

</html>