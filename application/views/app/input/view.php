<? $this->load->view('app/_common/header',null); ?> 

<body id="features">
  
 <? $this->load->view('app/_common/navigation',array($application)); ?> 

   <div id="grid-second">
        <div class="container">
            <div class="section_header">

                <a href="javascript:window.history.back()"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
            </div>

            <div class="row">
              <div class="col-sm-3">
                  <h4><?=$input->comment?></h4>
                  <h6><?=$input->created_on?></h6>
                  <hr/>
                <form action="<?=site_url()?>input/update/<?=$input->id?>" method="POST">
                  <h5>Assigned to</h5>
                  <select name="assigned_to">                  
                    <option>--Select--</option>
                    <? foreach ($members as $member) :?>                   
                      <option value="<?=$member->id?>" <?=($input->assigned_to==$member->id)?"selected":""?>><?=$member->name?></option>
                    <? endforeach; ?>
                  </select>

                  <h5>Status</h5>
                  <select name="input_status">                  
                    <? foreach ($statuses as $status) :?>                   
                      <option value="<?=$status->id?>" <?=($input->input_status_id==$status->id)?"selected":""?>><?=$status->name?></option>
                    <? endforeach; ?>
                  </select>
                  <br/><br/>
                  <button id="btn-signup" type="submit" class="button button-small"><i class="icon-hand-right"></i> Update</button>
                </form>
                <hr/>
                <form action="<?=site_url()?>input/delete/<?=$input->id?>" method="POST">                  
                  <button id="btn-signup" type="submit" class="button-delete button-small"><i class="icon-hand-right"></i> Delete</button>
                </form>
              </div>
              <div class="col-sm-9">
                <div class="row"> 
                  <a href="#modal_image" id="modal_image_trigger" data-toggle="modal" data-img-url="<?=$input->image?>">
                  <img id="feedback_image" src="<?=$input->image?>" width="250" align="left" />
                  </a>
                  <? foreach ($metadata as $meta) :?>
                    <? if ($meta->metadata_type_id == 1): ?>
                      <div class="col-sm-4"><p><?=$meta->description?><b> <?=$meta->value?></b></p></div>
                    <? endif; ?>
                  <? endforeach; ?>
                </div>

                <div class="row"> 

                  <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h4 class="panel-title">
                          <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                            User Data
                          </a>
                        </h4>
                      </div>
                      <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="panel-body">
                          <? foreach ($metadata as $meta) :?>
                            <? if ($meta->metadata_type_id == 2): ?>
                              <p><?=($meta->description!="")?$meta->description:$meta->parent.".".$meta->name?><b> = <?=(trim($meta->value)!=null)?$meta->value:"-"?></b></p>
                            <? endif; ?>
                          <? endforeach; ?>
                        </div>
                      </div>
                    </div>
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h4 class="panel-title">
                          <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                            Application Log
                          </a>
                        </h4>
                      </div>
                      <div id="collapseTwo" class="panel-collapse collapse">
                        <div class="panel-body">
                          <? foreach ($logs as $log) :?>                            
                            <h6><b><?=$log->created_on?></b></h6>
                            <pre><?=$log->content?></pre>                            
                          <? endforeach; ?>
                        </div>
                      </div>
                    </div>
                   
                  </div>

                  
                </div>
                </div>
                 
            </div>            

      </div>
    </div>

    <!-- Modal -->
<div class="modal fade" id="modal_image" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><?=$input->comment?></h4>
      </div>
      <div class="modal-body">
        <center>
          <img src="#"/>
        </center>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>        
      </div>
    </div>
  </div>
</div>

  <script>
    $('#modal_image_trigger').click(function(e) {
      $('#modal_image img').attr('src', $(this).attr('data-img-url')); 
    });
  </script>

  <? $this->load->view('app/_common/footer',null); ?>

</body>

</html>