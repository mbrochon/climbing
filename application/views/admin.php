<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
<?php 
foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
	<script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
<style type='text/css'>
body
{
	font-family: Arial;
	font-size: 14px;
}
a {
    color: blue;
    text-decoration: none;
    font-size: 14px;
}
a:hover
{
	text-decoration: underline;
}
</style>
</head>
<body>
	<div>
		<a href='<?php echo site_url('admin/users')?>'>Users</a> |	
		<a href='<?php echo site_url('admin/grades')?>'>Grades</a> |
		<a href='<?php echo site_url('admin/colors')?>'>Colors</a> |
		<a href='<?php echo site_url('admin/gyms')?>'>Gyms</a> |
		<a href='<?php echo site_url('admin/gym_setters')?>'>Gym Setters</a> |
		<a href='<?php echo site_url('admin/gym_links')?>'>Gym Links</a> |
		<a href='<?php echo site_url('admin/walls')?>'>Walls</a> |
		<a href='<?php echo site_url('admin/routes')?>'>Routes</a>			
	</div>
	<div style='height:20px;'></div>  
    <div>
		<?php echo $output; ?>
    </div>
</body>
</html>
