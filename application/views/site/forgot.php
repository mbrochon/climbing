<!DOCTYPE html>
<html>
<head>

  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" /> 
  <title>Klimbz</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />

  <!-- stylesheets -->
  <link rel="stylesheet" href="<?=assets_url()?>vendors/clean/css/bootstrap/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="<?=assets_url()?>vendors/react/theme.css">
  <link rel="stylesheet" type="text/css" href="<?=assets_url()?>vendors/misc/animate.css">

  <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->

</head>
<body id="signup" style="background: url(<?=assets_url()?>images/header_bg_blur.jpg) no-repeat center center fixed; background-size:cover;">
  <div class="container">
    <div class="row header">
      <div class="col-md-12">
        <h3 class="logo">
          <a href="<?=site_url()?>"><img src="<?=assets_url()?>images/logo-white.png" width="150"/></a>
        </h3>
      
          <h4>Forgotten Password</h4>        
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="wrapper clearfix">
          <div class="formy">
            <div class="row">
              <div class="col-md-12">
                <form action="<?= site_url()?>user/forgot" method="post" id="form_forgot">
                 <div class="form-group">
                    <label for="email">Email address</label>
                    <input type="email" class="form-control" id="email" name="email" />
                  </div>   
                   <div class="form-group">
                      Did you remember your password? <a href="<?=site_url()?>site/login">Login here</a>
                    </div>    
                   <div class="submit">                    
                    <a href="javascript:$('#form_forgot').submit()" class="button-clear">
                      <span>Reset Password</span>
                    </a>
                  </div>
            </form>
              </div>
            </div>            
          </div>
        </div>        
      </div>
    </div>
  </div>

   <!-- javascript -->
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
  <script type="text/javascript" src="https://www.parsecdn.com/js/parse-1.2.16.min.js"></script> 
  <script type="text/javascript" src="<?=assets_url()?>js/app.js"></script>

</body>
</html>

