<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge"><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Track your climbing with Klimbz</title>

    <!-- Stylesheets -->
    <link rel="stylesheet" href="<?=assets_url()?>vendors/bootstrap/css/bootstrap.min.css">
    <link href="<?=assets_url()?>vendors/bootstrap/css/font-awesome.css" rel="stylesheet" />
    <link href="<?=assets_url()?>css/styles.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="<?=assets_url()?>js/html5shiv.js"></script>
      <script src="<?=assets_url()?>js/respond.min.js"></script>
    <![endif]-->

    <!-- Favicons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/ico/apple-touch-icon-144-precomposed.png">
    <link rel="shortcut icon" href="img/ico/favicon.png">


  </head>

  <body>
  
    <header id="site-header" class="site-header">
      <div class="container">
        <h1><a class="site-title pull-left" href="#"></a></h1>
        <div class="btn-users text-right">
          <a class="btn btn-login" href="<?=site_url()?>site/login">Sign in</a>         
        </div>
      </div><!-- /container -->
      
      <div class="container">
          
        <div class="header-description">
          <h2>  Track your climbs easily
      with Klimbz.</h2>
          <a class="btn btn-lg btn-header" href="<?=site_url()?>site/signup">Join now</a>
        </div><!-- /.header-description -->
        
        <div class="header-features">
          <div class="row">
            <div class="col-sm-8 header-feature">
              <h3> <i class="fa fa-group"></i> &nbsp; Create a better climbing community</h3>
              <p>Klimbz integrates a system for gyms and setters to easily manage route setting with a simple interface for climbers. The system provides visual feedback for climbers and data for gyms.</p>
              <p>The Klimbz system allows gym climbers to track their routes, communicate with each other and the gym setters. Gyms can analyze, understand and get feedback from their members. Setters now have a way to communicate with their clients, get feedback on their setting and have a record of every route on every wall.</p>
            </div>
          </div>
        </div><!-- /.header-features -->
            
      </div><!-- /.container -->
    </header>
    
    <section id="slider" class="slider text-center">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <center>
                  <img src="<?=assets_url()?>images/demo1.png" class="img-responsive" alt="">
                  <br/>
            <center>
            
          </div><!-- /span12 -->
        </div><!-- /row -->
      </div><!-- /container -->
    </section>
       
    <section id="features" class="features">
      <div class="container">
        <div class="row">
          
          <div class="col-sm-4">
            <div class="feature">
              <i class="fa pull-left"><img src="<?=assets_url()?>images/biner.png" alt="biner"> </i>
              <h4>For climbers</h4>
              <p>In order to get better, you need to know what you are doing. Track, visualize and analyze what you are climbing in a gym. Easily tick gym routes. See a graph of all the problems you have completed. Discuss the problems with other climbers. Upload videos and pictures. Provide route setters with feedback on their work.</p>
            </div>
          </div><!-- /span3 -->
          
          <div class="col-sm-4">
            <div class="feature" style="border-right: 1px solid rgba(255, 255, 255, 0.3);border-left: 1px solid rgba(255, 255, 255, 0.3); padding:0px 20px;">
              <i class="fa fa-wrench fa fa-2x pull-left"></i>
              <h4>For Setters</h4>
              <p>Easily add problems as you reset a wall. Upload videos and stills of the routes being sent. Setters have a complete record of the routes set in the gym. Get a visualization of how many problems at each grade, on each wall within in the gym. Receive important feedback on their hard work from the gym clientele.</p>
            </div>
          </div><!-- /span3 -->
          
          <div class="col-sm-4">
            <div class="feature">
              <i class="fa fa-desktop fa fa-2x pull-left"></i>
              <h4>For Gyms</h4>
              <p>Provide a great service to your gym community. Enhance the gym climbing experience with a digitized gym. Provide a forum for your gym community to discuss what is important to them - the routes and getting better. A simple way to quantify the hard work your setters are doing, rate problems, customer feedback, visualize data of every setter, and route.</p>
            </div>
          </div><!-- /span3 -->
          
          
        </div>
      </div>
    </section>
    
    <section id="contact" class="contact">

      <div class="container">
        <div class="row">
          <div class="col-sm-6">
            <h2>Get in touch</h2>
            <p>Let's make stronger and better climbers. Get in touch with us and we can set up Klimbz for your gym and community.</p>
          </div>
          <div class="col-sm-6">
            <h2>Email Us&nbsp;</h2>
      <a href="mailto:info@klimbz.com?Subject=Hello" target="_top">  <i class="fa fa-envelope contact-form-btn closes"></a></i>
        
            </p>
          </div>
        </div>
      </div>      
    </section>
    
    <footer id="footer" class="footer">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <br/>
            <img src="<?=assets_url()?>images/logo.png" alt="klimbz logo" width="80">
            <p class="pull-right copyright">&copy; <?=date('Y')?> All Right Reserved.</p>
          </div>
        </div>
      </div>
    </footer>

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="<?=assets_url()?>vendors/bootstrap/js/bootstrap.min.js"></script>
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1956963-13', 'auto');
  ga('send', 'pageview');


</script>
  </body>
</html>