<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<?php echo $head; ?>
<style>
  p { color:#666!important; line-height:120%!important; font-size:12px!important; padding-top:20px;margin-bottom:20px }
  h1, h2, h3, h4, h4 { color:#666!important; line-height:120%!important; padding:10px; }
  h5 { font-size: 15px; font-weight: 700; }
  h6 { color:#666!important; font-size: 20px; font-weight: 700; letter-spacing: 0px; text-align: left;  } 
</style>
<body>
  <div id="wrap">
      <div class="row-fluid">        
        <div class="span12">  
          <div class="container">       
              <img style="margin-top:2%" src="<?=asset_url()?>images/logo-text.png" width="" /> 
              <br/><br/>
              <h6>Privacy Policy</h6>
          </div>
        </div><!--/span-->
      </div><!--/row-->      
      <div class="row-fluid">        
        <div class="span12">       
          <div class="container" style="">
           	<h5>General Information</h5><p>
We collect the e-mail addresses of those who communicate with us via e-mail, aggregate information on what pages consumers access or visit, and information volunteered by the consumer (such as survey information and/or site registrations). The information we collect is used to improve the content of our Web pages and the quality of our service, and is not shared with or sold to other organizations for commercial purposes, except to provide products or services you've requested, when we have your permission, or under the following circumstances:
		It is necessary to share information in order to investigate, prevent, or take action regarding illegal activities, suspected fraud, situations involving potential threats to the physical safety of any person, violations of Terms of Service, or as otherwise required by law.

</p><h5>Information Gathering and Usage</h5><p>
		When you register for Kloc we ask for information such as your name, email address and billing information. Members who sign up for the free account are not required to enter a credit card.
		Kloc uses collected information for the following general purposes: extraction of skills, billing, identification and authentication, services improvement, contact, and research.
</p><h5>Cookies</h5><p>
		A cookie is a small amount of data, which often includes an anonymous unique identifier, that is sent to your browser from a web site's computers and stored on your computer's hard drive.
		Cookies are required to use the Kloc service.
		We use cookies to record current session information, but do not use permanent cookies. You are required to re-login to your Kloc account after a certain period of time has elapsed to protect you against others accidentally accessing your account contents.
</p><h5>Data Storage</h5><p>
Kloc uses third party vendors and hosting partners to provide the necessary hardware, software, networking, storage, and related technology required to run Kloc. Although Kloc owns the code, databases, and all rights to the Kloc application, you retain all rights to your data.
</p><h5>Disclosure</h5><p>
Kloc may disclose personally identifiable information under special circumstances, such as to comply with subpoenas or when your actions violate the Terms of Service.
</p><h5>Changes</h5><p>
Kloc may periodically update this policy. We will notify you about significant changes in the way we treat personal information by sending a notice to the primary email address specified in your Kloc primary account holder account or by placing a prominent notice on our site.</p>
<h5>Questions</h5><p>
Any questions about this Privacy Policy should be addressed to support@kloc.me</p>

          </div>
        </div><!--/span-->
      </div><!--/row-->
      <div class="row-fluid footer">  
        <?php $this->load->view('footer_site',null);  ?>
      </div><!--/row-->

<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-38264293-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!-- JS
================================================== -->
<script src="http://code.jquery.com/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="<?= asset_url()?>js/mailing.js"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.8.24/themes/base/jquery-ui.css">
<script src="http://code.jquery.com/jquery-1.8.2.js"></script>
<script src="http://code.jquery.com/ui/1.8.24/jquery-ui.js"></script>
<script type="text/javascript" src="<?= asset_url()?>js/kloc.js"></script>
<script>
  $(document).keypress(function(e) {
  if(e.which == 13) {
    doLogin();
  }
  });

</script>
</body>

</html>
