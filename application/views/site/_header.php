<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><?php echo $head['title'] ?></title>

<meta name="viewport" content="">
<script>var DOMAIN_APP = "<?=site_url();?>";</script>
<!-- CSS
================================================== -->

<style type="text/css">
  body {
    
  }
  .sidebar-nav {
    padding: 9px 0;
  }

  @media (max-width: 980px) {
    /* Enable use of floated navbar text */
    .navbar-text.pull-right {
      float: none;
      padding-left: 5px;
      padding-right: 5px;
    }
  }
  .container {
    width: auto;
  }
  .container .credit {
    margin: 20px 0;
  }
</style>

<!--
<link href="<?=asset_url()?>bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">

<style>@import url('<?=asset_url()?>css/kloc.css');</style>






-->


<!-- Montserrat Sans -->
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400, 700' rel='stylesheet' type='text/css'>        
    <!-- Lato Font -->
    <link href='http://fonts.googleapis.com/css?family=Lato:300,700' rel='stylesheet' type='text/css'>

    <!-- Stylesheet -->
    <!--<link href="<?=asset_url()?>home/css/style.css" rel="stylesheet">-->
    <!--[if lt IE 9]>
      <script src="<?=asset_url()?>home/js/respond.min.js"></script>
    <![endif]-->
    
<!-- Favicons
================================================== -->
<!--<link rel="shortcut icon" href="<?=asset_url()?>images/kloc_favicon_32.png">
<link rel="icon" type="image/png" href="<?=asset_url()?>images/kloc_favicon_32.png" />
<link rel="apple-touch-icon" href="<?=asset_url()?>images/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?=asset_url()?>images/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?=asset_url()?>images/apple-touch-icon-114x114.png">
-->


<meta charset="UTF-8" />

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

<link rel="profile" href="http://gmpg.org/xfn/11" />

<!--[if IE 7]>
  <link rel="stylesheet" href="/css/font-awesome-ie7.css">
<![endif]-->

<link rel='stylesheet' id='bootstrap-styles-css'  href='<?=asset_url()?>bootstrap/css/bootstrap.css' type='text/css' media='all' />
<link rel="stylesheet" href="http://code.jquery.com/ui/1.8.24/themes/base/jquery-ui.css">
<link rel='stylesheet' id='responsive-style-css'  href='<?=asset_url()?>pinterest/style.css' type='text/css' media='all' />
<link rel="stylesheet" href="<?=asset_url()?>home/fonts/font-awesome/css/font-awesome.min.css">


<style>@import url('<?=asset_url()?>css/kloc.css');</style>
<style>@import url('<?=asset_url()?>css/theme_b.css');</style>
<style>@import url('<?=asset_url()?>css/navigation.css');</style>
<style>@import url('<?=asset_url()?>css/typography.css');</style>
<link href="<?=asset_url()?>css/form_elements.css" rel="stylesheet" type="text/css">
<link href="<?=asset_url()?>css/boxes.css" rel="stylesheet" type="text/css">

<? foreach ($head as $key => $value): ?>
    <meta property="<?=$key?>" content="<?=$value?>"/>
<? endforeach; ?>

<script src="http://code.jquery.com/jquery-1.8.3.min.js"></script>

</head>