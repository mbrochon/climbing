<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<?php echo $head; ?>

<body>
	<div id="wrap">
      <div class="row-fluid">        
        <div class="span12 container-tag">  
          <div class="container">        
              <h6>About</h6>
              <img src="<?= asset_url()?>images/logo-round-linecode.png" alt="Kloc" class="logo-round"/> 
          </div>
        </div><!--/span-->
      </div><!--/row-->
      
      <div class="row-fluid">        
	      <div class="span12 about">       
	       <div class="container">  
	          <div class="container" style="margin-bottom:30px;">  
	            <p>Kloc is a tool for Developers that redefines the Portfolio Experience. Kloc automatically generates beautiful, visual and interactive Portfolios. <br/><strong>Just for Developers.</strong></p> 
	          </div>
	          <img class="divider" src="<?=asset_url()?>images/divider.png" />
	          <div class="container team">            
                <div class="span4">
                  <img class="img-circle" src="<?=asset_url()?>images/team-juan.png">
                  <h1>Juan I. Salas</h1>
                  <p>Head of Development</p>              
                </div><!-- /.span4 -->
                <div class="span4">
                  <img class="img-circle" src="<?=asset_url()?>images/team-franco.png">
                  <h1>Franco Carrera</h1>
                  <p>Creative Designer</p>              
                </div><!-- /.span4 -->
                <div class="span4">
                  <img class="img-circle" src="<?=asset_url()?>images/team-enrique.png">
                  <h1>C. Enrique Ortiz</h1>
                  <p>Product and Operations</p>              
                </div><!-- /.span4 -->                
              </div><!--/span-->
	       </div>
	    </div><!--/span-->
      </div><!--/row-->
       <div class="row-fluid footer">  
        <?php $this->load->view('footer_site',null);  ?>
      </div><!--/row-->
    </div>
    
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-38264293-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!-- JS
================================================== -->
<script src="http://code.jquery.com/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="<?= asset_url()?>js/mailing.js"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.8.24/themes/base/jquery-ui.css">
<script src="http://code.jquery.com/jquery-1.8.2.js"></script>
<script src="http://code.jquery.com/ui/1.8.24/jquery-ui.js"></script>
<script type="text/javascript" src="<?= asset_url()?>js/kloc.js"></script>
<script>
  $(document).keypress(function(e) {
  if(e.which == 13) {
    doLogin();
  }
  });

</script>
</body>

</html>
