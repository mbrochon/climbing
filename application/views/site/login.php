<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" /> 
  <title>Klimbz</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />

  <!-- stylesheets -->
  <link rel="stylesheet" href="<?=assets_url()?>vendors//clean/css/bootstrap/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="<?=assets_url()?>vendors//react/theme.css">
  <link rel="stylesheet" type="text/css" href="<?=assets_url()?>vendors//misc/animate.css">

  
</head>
<body id="signup" style="background: url(<?=assets_url()?>images/header_bg_blur.jpg) no-repeat center center fixed; background-size:cover;">
  <div class="container">
    <div class="row header">
      <div class="col-md-12">
        <h3 class="logo">
          <a href="<?=site_url()?>"><img src="<?=assets_url()?>images/logo-white.png" width="150"/></a>
        </h3>
        <h4>Sign in to your account.</h4>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="wrapper clearfix">
          <div class="formy">
            <div class="row">
              <div class="col-md-12">
                <form id="login" role="form" method="post" action="<?=site_url()?>user/validate">
                    <? if(isset($error)){?>
                        <div class="alert alert-danger">
                            <?=$error?>
                        </div>
                    <? }?>
                    <div class="form-group">
                      <label for="email">Email address</label>
                      <input type="email" class="form-control" id="email" name="email" />
                    </div>
                    <div class="form-group">
                      <label for="password">Password</label>
                      <input type="password" class="form-control" id="password" name="password" />
                    </div>              
                    <div class="form-group">
                      Did you forget your password? <a href="<?=site_url()?>site/forgot">Reset it here</a>
                    </div>      
                    <div class="submit">
                      <a href="javascript:$('#login').submit()" class="button-clear">
                        <span>Sign in</span>
                      </a>
                    </div>
                </form>
              </div>
            </div>            
          </div>
        </div>        
      </div>
    </div>
  </div>
  
  <!-- javascript -->
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
  <script type="text/javascript" src="https://www.parsecdn.com/js/parse-1.2.16.min.js"></script>  
  <script type="text/javascript" src="<?=assets_url()?>js/app.js"></script>

  <script>
    $(document).keypress(function(e) {
      if(e.which == 13) {
        $("#login").submit();
      }
    });
  </script>
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1956963-13', 'auto');
  ga('send', 'pageview');

</script>


</body>
</html>