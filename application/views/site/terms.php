<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<?php echo $head; ?>
<style>
	p { color:#666!important; line-height:120%!important; font-size:12px!important; padding-top:20px;margin-bottom:20px }
	h1, h2, h3, h4, h4 { color:#666!important; line-height:120%!important; padding:10px; }
	h5 { font-size: 15px; font-weight: 700; }
	h6 { color:#666!important; font-size: 20px; font-weight: 700; letter-spacing: 0px; text-align: left;  } 
</style>
<body>
	<div id="wrap">
      <div class="row-fluid">        
        <div class="span12">  
          <div class="container">       
          		<img style="margin-top:2%" src="<?=asset_url()?>images/logo-text.png" width="" /> 
          		<br/><br/>
              <h6>Terms of Service</h6>
          </div>
        </div><!--/span-->
      </div><!--/row-->      
      <div class="row-fluid">        
        <div class="span12">       
          <div class="container" style="">
           	<p>By using the Kloc.me web site ("Service"), or any services of Laterous, LLC("Kloc"), you are agreeing to be bound by the following terms and conditions ("Terms of Service"). IF YOU ARE ENTERING INTO THIS AGREEMENT ON BEHALF OF A COMPANY OR OTHER LEGAL ENTITY, YOU REPRESENT THAT YOU HAVE THE AUTHORITY TO BIND SUCH ENTITY, ITS AFFILIATES AND ALL USERS WHO ACCESS OUR SERVICES THROUGH YOUR ACCOUNT TO THESE TERMS AND CONDITIONS, IN WHICH CASE THE TERMS "YOU" OR "YOUR" SHALL REFER TO SUCH ENTITY, ITS AFFILIATES AND USERS ASSOCIATED WITH IT. IF YOU DO NOT HAVE SUCH AUTHORITY, OR IF YOU DO NOT AGREE WITH THESE TERMS AND CONDITIONS, YOU MUST NOT ACCEPT THIS AGREEMENT AND MAY NOT USE THE SERVICES.
Please note that if you are accessing any Kloc service in a government or public entity capacity, there are special terms that may apply to you. Please see Section G.17, below, for more details.
If Kloc makes material changes to these Terms, we will notify you by email or by posting a notice on our site before the changes are effective. Any new features that augment or enhance the current Service, including the release of new tools and resources, shall be subject to the Terms of Service. Continued use of the Service after any such changes shall constitute your consent to such changes. 
Violation of any of the terms below will result in the termination of your Account. While Kloc prohibits such conduct and Content on the Service, you understand and agree that Kloc cannot be responsible for the Content posted on the Service and you nonetheless may be exposed to such materials. You agree to use the Service at your own risk.</p>
<h5>A. Account Terms</h5>	<p>
	.	You must be 13 years or older to use this Service.
	.	You must be a human. Accounts registered by "bots" or other automated methods are not permitted.
	.	You must provide your legal full name, a valid email address, and any other information requested in order to complete the signup process.
	.	Your login may only be used by one person - a single login shared by multiple people is not permitted. You may create separate logins for as many people as your plan allows.
	.	You are responsible for maintaining the security of your account and password. Kloc cannot and will not be liable for any loss or damage from your failure to comply with this security obligation.
	.	You are responsible for all Content posted and activity that occurs under your account (even when Content is posted by others who have accounts under your account).
	.	One person or legal entity may not maintain more than one free account.
	.	You may not use the Service for any illegal or unauthorized purpose. You must not, in the use of the Service, violate any laws in your jurisdiction (including but not limited to copyright or trademark laws).
</p><h5>B. API Terms</h5>	<p>
Customers may access their Kloc account data via an API (Application Program Interface). Any use of the API, including use of the API through a third-party product that accesses Kloc, is bound by these Terms of Service plus the following specific terms:
	.	You expressly understand and agree that Kloc shall not be liable for any direct, indirect, incidental, special, consequential or exemplary damages, including but not limited to, damages for loss of profits, goodwill, use, data or other intangible losses (even if Kloc has been advised of the possibility of such damages), resulting from your use of the API or third-party products that access data via the API.
	.	Abuse or excessively frequent requests to Kloc via the API may result in the temporary or permanent suspension of your account's access to the API. Kloc, in its sole discretion, will determine abuse or excessive usage of the API. Kloc will make a reasonable attempt via email to warn the account owner prior to suspension.
	.	Kloc reserves the right at any time to modify or discontinue, temporarily or permanently, your access to the API (or any part thereof) with or without notice.
</p><h5>C. Payment, Refunds, Upgrading and Downgrading Terms</h5>	<p>
	.	All paid plans must enter a valid credit card. Free accounts are not required to provide a credit card number.
	.	An upgrade from the free plan to any paying plan will immediately bill you.
	.	The Service is billed in advance on a monthly basis and is non-refundable. There will be no refunds or credits for partial months of service, upgrade/downgrade refunds, or refunds for months unused with an open account. In order to treat everyone equally, no exceptions will be made.
	.	All fees are exclusive of all taxes, levies, or duties imposed by taxing authorities, and you shall be responsible for payment of all such taxes, levies, or duties, excluding only United States (federal or state) taxes.
	.	For any upgrade or downgrade in plan level, your credit card that you provided will automatically be charged the new rate on your next billing cycle.
	.	Downgrading your Service may cause the loss of Content, features, or capacity of your Account. Kloc does not accept any liability for such loss.
</p><h5>D. Cancellation and Termination</h5>	<p>
	.	You are solely responsible for properly canceling your account. An email or phone request to cancel your account is not considered cancellation. You can cancel your account at any time by clicking on the Account link in the global navigation bar at the top of the screen. The Account screen provides a simple no questions asked cancellation link.
	.	All of your Content will be immediately deleted from the Service upon cancellation. This information can not be recovered once your account is cancelled.
	.	If you cancel the Service before the end of your current paid up month, your cancellation will take effect immediately and you will not be charged again.
	.	Kloc, in its sole discretion, has the right to suspend or terminate your account and refuse any and all current or future use of the Service, or any other Kloc service, for any reason at any time. Such termination of the Service will result in the deactivation or deletion of your Account or your access to your Account, and the forfeiture and relinquishment of all Content in your Account. Kloc reserves the right to refuse service to anyone for any reason at any time.
</p><h5>E. Modifications to the Service and Prices</h5>	<p>
	.	Kloc reserves the right at any time and from time to time to modify or discontinue, temporarily or permanently, the Service (or any part thereof) with or without notice.
	.	Prices of all Services, including but not limited to monthly subscription plan fees to the Service, are subject to change upon 30 days notice from us. Such notice may be provided at any time by posting the changes to the Kloc Site (Kloc.me) or the Service itself.
	.	Kloc shall not be liable to you or to any third party for any modification, price change, suspension or discontinuance of the Service.
</p><h5>F. Copyright and Content Ownership</h5>	<p>
	.	We claim no intellectual property rights over the material you provide to the Service. Your profile and materials uploaded remain yours. However, by setting your pages to be viewed publicly, you agree to allow others to view your Content. By setting your repositories to be viewed publicly, you agree to allow others to view and fork your repositories.
	.	Kloc does not pre-screen Content, but Kloc and its designee have the right (but not the obligation) in their sole discretion to refuse or remove any Content that is available via the Service.
	.	You shall defend Kloc against any claim, demand, suit or proceeding made or brought against Kloc by a third party alleging that Your Content, or Your use of the Service in violation of this Agreement, infringes or misappropriates the intellectual property rights of a third party or violates applicable law, and shall indemnify Kloc for any damages finally awarded against, and for reasonable attorney’s fees incurred by, Kloc in connection with any such claim, demand, suit or proceeding; provided, that Kloc (a) promptly gives You written notice of the claim, demand, suit or proceeding; (b) gives You sole control of the defense and settlement of the claim, demand, suit or proceeding (provided that You may not settle any claim, demand, suit or proceeding unless the settlement unconditionally releases Kloc of all liability); and (c) provides to You all reasonable assistance, at Your expense.
	.	The look and feel of the Service is copyright ©2010 Kloc Inc. All rights reserved. You may not duplicate, copy, or reuse any portion of the HTML/CSS, Javascript, or visual design elements or concepts without express written permission from Kloc.
</p><h5>G. General Conditions</h5>	<p>
	.	Your use of the Service is at your sole risk. The service is provided on an "as is" and "as available" basis.
	.	Support for Kloc services is only available in English, via email.
	.	You understand that Kloc uses third party vendors and hosting partners to provide the necessary hardware, software, networking, storage, and related technology required to run the Service.
	.	You must not modify, adapt or hack the Service or modify another website so as to falsely imply that it is associated with the Service, Kloc, or any other Kloc service.
	.	You may use Kloc subdomains (e.g., yourname.Kloc.io) solely as permitted and intended by the Kloc Pages tool to host your company pages, personal pages, or open source project pages, and for no other purpose. You may not use Kloc subdomains in violation of Kloc's trademark or other rights or in violation of applicable law. Kloc reserves the right at all times to reclaim any Kloc subdomain without liability to you.
	.	You agree not to reproduce, duplicate, copy, sell, resell or exploit any portion of the Service, use of the Service, or access to the Service without the express written permission by Kloc.
	.	We may, but have no obligation to, remove Content and Accounts containing Content that we determine in our sole discretion are unlawful, offensive, threatening, libelous, defamatory, pornographic, obscene or otherwise objectionable or violates any party's intellectual property or these Terms of Service.
	.	Verbal, physical, written or other abuse (including threats of abuse or retribution) of any Kloc customer, employee, member, or officer will result in immediate account termination.
	.	You understand that the technical processing and transmission of the Service, including your Content, may be transfered unencrypted and involve (a) transmissions over various networks; and (b) changes to conform and adapt to technical requirements of connecting networks or devices.
	.	You must not upload, post, host, or transmit unsolicited email, SMSs, or "spam" messages.
	.	You must not transmit any worms or viruses or any code of a destructive nature.
	.	If your bandwidth usage significantly exceeds the average bandwidth usage (as determined solely by Kloc) of other Kloc customers, we reserve the right to immediately disable your account or throttle your file hosting until you can reduce your bandwidth consumption.
	.	Kloc does not warrant that (i) the service will meet your specific requirements, (ii) the service will be uninterrupted, timely, secure, or error-free, (iii) the results that may be obtained from the use of the service will be accurate or reliable, (iv) the quality of any products, services, information, or other material purchased or obtained by you through the service will meet your expectations, and (v) any errors in the Service will be corrected.
	.	You expressly understand and agree that Kloc shall not be liable for any direct, indirect, incidental, special, consequential or exemplary damages, including but not limited to, damages for loss of profits, goodwill, use, data or other intangible losses (even if Kloc has been advised of the possibility of such damages), resulting from: (i) the use or the inability to use the service; (ii) the cost of procurement of substitute goods and services resulting from any goods, data, information or services purchased or obtained or messages received or transactions entered into through or from the service; (iii) unauthorized access to or alteration of your transmissions or data; (iv) statements or conduct of any third party on the service; (v) or any other matter relating to the service.
	.	The failure of Kloc to exercise or enforce any right or provision of the Terms of Service shall not constitute a waiver of such right or provision. The Terms of Service constitutes the entire agreement between you and Kloc and govern your use of the Service, superseding any prior agreements between you and Kloc (including, but not limited to, any prior versions of the Terms of Service). You agree that these Terms of Service and Your use of the Service are governed under California law.
	.	Questions about the Terms of Service should be sent to support@Kloc.me.</p>
          
        </div><!--/span-->
      </div><!--/row-->
    </div>
    <div class="row-fluid footer">  
        <?php $this->load->view('footer_site',null);  ?>
      </div><!--/row-->
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-38264293-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!-- JS
================================================== -->
<script src="http://code.jquery.com/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="<?= asset_url()?>js/mailing.js"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.8.24/themes/base/jquery-ui.css">
<script src="http://code.jquery.com/jquery-1.8.2.js"></script>
<script src="http://code.jquery.com/ui/1.8.24/jquery-ui.js"></script>
<script type="text/javascript" src="<?= asset_url()?>js/kloc.js"></script>

</body>

</html>
