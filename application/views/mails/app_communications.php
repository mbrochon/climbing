<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        
        <!-- Facebook sharing information tags -->
        <meta property="og:title" content="<?php echo $page_title ?>" />
        
        <title><?php echo $page_title ?></title>

  </head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" style="background-color:#fff">
      <center>
          <table style="background-color:#1E3F6D;" width="100%">
            <tr>
              <td>
                <br/>
                <table style="margin:0 auto;" width="600">
                  <tr>
                    <td>
                      <br/><br/>
                      <img src="<?=assets_url()?>images/logo.png" width="200" border="0" alt="Klimbz"/>
                      <br/>

                    </td>
                  </tr>
                  <tr>
                    <td>
                      <span style="color:#FFF;font-size:40px;font-weight:bold; font-family:'Arial';">
                        <?=$title?>
                      </span>
                      <br/>
                      <span style="color:#FFF;font-size:16px;font-weight:100; font-family:'Arial';">
                        <?=$description?>
                      </span>
                      <br/>

                      <? if (isset($link)):?>
                        <br/>
                        <br/>
                        <span style="color:#FFF;background-color:#4589E3;font-size: 15px; border-radius: 2px;padding:10px; font-family:'Arial';">
                          <a href="<?=$link?>" style="color:#FFF; text-decoration:none;"><?=$link_text?></a>
                        </span>
                      <? endif; ?>
                      <br/><br/>
                    </td>
                  </tr>
                </table>
                <br/>
              </td>
            </tr>
          </table>
          <table style="background-color:#FFF;" width="100%">
            <tr>
              <td>
                <br/>
                <table style="margin:0 auto;" width="600">
                  <tr>
                    <td colspan="2">
                      <br/><br/>
                      <center>
                      <span style="color:#888888;display:block;font-size:15px;font-weight:light; font-family:'Arial';">                        
                        Track your climbs easily with Klimbz.
                      </span>
                      </center>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <br/><br/>
                       <img src="<?=assets_url()?>images/logo.png" border="0" alt="Klimbz" width="60">          
                    </td>
                    <td style="text-align:right;">
                        <br/><br/>
                        <span style="color:#888888;display:block;font-size:11px;font-weight:light; font-family:'Arial';">
                        <?=date('Y')?> &copy; All Rights Reserved.         
                        </span>
                    </td>
                  </tr>
                </table>
                <br/>
              </td>
            </tr>
          </table>
        </center>
    </body>
</html>