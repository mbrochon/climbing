<?php
class Wall_model extends CI_Model {

    var $id   		            = '';
    var $gym_id 		= '';       
    var $name	            = '';    
   
    function __construct()
    {        
        parent::__construct();        
    }
 
	function get_by_gym($gym_id)
	{

		 $query = $this->db->query('
        select
            w.*, (select count(*) from routes r where r.wall_id = w.id and r.is_archived != 1) as routes, 
            (select max(g.value) from routes r join grades g on g.id = r.grade_id where r.wall_id = w.id and r.is_archived != 1) as max_grade,
            (select min(g.value) from routes r join grades g on g.id = r.grade_id where r.wall_id = w.id and r.is_archived != 1) as min_grade,
            (select path from wall_images where wall_id = w.id order by created_on DESC limit 0,1) as image,
            (select created_on from routes r where r.wall_id = w.id order by created_on desc limit 0,1) as updated
        from
            walls w
        where
            w.gym_id = '.$gym_id.'
        order by
            w.name 

        ');

        $results =  $query->result() ;

        return $results;

	}

    function get($id)
    {
        $query = $this->db->query('
        select
            w.*, (select count(*) from routes r where r.wall_id = w.id) as routes, 
            (select max(g.value) from routes r join grades g on g.id = r.grade_id where r.wall_id = w.id and r.is_archived != 1) as max_grade,
            (select min(g.value) from routes r join grades g on g.id = r.grade_id where r.wall_id = w.id and r.is_archived != 1) as min_grade,
            (select path from wall_images where wall_id = w.id order by created_on DESC limit 0,1) as image 
        from
            walls w
        where
            w.id = '.$id.'

        order by
            w.name 

        ');

        $results =  $query->result() ;

        return $results[0];

    }

    function get_statuses()
    {

        $this->db->select('*');
        $this->db->from('input_statuses');        
        
        $query = $this->db->get();      
        
        return $query->result();

    }

	
	function create()
    {        		

		$this->db->insert('walls', $this);      

    	$this->id = $this->db->insert_id(); 
    		
    	return $this;      
    }

    function get_images($wall_id)
    {
        $this->db->select('*');
        $this->db->from('wall_images');        
        $this->db->where('wall_id', $wall_id);
        $this->db->order_by("created_on", "desc"); 

        $query = $this->db->get();      
        
        return $query->result();

    }

    function add_image($wall_id, $user_id, $image_url)
    {               
        $data = array(
            'created_by' => $user_id,
            'wall_id' => $wall_id,
            'path' => $image_url
        );

        $this->db->insert('wall_images', $data);      
    }

    function update()
    {               
        unset($this->created_on);
        $this->db->where('id', $this->id);
        $this->db->update('input', $this);      
    }

    function archive_routes($wall_id)
    {               
        $data = array(
            'is_archived' => 1            
        );

        $this->db->where('wall_id', $wall_id);
        $this->db->update('routes', $data);      
    }
    
    function get_types()
    {

        $this->db->select('wt.*');
        $this->db->from('wall_types wt');      

        $query = $this->db->get();

        return $query->result();  
    }

}