<?php
class Route_model extends CI_Model {

    var $id   		= '';
    var $name 		= '';    
    var $created_by      = '';    
    var $wall_id         = '';    

    function __construct()
    {        
        parent::__construct();        
    }

    function create()
    {                       
        $this->db->insert('routes', $this);      
        $this->id = $this->db->insert_id();             
        return $this;      
    }

    function get_updated_recent($gym_id)
    {
        
        $this->db->select('r.*, c.name as color, g.name as grade, w.name as wall, r.id as route_id, w.id as wall_id, u.name as setter, u.email email, u.id as user_id,u.profile_picture_url as profile_picture_url, (select avg(value) from route_ratings where route_id = r.id) as rating, CONCAT("0") climbed_times');            
        $this->db->from('routes r'); 
        $this->db->join('colors c','c.id = r.color_id');
        $this->db->join('grades g','g.id = r.grade_id');       
        $this->db->join('users u','u.id = r.created_by');       
        $this->db->join('walls w','w.id = r.wall_id');    
        $this->db->where('w.gym_id', $gym_id);
        $this->db->where('r.is_archived', 0);
        $this->db->order_by('r.created_on','desc'); 
        $this->db->limit(5);

        $query = $this->db->get();

        return $query->result();
    }

    function get_by_wall($wall_id, $is_archived = 0, $user_id = null)
    {
        if ($user_id!=null) {
            $this->db->select('r.*, c.name as color, g.name as grade, u.name as setter, u.id as setter_id, (select avg(value) from route_ratings where route_id = r.id) as rating, (select count(*) from climbs where is_attempt = 0 and route_id = r.id and created_by = ' . $user_id .' group by route_id) climbed_times, (select count(*) from climbs where is_attempt = 1 and route_id = r.id and created_by = ' . $user_id .' group by route_id) climbed_attempts');
        }
        else
        {
            $this->db->select('r.*, c.name as color, g.name as grade, u.name as setter, u.id as setter_id, (select avg(value) from route_ratings where route_id = r.id) as rating, CONCAT("0") climbed_times');    
        }
        
        $this->db->from('routes r'); 
        $this->db->join('colors c','c.id = r.color_id');
        $this->db->join('grades g','g.id = r.grade_id');       
        $this->db->join('users u','u.id = r.created_by');       
        $this->db->where('r.wall_id', $wall_id);
        $this->db->where('r.is_archived', $is_archived);
        $this->db->order_by('g.value','asc'); 

        $query = $this->db->get();

        return $query->result();
    }

    function get($id, $user_id = null)
    {
        if ($user_id!=null) {
            $this->db->select('r.*, , c.name as color, g.name as grade, u.id user_id, u.name as setter, u.id as setter_id, (select count(*) from climbs where route_id = r.id) as climbs, (select avg(value) from route_ratings where route_id = r.id) as rating, (select count(*) from climbs where  is_attempt = 0 and route_id = r.id and created_by = ' . $user_id .' group by route_id) climbed_times, (select count(*) from climbs where is_attempt = 1 and route_id = r.id and created_by = ' . $user_id .' group by route_id) climbed_attempts');
        }
        else
        {
            $this->db->select('r.*, , c.name as color, g.name as grade, u.id user_id, u.name as setter, u.id as setter_id, (select count(*) from climbs where route_id = r.id) as climbs, (select avg(value) from route_ratings where route_id = r.id) as rating, CONCAT("0") climbed_times, CONCAT("0") climbed_attempts');   
        }
        $this->db->from('routes r');      
        $this->db->join('colors c','c.id = r.color_id');
        $this->db->join('grades g','g.id = r.grade_id');       
        $this->db->join('users u','u.id = r.created_by');  
        $this->db->where('r.id',$id);

        $query = $this->db->get();

        return $query->row(0,'Route_model');    
        
    }

    function get_attempts($route_id, $user_id = null)
    {

        $this->db->from('climbs c');      

        $this->db->where('c.route_id',$route_id);
        $this->db->where('c.created_by',$user_id);
        $this->db->where('c.is_attempt',1);

        $query = $this->db->get();

        return $query->result();
        
    }

    function rate($user_id, $value)
    {
        $this->db->select('*');
        $this->db->from('route_ratings');        
        $this->db->where('user_id',$user_id);
        $this->db->where('route_id',$this->id);
        $query = $this->db->get();

        if ($query->num_rows() == 0)
        {
            $data = array(
                'route_id' => $this->id,
                'user_id' => $user_id,
                'value' => $value 
            );

            $this->db->insert('route_ratings', $data);                  
        }
        else
        {
            $data = array(
                'value' => $value 
            );

            $this->db->where('route_id', $this->id);
            $this->db->where('user_id', $user_id);
            $this->db->update('route_ratings', $data);   
        }

    }

    function archive_route($route_id)
    {               
        $data = array(
            'is_archived' => 1            
        );

        $this->db->where('id', $route_id);
        $this->db->update('routes', $data);      
    }

    function delete()
    {               
        $this->db->where('id', $this->id);
        $this->db->delete('routes');       
    }

    function get_stats_by_application($application_id)
    {
        $this->db->select('sum(1) as sumatory, m.name, mxi.value ');
        $this->db->from('metadata m');
        $this->db->join('metadata_x_input mxi','m.id = mxi.metadata_id');
        $this->db->join('input i','i.id = mxi.input_id');
        $this->db->where('i.application_id', $application_id);        
        $this->db->group_by('mxi.value');

        $query = $this->db->get();

        return $query->result();
    }

    function get_climb_types()
    {
        $this->db->select('*');
        $this->db->from('climb_types');
        
        $query = $this->db->get();

        return $query->result();
    }
}