<?php
class Connector_model extends CI_Model {
    var $id   		= '';
   
    function __construct()
    {        
        parent::__construct();        
    }

    function get($id)
    {
        $this->db->select('*');
        $this->db->from('connectors');    
        $this->db->where('id', $id);    
        $query = $this->db->get();
        return $query->row(0,'Connector_model');  
    }

    function get_all()
    {
        $this->db->select('*');
        $this->db->from('connectors');        
        $query = $this->db->get();
        return $query->result();
    }

    function get_by_application($application_id)
    {
        $this->db->select('*');
        $this->db->from('connectors_x_application cxa');  
        $this->db->join('connectors c','c.id = cxa.connector_id');
        $this->db->where('cxa.application_id', $application_id);      
        $query = $this->db->get();
        return $query->result();
    }

    function redirect($connector, $input)
    {
        //TODO: improve Proof of concept now
        if ($connector->name == "Github")
        {
            $url = 'https://api.github.com/repos/'.$connector->destination.'/issues?access_token=' . $connector->token;
            
            $ch = curl_init($url);
             
            curl_setopt($ch, CURLOPT_POST, 1);
            
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(
                array(
                    'title' => $input->comment,
                    'body' =>  $input->comment . "\r\n ![Screenshot](". $input->image .") \r\n More details at " . site_url() . "input/view/" . $input->id,
                    'labels' => array('Klimbz')
                )
            ));
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'User-Agent: Klimbz',
                'Content-Type: application/json'                
            ));
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
             
            $response = curl_exec($ch);

            curl_getinfo($ch);

            curl_close($ch); 
            
            print_r($response);

        }
        else if ($connector->name == "Pivotal Tracker")
        {
            $url = 'https://www.pivotaltracker.com/services/v5/projects/'.$connector->destination.'/stories';
            
            $ch = curl_init($url);
             
            curl_setopt($ch, CURLOPT_POST, 1);
            
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(
                array(
                    'name' => $input->comment,
                    'description' =>  htmlentities($input->comment . "\r\n ". $input->image ." \r\n More details at " . site_url() . "input/view/" . $input->id )                   
                )
            ));
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(             
                'Content-Type: application/json',
                'X-TrackerToken: ' . $connector->token
            ));
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
             
            $response = curl_exec($ch);

            curl_getinfo($ch);

            curl_close($ch); 
            
            print_r($response);
            
        }

    }
}