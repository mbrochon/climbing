<?php
class Color_model extends CI_Model {

    var $id   		= '';
    var $name 		= '';    
    
    function __construct()
    {        
        parent::__construct();        
    }

    function create()
    {                       
        $this->db->insert('colors', $this);      
        $this->id = $this->db->insert_id();             
        return $this;      
    }

    function get()
    {
        $this->db->select('*');
        $this->db->from('colors');        
        $this->db->where('name',$this->name);
        $query = $this->db->get();

        if ($query->num_rows() == 0)
        {
            return $this->create();            
        }
        else
        {
            return $query->row(0,'Color_model');    
            
        }

    }

    function get_all()
    {

        $this->db->select('g.*');
        $this->db->from('colors g');      
        $this->db->order_by('g.name','asc'); 

        $query = $this->db->get();

        return $query->result();  
    }
	
}