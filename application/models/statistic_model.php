<?php
class Statistic_model extends CI_Model {

    
    function __construct()
    {        
        parent::__construct();        
    }

    function grades_stats($user_id, $gym_id = null)
    {
       
        $query = $this->db->query('SELECT
            count(1) as quantity, c.*, g.name
        FROM
            climbs c
            JOIN
            routes r on r.id = c.route_id
            JOIN
            grades g on g.id = r.grade_id
            JOIN 
            walls w on w.id = r.wall_id
        WHERE
            c.created_by = ' . $user_id . (($gym_id==null)?'':'  and w.gym_id = ' .$gym_id). '
        GROUP BY
            g.id 
        ORDER BY
            g.value asc
            ');

        $results =  $query->result();

        return $results;

    }

    function grades_stats_gym($gym_id = null)
    {
       
        $query = $this->db->query('SELECT
            count(1) as quantity, r.*, g.name
        FROM            
            routes r 
            JOIN
            grades g on g.id = r.grade_id
            JOIN 
            walls w on w.id = r.wall_id
        WHERE
            w.gym_id = ' .$gym_id. ' and r.is_archived != 1
        GROUP BY
            g.id 
        ORDER BY
            g.value asc
            ');

        $results =  $query->result();

        return $results;

    }

    function grades_stats_wall($wall_id = null)
    {
       
        $query = $this->db->query('SELECT
            count(1) as quantity, r.*, g.name
        FROM            
            routes r 
            JOIN
            grades g on g.id = r.grade_id
            JOIN 
            walls w on w.id = r.wall_id
        WHERE
            w.id = ' .$wall_id. ' and r.is_archived != 1
        GROUP BY
            g.id 
        ORDER BY
            g.value asc
            ');

        $results =  $query->result();

        return $results;

    }

    function least_climbed_routes($gym_id = null, $top = 10)
    {
       
        $query = $this->db->query('SELECT
            count(*) as quantity, c.*, g.name grade_name, r.id, r.name route_name, w.name wall_name, w.id, co.name color_name
        FROM            
            routes r
            LEFT OUTER JOIN
            climbs c            
            on r.id = c.route_id
            JOIN
            grades g on g.id = r.grade_id
            JOIN
            colors co on co.id = r.color_id
            JOIN 
            walls w on w.id = r.wall_id
        WHERE
            1=1 ' . (($gym_id==null)?'':'  and w.gym_id = ' .$gym_id). '

        GROUP BY
            r.id
        ORDER BY
            quantity asc
        LIMIT ' . $top);

        $results =  $query->result();

        return $results;

    }

    function top_climbed_routes($gym_id = null, $top = 10)
    {
       
        $query = $this->db->query('SELECT
            count(*) as quantity, g.name grade_name, r.id, r.name route_name, w.name wall_name, co.name color_name
        FROM
            climbs c
            JOIN
            routes r on r.id = c.route_id
            JOIN
            grades g on g.id = r.grade_id
            JOIN
            colors co on co.id = r.color_id
            JOIN 
            walls w on w.id = r.wall_id
        WHERE
            r.is_archived != 1 ' . (($gym_id==null)?'':'  and w.gym_id = ' .$gym_id). '

        GROUP BY
            r.id
        ORDER BY
            quantity desc
        LIMIT ' . $top);

        $results =  $query->result();

        return $results;

    }

    function top_climbed_walls($gym_id = null, $top = 10)
    {
       
        $query = $this->db->query('SELECT
           count(*) as quantity, g.name grade_name, r.id, r.name route_name, w.name wall_name, co.name color_name
        FROM
            climbs c
            JOIN
            routes r on r.id = c.route_id
            JOIN
            grades g on g.id = r.grade_id
            JOIN
            colors co on co.id = r.color_id
            JOIN 
            walls w on w.id = r.wall_id
        WHERE
            r.is_archived != 1 ' . (($gym_id==null)?'':'  and w.gym_id = ' .$gym_id). '

        GROUP BY
            w.id
        ORDER BY
            quantity desc
        LIMIT ' . $top);

        $results =  $query->result();

        return $results;

    }

    function last_climbs($user_id, $gym_id = null)
    {

        $query = $this->db->query('SELECT
            c.*, g.name grade_name, r.name route_name, gy.name gym_name, cl.name as color_name, g.value as grade_value, gy.id gym_id
        FROM
            climbs c
            JOIN
            routes r on r.id = c.route_id
            JOIN
            grades g on g.id = r.grade_id
            JOIN
            colors cl on cl.id = r.color_id
            JOIN 
            walls w on w.id = r.wall_id
            JOIN 
            gyms gy on gy.id = w.gym_id
        WHERE
            c.created_by = ' . $user_id . (($gym_id==null)?'':'  and w.gym_id = ' .$gym_id). ' ORDER BY c.created_on DESC LIMIT 10');


        $results =  $query->result();

        return $results;

    }

    function user_climbs($user_id, $gym_id = null)
    {

        $query = $this->db->query('SELECT
            count(*) quantity, date(c.created_on) name, g.name grade_name, r.name route_name, gy.name gym_name, cl.name as color_name, g.value as grade_value
        FROM
            climbs c
            JOIN
            routes r on r.id = c.route_id
            JOIN
            grades g on g.id = r.grade_id
            JOIN
            colors cl on cl.id = r.color_id
            JOIN 
            walls w on w.id = r.wall_id
            JOIN 
            gyms gy on gy.id = w.gym_id
        WHERE
            c.created_by = ' . $user_id . (($gym_id==null)?'':'  and w.gym_id = ' .$gym_id). ' GROUP BY date(c.created_on) ORDER BY date(c.created_on) asc');

        $results =  $query->result();

        return $results;

    }

    function user_climbs_by_date($user_id, $date)
    {
        
        $query = $this->db->query('SELECT
            date(c.created_on) as date_climb, w.name wall_name, g.name grade_name, r.name route_name, gy.name gym_name, cl.name as color_name, g.value as grade_value
        FROM
            climbs c
            JOIN
            routes r on r.id = c.route_id
            JOIN
            grades g on g.id = r.grade_id
            JOIN
            colors cl on cl.id = r.color_id
            JOIN 
            walls w on w.id = r.wall_id
            JOIN 
            gyms gy on gy.id = w.gym_id
        WHERE
            c.created_by = ' . $user_id . ' and date(c.created_on) = "' . $date. '"');
        
        $results =  $query->result();

        return $results;

    }


    function get_all()
    {

        $this->db->select('g.*');
        $this->db->from('gyms g');      

        $query = $this->db->get();

        return $query->result();  
    }
	
}