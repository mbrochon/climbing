<?php
class Comment_model extends CI_Model {

    var $id   		        = '';
    var $conversation_id 	= '';    
    var $text	            = '';    
    var $parent_id   	    = '';  
    var $is_hidden  	    = '';  

    function __construct()
    {        
        parent::__construct();        
    }

    function create()
    {                       
        $this->db->insert('comments', $this);      
        $this->id = $this->db->insert_id();             
        return $this;      
    }

    function get_by_conversation($conversation_id)
    {

        $this->db->select('c.*, u.email email, u.id user_id, u.name username, u.profile_picture_url');
        $this->db->from('comments c');        
        $this->db->join('users u','u.id = c.created_by');               
        $this->db->where('c.conversation_id', $conversation_id);
        $this->db->order_by("created_on", "desc"); 
        
        $query = $this->db->get();

        return $query->result();    
    }

     function get_by_id_and_gym_id($id,$gym_id)
    {
        $conversation_type_id = CONVERSATION_TYPE_ROUTE;
        $this->db->select('c.*');
        $this->db->from('comments c');               
        $this->db->join('conversations o','o.id = c.conversation_id');
        $this->db->join('routes r','r.id = o.parent_id');
        $this->db->join('walls w','r.wall_id = w.id');
        $this->db->where('o.conversation_type_id', $conversation_type_id);
        $this->db->where('c.id', $id);
        $this->db->where('w.gym_id', $gym_id);
        $query = $this->db->get();
        return $query->row(0,'Comment_model');     
    }
    
    function update_comment($comment)
    {
        $this->db->where('id', $comment->id);
        $this->db->update('comments', $comment);
    }
    
    function delete_comment($comment)
    {
        $this->db->where('id', $comment->id);
        $this->db->delete('comments', $comment);
    }
}