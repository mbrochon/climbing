<?php
class Gym_model extends CI_Model {

    var $id   		= '';
    var $name 		= '';    
    var $address	    = '';    
    var $latitude   	= '';    
    var $longitude       = '';   
    var $moderate_comments = '';

    function __construct()
    {        
        parent::__construct();        
    }

    function get($id)
    {

        $this->db->select('g.*');
        $this->db->from('gyms g');               
        $this->db->where('g.id', $id);
                
        $query = $this->db->get();

        return $query->row(0,'Gym_model');     
    }

    function managed_by_user($user_id)
    {

        $this->db->select('g.*');
        $this->db->from('gyms g');    
        $this->db->join('gym_setters gs','gs.gym_id = g.id');           
        $this->db->where('gs.user_id', $user_id);
                
        $query = $this->db->get();
        
        return $query->result();  

    }

    function get_all()
    {

        $this->db->select('g.*');
        $this->db->from('gyms g');      

        $query = $this->db->get();

        return $query->result();  
    }

    function get_links()
    {

        $this->db->select('g.*');
        $this->db->from('gym_links g');      
        $this->db->where('g.gym_id', $this->id);
        
        $query = $this->db->get();

        return $query->result();  
    }

    function add_setter($user_id)
    {
        $data = array(
        'gym_id' => $this->id ,
        'user_id' => $user_id
        );
        $this->db->insert('gym_setters', $data); 
    }
	
    function moderate_comments($gym)
    {
        $this->db->where('id', $gym->id);
        $this->db->update('gyms', $gym);
    }
    
}