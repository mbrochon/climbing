<?php
class Grade_model extends CI_Model {

    var $id   		= '';
    var $name 		= '';    
    
    function __construct()
    {        
        parent::__construct();        
    }

    function get_all()
    {

        $this->db->select('g.*');
        $this->db->from('grades g'); 
        $this->db->order_by('g.value','asc'); 

        $query = $this->db->get();

        return $query->result();  
    }
	
}