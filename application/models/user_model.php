<?php
class User_model extends CI_Model {

    var $id   		= '';
    var $name 		= '';    
    var $email	    = '';    
    var $password	= '';
    
    //1: On hold  --- 2: Active
    var $user_status_id = 2;
	
    function __construct()
    {        
        parent::__construct();        
    }
    
	function get_by_email($email = null)
	{
		$this->db->select('*');
		$this->db->from('users');
        if(isset($email)){
            $this->db->where('users.email', $email);
        }
       
		$query = $this->db->get();
    	$users = $query->result();
    	
    	if (count($users) > 0)
    	{        
            return $query->row(0,'User_model');                    
    	}
    	else
    	{
    		return false;
    	}
	}

    function get_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('users');
        
        $this->db->where('users.id', $id);
        
        $query = $this->db->get();
        $users = $query->result();
        
        if (count($users) > 0)
        {        
            return $query->row(0,'User_model');                    
        }
        else
        {
            return false;
        }
    }
	
	function create()
    {        		
		
		//password encrypt
		//--your unencrypted password goes here-->
		$password = $this->password;
		$salt = '';
		//generate salt----------------------------------->
		   for ($i=0; $i<=32; $i++) {
			  $d=rand(1,30)%2;
			  $salt .= $d ? chr(rand(65,90)) : chr(rand(48,57));
		   }
		//end generate salt--------------------------------|
		
		//hash password with salt-->
		$hashed = md5($password . $salt);			
		
		$encrypted = $hashed . ':' . $salt;

		$this->password = $encrypted;

		$this->db->insert('users', $this);      

    	$this->id = $this->db->insert_id(); 
    		

    	return $this;      
    }
    
    function link_service($user_id,$service_username, $service_id, $access_token,$is_private)
    {
        $this->db->select('*');
        $this->db->from('services_x_user');
        $this->db->where('user_id',$user_id); 
        $this->db->where('service_id',$service_id);
        $query = $this->db->get();

        if ($query->num_rows() == 0)
        {
            $service = new stdClass();
            $service->user_id = $user_id;
            $service->service_id = $service_id;
            $service->access_token = $access_token;
            $service->username = $service_username;
            $service->is_private = $is_private;
            $service->updated_on = date('Y-m-d',strtotime('-1 year'));
            $this->db->insert('services_x_user', $service);    
        }
        else
        {
            $this->db->where('user_id',$user_id); 
            $this->db->where('service_id',$service_id);
            $this->db->set('access_token', $access_token);
            $this->db->update('services_x_user'); 
        }
    }

    function unlink_service($user_id, $service_id)
    {
        $this->db->where('service_id', $service_id);
        $this->db->where('user_id', $user_id);
        $this->db->delete('services_x_user');
    } 

    function toggle_follow($target_user_id,$user_id)
    {

        $this->db->select('*');
        $this->db->from('follows');
        $this->db->where('user_id',$user_id); 
        $this->db->where('target_user_id',$target_user_id);
        $query = $this->db->get();
        $users = $query->result();
        
        if (count($users) > 0)
        {
            //already following needs to unfollow
            $this->db->where('user_id',$user_id); 
            $this->db->where('target_user_id',$target_user_id);
            $this->db->delete('follows');

            return false;
        }
        else
        {
            //not following and generates de notification in activity
            $user = $this->get($user_id);
            $user_taget = $this->get($target_user_id);

            $activity = new Activity_model();
            $activity->target_user_id = $target_user_id;
            $activity->verb = "follow";
            $activity->notification = 1;
            
            $activity->actor = $user;
            $activity->created_by = $user_id;
            $activity->create();


            $follow_user = new stdClass();
            $follow_user->user_id = $user_id;
            $follow_user->target_user_id = $target_user_id;
            $this->db->insert('follows', $follow_user);

             //send email to beta user
            $message_data = array(
                "page_title" => "Kloc",
                "title" => "You have a new follower",
                "description" => $user->full_name . " started following you on Kloc.me",
                "link" => site_url() . "site/login/",
                "link_text" => "Login Now"
            );
            
            $message = $this->load->view('/mails/app_communications',$message_data,true);  
            $subject = $user->full_name . " is following you on Kloc.me";

            $this->utils->send_mail($user_taget->email, $subject, $message);

            return true;
        }
        
    }

    function validate($email, $password)
    {
    	$this->db->select('*');
		$this->db->from('users');
		$this->db->where('users.email', $email); 		
		
		$query = $this->db->get();
		
    	$users = $query->result();

    	if (count($users) > 0)
    	{
    		$user = $users[0];

            if (count(explode(":", $user->password)) > 1) {
                list($md5pass, $saltpass) = explode(":", $user->password);
            
                if ((md5($password.$saltpass))==$md5pass) 
                {               
                    return $user;               
                } 
                else 
                {
                    return null;
                }   
            }
            else
            {
                return null;
            }
    				  
    	}
		else 
		{
			return null;
		}
    }
    
    function get($user)
    {
    	$this->db->select('*');
		$this->db->from('users');
		$this->db->where('users.id', $user); 		
        $this->db->or_where('users.email', $user); 
		
		$query = $this->db->get();

    	return $query->row(0,'User_model');    	
    }

    function get_all()
    {

        $this->db->select('g.*');
        $this->db->from('users g');      

        $query = $this->db->get();

        return $query->result();  
    }

    function update()
    {                      
        unset($this->password);            
        $this->db->where('id', $this->id);
        $this->db->update('users', $this);             
    }

    function update_password()
    {                              
        $this->db->where('id', $this->id);
        $this->db->update('users', $this);             
    }

    function add_role($role){

        $this->db->select('*');
        $this->db->from('roles_x_user');
        $this->db->where('roles_x_user.user_id', $this->id); 
        $this->db->where('roles_x_user.role_id', $role->id); 
        $query = $this->db->get();
        $exist = $query->num_rows() > 0;

        if (!$exist)
        {
            $data = array(
           'role_id' => $role->id ,
           'user_id' => $this->id
            );
            $this->db->insert('roles_x_user', $data); 
        }
    }

    function get_all_setters($gym_id)
    {

        $this->db->select('u.*');
        $this->db->from('users u');      
        $this->db->join('gym_setters gs','gs.user_id = u.id');        
        $this->db->where('gs.gym_id', $gym_id); 
        $this->db->order_by('u.name','asc'); 

        $query = $this->db->get();

        return $query->result();  
    }

    function get_setter()
    {

        $this->db->select('u.*');
        $this->db->from('users u');      
        $this->db->join('gym_setters gs','gs.user_id = u.id');        
        $this->db->where('gs.gym_id', $this->gym_id); 
        $this->db->where('u.name', $this->name); 
        
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->row(0,'User_model');
        }
        else
        {

            $gym_id = $this->gym_id;
            unset($this->gym_id);

            $this->db->insert('users', $this);      
            $this->id = $this->db->insert_id(); 
                
            $data = array(
                'gym_id' => $gym_id,
                'user_id' => $this->id
            );

            $this->db->insert('gym_setters', $data); 

            return $this;
        }
        
    }

    function is_setter_at_gym($gym_id){

        $this->db->select('*');
        $this->db->from('gym_setters');
        $this->db->where('user_id', $this->id); 
        $this->db->where('gym_id', $gym_id); 
        $query = $this->db->get();
        
        return ($query->num_rows() > 0);
    }

    function add_route($data){

        $data = array(
           'route_id' => $data['route_id'],
           'created_on' => $data['climbed_on'],
           'climb_type_id' => $data['climb_type'],
           'is_repeat' => $data['repeat'],
           'created_by' => $this->id
        );

        $this->db->insert('climbs', $data); 
        
    }

    function add_route_attempt($route_id, $notes){

        $data = array(
           'route_id' => $route_id,
           'created_by' => $this->id,
           'is_attempt' => 1,
           'notes' => $notes
        );

        $this->db->insert('climbs', $data); 
        
    }

}