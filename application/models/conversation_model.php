<?php
class Conversation_model extends CI_Model {

    var $id   		        = '';
    var $conversation_type_id 	= '';        
    var $parent_id   	    = '';    

    function __construct()
    {        
        parent::__construct();        
    }

    function create()
    {                       
        $this->db->insert('conversations', $this);      
        $this->id = $this->db->insert_id();             
        return $this;      
    }

    function get_by_type_and_parent()
    {

        $this->db->select('*');
        $this->db->from('conversations');        
        $this->db->where('conversation_type_id',$this->conversation_type_id);
        $this->db->where('parent_id',$this->parent_id);
        $query = $this->db->get();

        if ($query->num_rows() == 0)
        {
            return $this->create();            
        }
        else
        {
            return $query->row(0,'Conversation_model');    
            
        }
    }

}