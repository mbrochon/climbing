<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 


class Utils {

  function get_gravatar( $email, $s = 80, $d = 'mm', $r = 'g', $img = false, $atts = array() ) {
    $url = 'http://www.gravatar.com/avatar/';
    $url .= md5( strtolower( trim( $email ) ) );
    $url .= "?s=$s&d=$d&r=$r";
    if ( $img ) {
        $url = '<img src="' . $url . '"';
        foreach ( $atts as $key => $val )
            $url .= ' ' . $key . '="' . $val . '"';
        $url .= ' />';
    }
    return $url;
}


  function get_gravatar_user($user)
  {
    $CI =& get_instance();    
    //print_r($user);
    
    if ($user->profile_picture_url == "") {
      return $CI->utils->get_gravatar($user->email);
    }
    else
    { 
      return $user->profile_picture_url;
    }    
  }
  
	function get_gravatar_logged_user()
  {
    $CI =& get_instance();
    $user = $CI->utils->get_logged_user();

    if ($user->profile_picture_url == "") {
      return $CI->utils->get_gravatar_user($user);
    }
    else
    { 
      return $user->profile_picture_url;
    }

    
  }


  function get_app_store_details($bundle_id)
  {
    $json = file_get_contents('http://itunes.apple.com/lookup?bundleId='.$bundle_id);
    return json_decode($json);
  }
  

  

  	function send_mail($email, $subject, $message, $bcc =null){
      
  		$CI =& get_instance();
  		$CI->load->library('mailer');		
		  return $CI->mailer->send_mail($email,$subject,$message,$bcc);
  	}

  	function generate_salt()
  	{
  		$salt = '';
		//generate salt----------------------------------->
		   for ($i=0; $i<=32; $i++) {
			  $d=rand(1,30)%2;
			  $salt .= $d ? chr(rand(65,90)) : chr(rand(48,57));
		   }
		//end generate salt--------------------------------|

		return $salt;
  	}

  	function log($desc, $obj)
  	{
  		if (LOGGING == 1) {
  			echo "<h2>".$desc."</h2>";
  			print_r($obj);
  			echo "<br/>";
  		}
  	}

  	function substring($string, $chars, $ellipsis = true)
  	{
  		$response = $string;  		
  		if (strlen($string) > $chars) {
  			//we have to cut cut!
  			if (strpos($string," ") > 0 && strpos($string," ", $chars - 5)) {
  				$response = substr($string, 0, strpos($string," ", $chars - 5));
  			}
  			else
  			{
  				$response = substr($string, 0, $chars);
  			}
  		}

  		if ($ellipsis && strlen($string) > $chars) {
  			return $response . "…";
  		}
  		else
  		{
  			return $response;
  		}
  		
  	}

    
    function time_elapsed_string($ptime)
    {

        $etime = time("U") - $ptime;

        if ($etime < 1)
        {
            return '0 seconds';
        }

        $a = array( 12 * 30 * 24 * 60 * 60  =>  'year',
                    30 * 24 * 60 * 60       =>  'month',
                    24 * 60 * 60            =>  'day',
                    60 * 60                 =>  'hour',
                    60                      =>  'minute',
                    1                       =>  'second'
                    );

        foreach ($a as $secs => $str)
        {
            $d = $etime / $secs;
            if ($d >= 1)
            {
                $r = round($d);
                return $r . ' ' . $str . ($r > 1 ? 's' : '') . ' ago';
            }
        }
    }

    function interval_to_hours($interval){
        return ($interval->y * 365 * 24) + 
               ($interval->m * 30 * 24) + 
               ($interval->d * 24) + 
               ($interval->h);
    }

    function get_string_between($string, $start, $end){
      $string = " ".$string;
      $ini = strpos($string,$start);
      if ($ini == 0) { return $string; }
      $ini += strlen($start);
      $len = strpos($string,$end,$ini) - $ini;
      return substr($string,$ini,$len);
    }

    function activity_description($activity)
    {
      switch ($activity->verb) {
        case 'follow':
          return "is following you.";
          break;
        case 'work':
          $object = json_decode($activity->object);
          if ($object->type == "framework") {
            return "is developing with <strong>" . $object->name . "</strong> framework";
          }
          else if ($object->type == "technology") {
            return "is gaining experience in <strong>" . $object->name . "</strong> technologies";
          }
          break;
        case 'share':
          $object = json_decode($activity->object);
          return "said <i><strong>" . $object->name . "</strong></i>";
          break;
        case 'checkin':
          $object = json_decode($activity->object);
          return "<i><strong>" . $object->name . "</strong></i>";
          break;
        case 'suggests':
          $object = json_decode($activity->object);
          return "suggests you reading <i><a href='" . $object->url . "'><strong>" . $object->name . "</strong></a></i><small><br/>via ".$object->via."</small>";
          break;
        case 'write':
          $objects = json_decode($activity->object);
          return "wrote <strong>" . $objects[0]->name ."</strong> and deleted <strong>" . $objects[1]->name . "</strong> lines of code.";
          break;
        default:
          return $activity->verb;
          break;
      }
    }

    function notification_url($url)
    {
        if (stripos($url, "http") === FALSE) {
            return site_url() . $url;
        }
        else
        {
            return $url;
        }

    }

    function leaderboard_position($pos)
    {
      if ($pos == 1) {
          return $pos."st.";
      }
      else if ($pos == 2) {
          return $pos."nd.";
      }
      else if ($pos == 3) {
          return $pos."rd.";
      }
      else if ($pos >= 4) {
          return $pos."th.";
      }
    }

    function url_is_active($url)
    { 

      $CI =& get_instance();

      
        if ($url == $CI->router->uri->uri_string) {
          return "active";
        }
        else
        {
          return "";
        }
    }

    function random_username()
    { 

      $CI =& get_instance();

      $CI->load->model('User_model');
      
      $user = $CI->User_model->get_random_username();

      if (count($user) > 0) {
        return $user->username;
      }
      else
      {
        return "";
      }
      
    }

    function level_indicator($max_quantity, $current_quantity)
    {
      return "level_".round(($current_quantity * 5) / $max_quantity);
    }

    function impact_level($max_quantity, $current_quantity)
    {
      return round(($current_quantity * 100) / $max_quantity) * 1.1;
    }

    function impact_level_p($max_quantity, $current_quantity)
    {
      return round(($current_quantity * 100) / $max_quantity);
    }

    function replace_tags($user, $text)
    {
        $text = str_ireplace("[NAME]", $user->full_name, $text);
        return $text;
    }

    function mixcolors($color1, $color2)
    {

      $c1_p1 = hexdec(substr($color1, 0, 2));
      $c1_p2 = hexdec(substr($color1, 2, 2));
      $c1_p3 = hexdec(substr($color1, 4, 2));

      $c2_p1 = hexdec(substr($color2, 0, 2));
      $c2_p2 = hexdec(substr($color2, 2, 2));
      $c2_p3 = hexdec(substr($color2, 4, 2));

      $m_p1 = sprintf('%02x', (round(($c1_p1 + $c2_p1)/2)));
      $m_p2 = sprintf('%02x', (round(($c1_p2 + $c2_p2)/2)));
      $m_p3 = sprintf('%02x', (round(($c1_p3 + $c2_p3)/2)));

     return    $m_p1 . $m_p2 . $m_p3;
    }

    function get_random_color()
    {
      $input = array("#282E3C", "#00D9A3", "#323A4B", "#57C505", "#FF4D4D","#009DE9","#00D9A3","#FFA64D");
      $rand_keys = array_rand($input, 1);
      return $input[$rand_keys];
      
    }

    function has_activity($user)
    { 
        if (isset($user->lines_this_week_for_user->additions) || isset($user->lines_this_month_for_user->additions)) {
          
        }
        else
        {
          return false;
        }

        if ($user->lines_this_week_for_user->additions=="" && $user->lines_this_week_for_user->deletions=="" && 
            $user->lines_this_month_for_user->additions=="" && $user->lines_this_month_for_user->deletions=="")
        {
          return false;
        } 
        else
        {
          return true;
        }
      
    }

    function has_skills($user)
    { 
        $sum = (count($user->skills) > 0);

        return $sum;
        
    }

    function get_session_ip()
    {
      $ip = null;

      if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
          $ip = $_SERVER['HTTP_CLIENT_IP'];
      } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
          $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
      } else {
          $ip = $_SERVER['REMOTE_ADDR'];
      }

      return $ip;      
    }


    function send_welcome_email($user)
    {
      $CI =& get_instance();
      
      //send email to beta user
      $message_data = array(
        "page_title" => "Kloc",
        "title" => "Welcome to Kloc",
        "description" => "You can login using your email and password",
        "link" => site_url() . "site/login/",
        "link_text" => "Login Now"
      );
      
      $message = $CI->load->view('/mails/app_communications',$message_data,true);  
      $subject = "Welcome to Kloc!";

      $this->send_mail($user->email, $subject, $message);

    }

    function get_logged_user()
    {
      
      $CI =& get_instance();
      
      $user = $CI->session->userdata('user');
      
      if (!isset($user) || $user == null || $user == "") {        

        if (isset($_GET['token'])) {                    
          
          $CI->load->model('User_model');      
          $user = $CI->User_model->get_by_token($_GET['token']);

        }
        else
        {
          //no session, no token.. nothing! anonymous user.
          $user = null;
        }
      }
      else
      {        
        //$CI->load->model('Application_model');    
        $user = json_decode($user);
        //$user->applications = $CI->Application_model->get_for_user($user);*/
      }

      return $user;

    }

    function formatted_date($date)
    {
      $createDate = new DateTime($date);
      $strip = $createDate->format('d-m-Y');
      return $strip;
    }
}

/* End of file Utils.php */