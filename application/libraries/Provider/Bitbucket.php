<?php

class OAuth_Provider_Bitbucket extends OAuth_Provider {

	public $name = 'bitbucket';

	public function url_request_token()
	{
		return 'https://bitbucket.org/!api/1.0/oauth/request_token';
	}

	public function url_authorize()
	{
		return 'https://bitbucket.org/!api/1.0/oauth/authenticate';
	}

	public function url_access_token()
	{
		return 'https://bitbucket.org/api/1.0/oauth/access_token';
	}
	
	public function get_user_info(OAuth_Consumer $consumer, OAuth_Token $token)
	{
		// Create a new GET request with the required parameters
		$request = OAuth_Request::forge('resource', 'GET', 'https://bitbucket.org/api/1.0/user/', array(
			'oauth_consumer_key' => $consumer->key,
			'oauth_token' => $token->access_token,
		));

		// Sign the request using the consumer and token
		$request->sign($this->signature, $consumer, $token);
		
		$user = json_decode($request->execute());
		
		// Create a response from the request
		return $user;
		
	}
	
	public function get_repositories(OAuth_Consumer $consumer, OAuth_Token $token)
	{
		// Create a new GET request with the required parameters
		$request = OAuth_Request::forge('resource', 'GET', 'https://bitbucket.org/api/1.0/user/repositories/', array(
			'oauth_consumer_key' => $consumer->key,
			'oauth_token' => $token->access_token,
		));

		// Sign the request using the consumer and token
		$request->sign($this->signature, $consumer, $token);

		$repositories = json_decode($request->execute());
		
		// Create a response from the request
		return $repositories;		
	}

	public function get_changesets(OAuth_Consumer $consumer, OAuth_Token $token, $repository_slug, $owner)
	{
		// Create a new GET request with the required parameters
		$request = OAuth_Request::forge('resource', 'GET', 'https://bitbucket.org/api/1.0/repositories/'.$owner.'/'.$repository_slug.'/changesets/', array(
			'oauth_consumer_key' => $consumer->key,
			'oauth_token' => $token->access_token,
		));

		// Sign the request using the consumer and token
		$request->sign($this->signature, $consumer, $token);

		$changesets = json_decode($request->execute());
		
		// Create a response from the request
		return $changesets;		
	}

	public function get_issues(OAuth_Consumer $consumer, OAuth_Token $token, $repository_slug, $owner, $responsible)
	{		
		// Create a new GET request with the required parameters
		$request = OAuth_Request::forge('resource', 'GET', 'https://bitbucket.org/api/1.0/repositories/'.$owner.'/'.$repository_slug.'/issues?responsible='.$responsible, array(
			'oauth_consumer_key' => $consumer->key,
			'oauth_token' => $token->access_token,
		));

		// Sign the request using the consumer and token
		$request->sign($this->signature, $consumer, $token);

		$changesets = json_decode($request->execute());
		
		// Create a response from the request
		return $changesets;		
	}

	public function get_individual_changeset(OAuth_Consumer $consumer, OAuth_Token $token, $repository_slug, $owner,$raw_node)
	{
		// Create a new GET request with the required parameters
		$request = OAuth_Request::forge('resource', 'GET', 'https://bitbucket.org/api/1.0/repositories/'.$owner.'/'.$repository_slug.'/changesets/'.$raw_node, array(
			'oauth_consumer_key' => $consumer->key,
			'oauth_token' => $token->access_token,
		));

		// Sign the request using the consumer and token
		$request->sign($this->signature, $consumer, $token);

		$changeset = json_decode($request->execute());
		
		// Create a response from the request
		return $changeset;		
	}
	
	public function get_changesets_details(OAuth_Consumer $consumer, OAuth_Token $token, $repository_slug, $owner, $raw_node)
	{
		// Create a new GET request with the required parameters
		$request = OAuth_Request::forge('resource', 'GET', 'https://bitbucket.org/api/1.0/repositories/'.$owner.'/'.$repository_slug.'/changesets/' . $raw_node . '/diffstat', array(
			'oauth_consumer_key' => $consumer->key,
			'oauth_token' => $token->access_token,
		));

		// Sign the request using the consumer and token
		$request->sign($this->signature, $consumer, $token);

		$changesets = json_decode($request->execute());
		
		// Create a response from the request
		return $changesets;		
	}

	public function get_events_by_user(OAuth_Consumer $consumer, OAuth_Token $token, $username)
	{
		// Create a new GET request with the required parameters
		$request = OAuth_Request::forge('resource', 'GET', 'https://bitbucket.org/api/1.0/users/'.$username.'/events?type=pushed&limit=50', array(
			'oauth_consumer_key' => $consumer->key,
			'oauth_token' => $token->access_token,
		));

		// Sign the request using the consumer and token
		$request->sign($this->signature, $consumer, $token);

		$changesets = json_decode($request->execute());
		
		// Create a response from the request
		return $changesets;		
	}

	public function process_commits(OAuth_Consumer $consumer, OAuth_Token $token, $commits, $repository, $user, $event)
	{
		/*print_r($event);
		print_r($user);
		print_r($repository);
*/
		$CI =& get_instance();
		$CI->load->model('Service_model');
		$CI->load->model('Extension_model');
		$CI->load->model('Repository_model');
		$CI->load->model('Language_model');
		$CI->load->model('Commit_model');
		$CI->load->model('Filecode_model');
		$CI->load->model('Push_model');
		$CI->load->model('User_model');
		$CI->load->model('Framework_model');
		$CI->load->model('Badge_model');
		$CI->load->model('Issue_model');

		//$language_db = $CI->Language_model->get_by_id($repository->language_id);

		$provider = $CI->oauth->provider('bitbucket');
		$bitbucket = $CI->Service_model->get_by_name(BITBUCKET_SERVICE_NAME);

		foreach ($commits as $bb_commit) {
			//Get push details and insert it in DB

			if (isset($bb_commit->hash)) {
				$hash = $bb_commit->hash;				
			}
			else if (isset($bb_commit->raw_node))
			{
				$hash = $bb_commit->raw_node;
			}	
			else
			{
				continue;
			}

			if (isset($event->event)) {
				$event_type = $event->event;		
				$created_on = $event->utc_created_on;		
			}
			else 
			{
				$event_type = "";
				$created_on = $bb_commit->utctimestamp;
			}

			if (isset($bb_commit->description)) {
				$description = $bb_commit->description;		
			}
			else 
			{
				$description = $event->message;
			}

			$changeset = $provider->get_individual_changeset($consumer, $token, $repository->repository_id, $repository->owner,$hash);
			
			$push = new Push_model();
			$push->push_id = $event_type.':'.base64_encode($created_on); // Bitbucket does not have a unique value to ref a push yet.
			$push->user_id = $user->id;
			$push->repo_id = $repository->id;
			$push->created_on = $created_on;
			$push = $push->create();
			//Insert the files info

			$commit = new Commit_model();
			$commit->push_id = $push->id;
			$commit->sha = $hash;
			$commit->service_id = $bitbucket->id;
			$commit->description = $description;
			$commit = $commit->create();
			$total_additions = 0;
			$total_deletions = 0;
			$changesetDetails = $provider->get_changesets_details($consumer, $token, $repository->repository_id, $repository->owner,$hash);

			$CI->utils->log('Changeset Repository',$repository);
			$CI->utils->log('Changeset',$changeset);	
			$CI->utils->log('Changeset Details',$changesetDetails);	

			if (!isset($changesetDetails)) {
				return;
			}

			foreach ($changesetDetails as $bb_file) {
				$filename = explode('.',$bb_file->file);
				//use the repo language as default
				/*$language = $language_db;
				if(count($filename)>1){
					$ext = $filename[count($filename)-1];
					$languages_db = $CI->Language_model->get_by_extension($ext);
					if(count($languages_db)==1){
						$language = $languages_db[0];
						$languages[] = $language;
					}
					$extension = $CI->Extension_model->get_by_name($ext);
				}*/
				//Add file data to DB
				$filecode = new Filecode_model();
				$filecode->commit_id = $commit->id;
				$filecode->filename = $bb_file->file;
				$filecode->additions = $bb_file->diffstat->added == ''? 0 : $bb_file->diffstat->added;
				$filecode->deletions = $bb_file->diffstat->removed == ''? 0 : $bb_file->diffstat->removed;
				$filecode->changes = $bb_file->diffstat->added + $bb_file->diffstat->removed;
				$filecode->raw_url = $bb_file->file;
				$filecode->language_id =  null; //isset($language->id) ? $language->id : null;
				$filecode->user_id = $user->id;
				$filecode->extension_id = null;//  $extension != '' ? $extension->id : NULL;
				$filecode->excepted = preg_match("/".REGEX_FILE_EXCEPTION."/",$bb_file->file);
				$filecode->create($bb_file->type);
				$total_additions += $bb_file->diffstat->added;
				$total_deletions += $bb_file->diffstat->removed;
			}
			//Add stats to commit
			$commit->update_stats($total_additions,$total_deletions);
		}
	}

	public function process_issues(OAuth_Consumer $consumer, OAuth_Token $token, $issues, $repository, $user)
	{
		$CI =& get_instance();
		$CI->load->model('Issue_model');

		if (!isset($issues->issues)) {
			return;
		}

		//Getting Issues
		foreach($issues->issues as $issue){				
			//hack Bitbucket ID
			$issue_id = $user->id . "090909" . $issue->{'local_id'};

			$issue_obj = $CI->Issue_model->get($issue_id);

			if (isset($issue_obj)) {

				if ($issue->status == "new") {
					
					$issue_obj->has_re_opens = 0;
					$issue_obj->is_open = 1;

				}
				else if ($issue->status == "open") 
				{
					if ($issue_obj->is_open == 0) {
						$issue_obj->has_re_opens = $issue_obj->has_re_opens + 1;
					}

					$issue_obj->is_open = 1;

				}
				else if ($issue->status == "resolved") 
				{
					$issue_obj->is_open = 0;
				}

				$issue_obj->update();			
			}
			else
			{
				//Get Repository							
				$issue_obj = new Issue_model();					
				$issue_obj->description = $issue->{'title'};
				$issue_obj->user_id = $user->id;
				$issue_obj->issue_id = $issue_id;
				$issue_obj->is_open = 1;
				$issue_obj->has_re_opens = 0;
				$issue_obj->created_on = $issue->{'utc_created_on'};
				$issue_obj->repository_id = $repository->id;

				$issue_obj->create();
			}
		}
	}

	public function process_profile($user){
		$CI =& get_instance();
		$CI->load->model('Service_model');
		$CI->load->model('Extension_model');
		$CI->load->model('Repository_model');
		$CI->load->model('Language_model');
		$CI->load->model('Commit_model');
		$CI->load->model('Filecode_model');
		$CI->load->model('Push_model');
		$CI->load->model('User_model');
		$CI->load->model('Framework_model');
		$CI->load->model('Badge_model');
		$CI->load->model('Issue_model');
		
		$provider = $CI->oauth->provider('bitbucket');

		$bitbucket = $CI->Service_model->get_by_name(BITBUCKET_SERVICE_NAME);

		$service = $CI->Service_model->get_token_by_user($user->id,$bitbucket->id);

		$service_updated_on = strtotime($service->updated_on);

		$response = new stdClass();
		if(isset($service->access_token)){
			$languages_by_ext = $CI->Extension_model->get_all();
			//Get BitBucket Access token for this user and process From DB
			$token = unserialize(base64_decode($service->access_token));

			$service_id = $service->service_id;
			//Set the service username for the user
			$service_username = $service->username;
			$languages = array();
			//Prepare consumer to request
			$consumer = $CI->oauth->consumer(array(
	            'key' => BITBUCKET_KEY,
	            'secret' => BITBUCKET_SECRET
	        ));
			// We got the token, let's get some user data
			
	        $user_info = $provider->get_user_info($consumer, $token);	        
	        
	        if(($user_info == null) || !isset($user_info->user->username)){

				//Delete this service from the services_x_user table
				$CI->User_model->unlink_service($user->id, $service_id);
				//Send Email to user notifiying that the Token is no longer valid and must login to refresh it
				$response->error = true;
				$response->code = ERROR_SERVICE_BAD_TOKEN;
				return $response;
			}
			
			//We need the user events
			$events_object = $provider->get_events_by_user($consumer, $token, $user_info->user->username);

			$CI->utils->log('Events',$events_object);	

			
			foreach ($events_object->events as $bb_event){
				$event_date = strtotime($bb_event->utc_created_on);
				if($event_date<$service_updated_on)
					continue;

				//Event type must be push and must contain a description, otherwise is not a commit
				if((strcasecmp($bb_event->event,"pushed")==0) && isset($bb_event->description) && isset($bb_event->repository)){ 
					$bb_repository = $bb_event->repository;
					$language_db = $CI->Language_model->get_by_name($bb_repository->language);
					$repository = new Repository_model();
					$repository->repository_id = $bb_repository->slug;
					$repository->language_id = isset($language_db->id) ? $language_db->id : null;
					$repository->owner = $bb_repository->owner;
					$repository->name = $bb_repository->name;
					$repository->service_id = $bitbucket->id;
					$repository->is_private = $bb_repository->is_private;
					$repository->description = $bb_repository->description;
					$repository->created_at = isset($bb_repository->utc_created_on) ? $bb_repository->utc_created_on : null;
					$repository->updated_at = isset($bb_repository->utc_updated_on) ? $bb_repository->utc_updated_on : null;
					$repository->pushed_at = null;

					if (isset($bb_repository->followers_count)) {
						$repository->watchers = $bb_repository->followers_count;
					}
					
					$repository->forks = $bb_repository->no_public_forks;

					if($repository->exists()){
						$repository = $repository->update();
					}else{
						$repository = $repository->create();
					}

					//$repository = $CI->Repository_model->get($bb_repository->slug,$bitbucket->id);
					//$repository = $repository->create();

					//Add the relation between the user and the repo
					$CI->Repository_model->add_by_user($user->id,$repository->id);

					//Get the issues if any
					/*$issues = $provider->get_issues($consumer, $token, $bb_repository->slug, $bb_repository->owner,$user_info->user->username);						

					$CI->utils->log('Issues',$issues);	

					$provider->process_issues($consumer, $token, $issues, $repository, $user);
					*/
					
					$CI->utils->log('Commits',$bb_event->description->commits);	

					$provider->process_commits($consumer, $token,$bb_event->description->commits, $repository, $user, $bb_event);

				}
			}
			
			//now analyze activity as owner of repos..
			/*$repositories = $CI->Repository_model->get_by_user($user->id);


			foreach ($repositories as $repository) {
				$changesets = $provider->get_changesets($consumer, $token, $repository->repository_id, $repository->owner);
				
				print_r($changesets);

				if (!isset($changesets)) {
					continue;
				}

				foreach ($changesets->changesets as $changeset) {

					//Verify if user exists in kloc... if not create one... and process... if exists do nothing because it will override
					$user_not_me = $CI->Service_model->get_by_username_and_service($changeset->author, BITBUCKET_SERVICE_ID);
					
					if (count($user_not_me) == 0) {
						//extra verification
						$user_not_me = $CI->User_model->exists(null, strtolower(trim($changeset->author)));

						if (!$user_not_me) {
							//This means it doesn't exist
							$user_not_me = new User_model();
							$user_not_me->full_name = $changeset->author;
					      	$user_not_me->username = strtolower(str_replace(' ', '', $changeset->author)) . rand(1, 20);				      	
					      	$user_not_me->email = $CI->utils->get_string_between($changeset->raw_author, "<", ">");				      						      	
				      		$user_not_me = $user_not_me->create(false);

				      		//Add the relation between the user and the repo
							$CI->Repository_model->add_by_user($user_not_me->id,$repository->id);

						}
						
				      	$commits = array();
				      	$commits[] = $changeset;

				      	$repository->name = $repository->repository_id;

				      	$provider->process_commits($consumer, $token,$commits, $repository, $user_not_me, $changeset);

				      	$user_not_me->process_skills();
					}

					//Get the issues if any
					//$issues = $provider->get_issues($consumer, $token, $bb_repository->slug, $bb_repository->owner,$user_info->user->username);						
					//$provider->process_issues($issues, $repository, $user);
				}
				
			}
			*/
			//Set updated date for this service. In this case Github
			$user->set_updated($service_id);
			//Build response
			$response->error = false;
		}else{
			$response->error = true;
			$response->code = ERROR_SERVICE_INEXISTENT;
		}
		return $response;
		
	}
		
} // End Provider_Dropbox