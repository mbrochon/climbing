<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 


class Custom_loader {

  	/*
	* Loads a view and the head for the page
	*/
	function load_app_view($title,$view,$view_data = null, $head_data = null) {
		$CI =& get_instance();
		if ($head_data == null) {
			$head_data =  array('title' => $title);
		}
	  	else
	  	{
	  		$head_data['title'] = $title;
	  	}

		$head = $CI->load->view('site/_header',array('head'=>$head_data),true);
		if($view_data==null){
			$view_data = array('head' => $head);
		}else{
			$view_data['head']=$head;
		}
		
		$CI->load->view($view,$view_data);  
  	}

	/*
	* Loads a view and the head for the widget
	*/
	function load_widget_view($title,$view,$view_data = null) {
		$CI =& get_instance();
	  	$head_data =  array('title' => $title);
		$head = $CI->load->view('head_widget',$head_data,true);
		if($view_data==null){
			$view_data = array('head' => $head);
		}else{
			$view_data['head']=$head;
		}
		
		$CI->load->view($view,$view_data);  
  	}


  	/*
	* Loads a view and the head for the page
	*/
	function load_site_view($title,$view,$view_data = null) {
		$CI =& get_instance();
	  	$head_data =  array('title' => $title);
		$head = $CI->load->view('head_site',$head_data,true);
		if($view_data==null){
			$view_data = array('head' => $head);
		}else{
			$view_data['head']=$head;
		}
		
		$CI->load->view($view,$view_data);  
  	}

  	/*
	* Loads a view and the head for the widget
	*/
	function load_team_view($title,$view,$view_data = null) {
		$CI =& get_instance();
	  	$head_data =  array('title' => $title);
		$head = $CI->load->view('team/head',$head_data,true);
		if($view_data==null){
			$view_data = array('head' => $head);
		}else{
			$view_data['head']=$head;
		}
		
		$CI->load->view($view,$view_data);  
  	}
}

/* End of file CustomLoader.php */