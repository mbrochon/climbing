<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 


class Github {

	function createCurl($url){
		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_USERAGENT, "Kloc"); 
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		return $curl;
	}

	function get_access_token($code){
		$code = "code=" . $code . "&client_id=".CLIENT_ID."&client_secret=".CLIENT_SECRET;			
		$url = 'https://github.com/login/oauth/access_token';
		$ch = $this->createCurl($url);

		curl_setopt($ch, CURLOPT_POST, true);		
		curl_setopt($ch, CURLOPT_POSTFIELDS, $code);
		 
		$response = curl_exec($ch);
		
		print_r(json_decode($response));
		
		curl_close($ch); 
		
		parse_str($response,$output);

		return $output['access_token'];
	}

	function get_user_info($access_token){
		$code = 'access_token='.$access_token.'&type=all';	
		$url = 'https://api.github.com/user?'.$code;
		$ch = $this->createCurl($url);
		$response = curl_exec($ch);
		$user = json_decode($response);
		curl_close($ch);
		return $user;
		
	}


	function get_feed($username, $token){
		$code = 'access_token='.$token;	
		$url = 'https://github.com/'.$username.'.private?'.$code;
		$ch = $this->createCurl($url);
		$response = curl_exec($ch);
		$feed = json_decode($response);
		curl_close($ch);
		return $feed;
		
	}

	

	function get_organizations($token){
		$code = 'access_token='.$token;	
		$url = 'https://api.github.com/user/orgs?'.$code;
		$ch = $this->createCurl($url);
		$response = curl_exec($ch);
		$orgs = json_decode($response);
		curl_close($ch);
		return $orgs;
		
	}

	function get_issues_assigned($token, $state){
		$code = 'access_token='.$token.'&state='.$state;	
		$url = 'https://api.github.com/issues?'.$code;
		$ch = $this->createCurl($url);
		$response = curl_exec($ch);
		$issues = json_decode($response);
		curl_close($ch);
		return $issues;
		
	}

	function get_repos_for_auth_user($token){
		$code = 'access_token='.$token;	
		$url = 'https://api.github.com/user/repos?'.$code;
		$ch = $this->createCurl($url);
		$response = curl_exec($ch);
		$repos = json_decode($response);
		curl_close($ch);
		return $repos;
	}


	function get_repositories_by_user($token){
		$code = 'access_token='.$token;	
		$url = 'https://api.github.com/user/subscriptions?'.$code;
		$ch = $this->createCurl($url);
		$response = curl_exec($ch);
		$repos = json_decode($response);
		curl_close($ch);
		return $repos;

	}

	function get_repos_for_orgs($org, $token){
		$code = 'access_token='.$token;
		$url = 'https://api.github.com/orgs/'.$org.'/repos?'.$code;
		$ch = $this->createCurl($url);
		$response = curl_exec($ch);
		$repos = json_decode($response);
		curl_close($ch);
		return $repos;
	}

	function get_repo_info($owner,$name, $token){
		$code = 'access_token='.$token;
		$url = 'https://api.github.com/repos/'.$owner.'/'.$name.'?'.$code;
		$ch = $this->createCurl($url);
		$response = curl_exec($ch);
		$repos = json_decode($response);
		curl_close($ch);
		return $repos;
	}

	function get_info_from_repositories($repos) {
		$lang = array();
		foreach($repos as $repo){
			$language = $repo->language;
			if (!in_array($language, $lang)) {
			    array_push($lang, $language);
			}
		}
		return $lang;
		
	}

	function get_repo_commits($repo,$token) {
		$code = 'access_token='.$token;	
		$url = 'https://api.github.com/repos/'.$repo->owner.'/'.$repo->name.'/commits?'.$code . '&per_page=100';
		$ch = $this->createCurl($url);
		$response = curl_exec($ch);
		$commit = json_decode($response);
		curl_close($ch);
		return $commit;
	}

	function get_commit_info($repo,$commit,$token) {
		$code = 'access_token='.$token;	
		$url = 'https://api.github.com/repos/'. ((isset($repo->owner->login))?$repo->owner->login:$repo->owner).'/'.$repo->name.'/commits/'.$commit->sha.'?'.$code;
		$ch = $this->createCurl($url);
		$response = curl_exec($ch);
		$commit = json_decode($response);
		curl_close($ch);
		return $commit;
	}

	function get_events_for_user($username,$service, $token) {
		//https://api.github.com/users/nifer/events?per_page=300		
		$code = 'access_token='.$token;
		$events = array();
		$i = 1;
		$keepfinding = true;
		$today = new DateTime();
		$dtEnd = date_create($service->updated_on);
		//According to the last update time, I'll set the totalPages amount
		if(strtotime($service->updated_on)>0 && $today->diff($dtEnd)->format('%a')<3){
			$totalPages = GITHUB_MIN_EVENTS_PAGES;
		}
		else{
			$totalPages = GITHUB_MAX_EVENTS_PAGES;
		}
		
		while ($i <= $totalPages && $keepfinding){
			$url = 'https://api.github.com/users/'.$username.'/events?page='.$i.'&'.$code;
			$ch = $this->createCurl($url);
			$response = curl_exec($ch);
			$repos = json_decode($response);
			if(count($repos) < 30){
				$keepfinding = false;
			}
			
			$events = array_merge((array)$events,(array)$repos);
			curl_close($ch);
			$i++;
		}

		return $events;
		
	}

	function filter_push_events($events){

		$onlyPushEvents = array();

		foreach($events as $event){
			if(isset($event) && $event->type == 'PushEvent'){
				array_push($onlyPushEvents, $event);
			}
		}
		
		return $onlyPushEvents;
	}

	/* 
	*	returns a repositories dictionary, indexed by the repo ID
	*
	*	param repositiories array
	*/
	function get_repositories_dictionary($repositories){

		$dictionary = array();
		foreach ($repositories as $repo) {
			$dictionary[$repo->id] = $repo;
		}

		return $dictionary;
	}

	function process_commits($user, $github, $push, $ghub_commit)
	{
		$CI =& get_instance();
		$CI->load->model('Service_model');
		$CI->load->model('Extension_model');
		$CI->load->model('Repository_model');
		$CI->load->model('Language_model');
		$CI->load->model('Commit_model');
		$CI->load->model('Filecode_model');
		$CI->load->model('Push_model');
		$CI->load->model('User_model');
		$CI->load->model('Framework_model');
		$CI->load->model('Badge_model');
		$CI->load->model('Issue_model');

		if(isset($ghub_commit->sha)){
			$commit = new Commit_model();
			$commit->push_id = $push->id;
			$commit->sha = $ghub_commit->sha;
			$commit->service_id = $github->id;
			$commit->description = $ghub_commit->commit->message;
			if(isset($ghub_commit->stats)){
				$commit->total = $ghub_commit->stats->total;
				$commit->additions = $ghub_commit->stats->additions;
				$commit->deletions = $ghub_commit->stats->deletions;	
			}
			$commit = $commit->create();
			//Get files modified in commit
			if(isset($ghub_commit->files)){
				foreach ($ghub_commit->files as $file) {
					$filename = explode('.',$file->filename);
					//use the repo language as default
					/*$language = $language_db;
					if(count($filename)>1){
						$ext = $filename[count($filename)-1];
						$languages_db = $CI->Language_model->get_by_extension($ext);
						if(count($languages_db)==1){
							$language = $languages_db[0];
							$languages[] = $language;
						}
						$extension = $CI->Extension_model->get_by_name($ext);
					}*/

					//Add file data to DB
					$filecode = new Filecode_model();
					$filecode->commit_id = $commit->id;
					$filecode->filename = $file->filename;
					$filecode->additions = $file->additions;
					$filecode->deletions = $file->deletions;
					$filecode->changes = $file->changes;
					$filecode->raw_url = $file->contents_url;
					$filecode->language_id = null; //$language->id;
					$filecode->user_id = $user->id;
					$filecode->extension_id =  null; //$extension != '' ? $extension->id : NULL;
					$filecode->excepted = preg_match("/".REGEX_FILE_EXCEPTION."/",$file->filename);
					$filecode->create($file->status);
				}	
			}
		}
	}

	function process_profiles($user)
	{
		$CI =& get_instance();
		$CI->load->model('Service_model');
		$CI->load->model('Extension_model');
		$CI->load->model('Repository_model');
		$CI->load->model('Language_model');
		$CI->load->model('Commit_model');
		$CI->load->model('Filecode_model');
		$CI->load->model('Push_model');
		$CI->load->model('User_model');
		$CI->load->model('Framework_model');
		$CI->load->model('Badge_model');
		$CI->load->model('Issue_model');
		
		$github = $CI->Service_model->get_by_name(GITHUB_SERVICE_NAME);
		$service = $CI->Service_model->get_token_by_user($user->id,$github->id);
		$service_updated_on = strtotime($service->updated_on);
		$response = new stdClass();
		if(isset($service->access_token)){
			$languages_by_ext = $CI->Extension_model->get_all();
			//Get GitHub Access token for this user and process 
			$token = $service->access_token;

			$service_id = $service->service_id;
			//Set the service username for the user
			$service_username = $service->username;

			$user_data = $this->get_user_info($token);

			if(!isset($user_data->id)){
				//Delete this service from the services_x_user table
				$CI->User_model->unlink_service($user->id, $service_id);
				//Send Email to user notifiying that the Token is no longer valid and must login to refresh it
				$response->error = true;
				$response->code = ERROR_SERVICE_BAD_TOKEN;
				return $response;
			}
			
			//$orgs = $this->get_organizations($token);

			/*$open_issues = $this->get_issues_assigned($token, 'open');
			$closed_issues = $this->get_issues_assigned($token, 'closed');
			*/

			//$userRepos = $this->get_repositories_by_user($token);
			$userRepos = array();

			$allEvents = $this->get_events_for_user($service_username, $service, $token);
			
			$onlyPushEvents = $this->filter_push_events($allEvents);

			$countingPushByRepos = array();
			
			$languages = array();

			//logging info
			//$CI->utils->log('Organizations',$orgs);	
			$CI->utils->log('User Info',$user_data);	
			//$CI->utils->log('Repos',$userRepos);	
			$CI->utils->log('All Events',$allEvents);	
			/*$CI->utils->log('Push Events',$onlyPushEvents);	
			$CI->utils->log('Open Issues',$open_issues);	
			$CI->utils->log('Closed Issues',$closed_issues);	
			*/
			foreach($onlyPushEvents as $event){
				$repoId = $event->{'repo'}->{'id'};
				if(isset($countingPushByRepos[$repoId])){
					$countingPushByRepos[$repoId]++;
				}else{
					$countingPushByRepos[$repoId] = 1;
				}
			}

			//$repoDictionary = $this->get_repositories_dictionary($userRepos);
			$repoDictionary = array();

			//Get All repos from db
			
			$orgsReposDictionary = null;
			$i=0;
			foreach($onlyPushEvents as $push_evt){
				$event_date = strtotime($push_evt->created_at);
				if($event_date<$service_updated_on)
					continue;

				$ghub_repo = null;
				if(isset($repoDictionary[$push_evt->repo->id])){
					$ghub_repo = $repoDictionary[$push_evt->repo->id];
				}else{
					list($owner, $name) = explode("/",$push_evt->repo->name);
					$ghub_repo = $this->get_repo_info($owner,$name, $token);
					//Add this repo to the user repositories to calculate languages commits etc
					array_push($userRepos,$ghub_repo);
				}

				$CI->utils->log('Push Event',$push_evt);
				$CI->utils->log('Repo Info',$ghub_repo);
				
				if (isset($ghub_repo) && isset($ghub_repo->id)){
					$push = new Push_model();
					$push->push_id = $push_evt->payload->push_id;
					if(!$push->exists(null, $user->id)){
						$language_db = $CI->Language_model->get_by_name($ghub_repo->language);
						$repository = new Repository_model();
						$repository->repository_id = $push_evt->repo->id;
						$repository->language_id = NULL; //$language_db->id;
						$repository->owner = $ghub_repo->owner->login;
						$repository->name = $ghub_repo->name;
						$repository->service_id = $github->id;
						$repository->is_private = $ghub_repo->private;
						$repository->description = $ghub_repo->description;
						$repository->created_at = $ghub_repo->created_at;
						$repository->updated_at = $ghub_repo->updated_at;
						$repository->pushed_at = $ghub_repo->pushed_at;
						$repository->watchers = $ghub_repo->watchers;
						$repository->forks = $ghub_repo->forks;
						$repository->open_issues = $ghub_repo->open_issues;

						if($repository->exists()){
							$repository = $repository->update();
						}else{
							$repository = $repository->create();
						}

						$push->user_id = $user->id;
						$push->repo_id = $repository->id;
						$push->created_on = $push_evt->created_at;
						$push = $push->create();
						//Add the relation between the user and the repo
						$CI->Repository_model->add_by_user($user->id,$repository->id);
						//Add language to the user
						$repo_language = $ghub_repo->language;
						if (!in_array($repo_language, $languages)) {
						    array_push($languages, $repo_language);
						}
						//Add this push commits
						foreach ($push_evt->payload->commits as $item) {
							//echo($item->sha);
							$ghub_commit = (object)$this->get_commit_info($ghub_repo,$item,$token);
							//Add this commit
							$this->process_commits($user, $github, $push, $ghub_commit);
						}
					}
				}
			}
			/*
			//Getting Issues
			if (is_array($open_issues))
			{
				foreach($open_issues as $issue){				
					$issue_id = $issue->{'id'};
					$issue_obj = $CI->Issue_model->get($issue_id);

					if (isset($issue_obj)) {

						if ($issue_obj->is_open == 0) {
							$issue_obj->has_re_opens = $issue_obj->has_re_opens + 1;
							$issue_obj->is_open = 1;

							$CI->utils->log('Updating...',$issue_obj);
						}
						else
						{
							//Nothing changed from last time...		
							$CI->utils->log('Nothing changed..',$issue_obj);				
						}		

							

						$issue_obj->update();			
					}
					else
					{
						//Get Repository
						$repository = $CI->Repository_model->get($issue->{'repository'}->{'id'}, GITHUB_SERVICE_ID);

						$issue_obj = new Issue_model();					
						$issue_obj->description = $issue->{'title'};
						$issue_obj->user_id = $user->id;
						$issue_obj->issue_id = $issue_id;
						$issue_obj->is_open = 1;
						$issue_obj->has_re_opens = 0;
						$issue_obj->created_on = $issue->{'created_at'};
						$issue_obj->repository_id = $repository->id;

						$issue_obj->create();
					}
				}
			}
			
			if (is_array($closed_issues))
			{
				foreach($closed_issues as $issue){				
					$issue_id = $issue->{'id'};
					$issue_obj = $CI->Issue_model->get($issue_id);

					if (isset($issue_obj)) {
						if ($issue_obj->is_open == 1) {						
							$issue_obj->is_open = 0;
						}
						else
						{
							//Nothing changed from last time...
						}		

						$issue_obj->update();			
					}
					else
					{
						//Get Repository
						$repository = $CI->Repository_model->get($issue->{'repository'}->{'id'}, GITHUB_SERVICE_ID);

						$issue_obj = new Issue_model();
						$issue_obj->issue_id = $issue_id;
						$issue_obj->description = $issue->{'title'};
						$issue_obj->user_id = $user->id;
						$issue_obj->is_open = 0;
						$issue_obj->has_re_opens = 0;
						$issue_obj->repository_id = $repository->id;
						$issue_obj->created_on = $issue->{'created_at'};

						$issue_obj->create();
					}
				}
			}
*/
			//now analyze activity as owner of repos..
/*			$repositories = $CI->Repository_model->get_by_user($user->id);


			foreach ($repositories as $repository) {
				$commits = $this->get_repo_commits($repository, $token);
				
				$CI->utils->log('Repo',$repository);	
				

				if (!isset($commits) || (!is_array($commits))) {
					continue;
				}

				foreach ($commits as $commit) {					
					$commit_date = strtotime($commit->commit->committer->date);
					if($commit_date<$service_updated_on)
						continue;

					//Verify if user exists in kloc... if not create one... and process... if exists do nothing because it will override
					$user_not_me = $CI->Service_model->get_by_username_and_service($commit->author->login, GITHUB_SERVICE_ID);
					
					if (count($user_not_me) == 0) {
						//extra verification
						$user_not_me = $CI->User_model->exists(null, $commit->author->login);

						if (!$user_not_me) {
							//This means it doesn't exist
							$user_not_me = new User_model();
							$user_not_me->full_name = $commit->commit->author->name;
					      	$user_not_me->username = $commit->author->login;				      	
					      	$user_not_me->email = $commit->commit->author->email;				      						      	
				      		$user_not_me = $user_not_me->create(false);

				      		//Add the relation between the user and the repo
							$CI->Repository_model->add_by_user($user_not_me->id,$repository->id);

						}
						
				      	$push = new Push_model();
				      	//Just a hack...
						$push->push_id = $commit->sha;

						if(!$push->exists(null, $user_not_me->id)){							
							$push->user_id = $user_not_me->id;
							$push->repo_id = $repository->id;
							$push->created_on = $commit->commit->committer->date;
							$push = $push->create();
							//Add the relation between the user and the repo
							$CI->Repository_model->add_by_user($user_not_me->id,$repository->id);
							//Add language to the user							
						}

						$ghub_commit = (object)$this->get_commit_info($repository,$commit,$token);

						$CI->utils->log('Commit Info',$ghub_commit);	

				      	$this->process_commits($user_not_me, $github, $push, $ghub_commit);

				      	
				      	$user_not_me->process_skills();
					}

					//Get the issues if any
					//$issues = $provider->get_issues($consumer, $token, $bb_repository->slug, $bb_repository->owner,$user_info->user->username);						
					//$provider->process_issues($issues, $repository, $user);
				}
				
			}
*/
			
			//Set updated date for this service. In this case Github
			$user->set_updated($service_id);
			//Build response
			$response->error = false;
		}else{
			$response->error = true;
			$response->code = ERROR_SERVICE_INEXISTENT;
		}
		return $response;	
	}
  	
}

/* End of file Github.php */